#!/bin/bash

docker-compose -f docker-compose.prod.sanalto.yml build
docker-compose -f docker-compose.prod.sanalto.yml up --remove-orphans -d nginx django vuejs postgres codigos_postales_api
