#!/usr/bin/bash

clear

pushd Frontend
npm run build
popd

pushd Backend
python manage.py collectstatic --noinput
popd
