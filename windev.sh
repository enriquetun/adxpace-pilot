#!/bin/bash

clear
docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml up --remove-orphans -d postgres nginx
docker-compose -f docker-compose.dev.yml up --remove-orphans django
