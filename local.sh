
#!/bin/bash

docker-compose build
docker-compose up --remove-orphans -d nginx django vuejs postgres codigos_postales_api
