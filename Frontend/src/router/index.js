import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Pilot/Landing/Landing'
import AsistenteLanding from '@/components/Pilot/Asistente/AsistenteLanding'
import AsistentePropietarios from '@/components/Pilot/Asistente/AsistentePropietarios'
import AsistentePropietario from '@/components/Pilot/Asistente/AsistentePropietario'
import AsistenteDatosBancarios from '@/components/Pilot/Asistente/AsistenteDatosBancarios'
import AsistenteEspacios from '@/components/Pilot/Asistente/AsistenteEspacios'
import AsistenteEspacio from '@/components/Pilot/Asistente/AsistenteEspacio'
import VendedorLanding from '@/components/Pilot/Vendedor/VendedorLanding'
import VendedorDatosPersonales from '@/components/Pilot/Vendedor/VendedorDatosPersonales'
import VendedorDatosComerciales from '@/components/Pilot/Vendedor/VendedorDatosComerciales'
import Espacios from '@/components/Pilot/Vendedor/Espacios'
import Pro from '@/components/Pilot/Vendedor/Pro'
import VendedorMisPropietarios from '@/components/Pilot/Vendedor/VendedorMisPropietarios'
import PropietarioLanding from '@/components/Pilot/Propietario/PropietarioLanding'
import PropietarioDatosPersonales from '@/components/Pilot/Propietario/PropietarioDatosPersonales'
import PropietarioDatosBancarios from '@/components/Pilot/Propietario/PropietarioDatosBancarios.vue'
import Espacio from '@/components/Pilot/Propietario/Espacio'
import PropietarioEspaciosRegistrados from '@/components/Pilot/Propietario/PropietarioEspaciosRegistrados'
import RegistrarDirecciones from '@/components/Pilot/Propietario/RegistrarDirecciones'
import VendedorRegistrarDirecciones from '@/components/Pilot/Vendedor/VendedorRegistrarDirecciones'
import PropietarioDirecciones from '@/components/Pilot/Propietario/PropietarioDirecciones'
import AsistenteLocal from '@/components/Pilot/Asistente/AsistenteLocal'
import AsistenteLocales from '@/components/Pilot/Asistente/AsistenteLocales'
import AsistentePropietarioEspacio from '@/components/Pilot/Asistente/AsistentePropietarioEspacio'
import AsistentePropietarioLocal from '@/components/Pilot/Asistente/AsistentePropietarioLocal'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing,
    },
    {
      path: '/login',
      name: 'Login',
      component: Landing

    },
    {
      path: '/propietario/',
      name: 'PropietarioLanding',
      component: PropietarioLanding,
    },
    {
      path: '/propietario/espacio',
      name: 'Espacio',
      component: Espacio,
    },
    {
      path: '/propietario/datos_personales',
      component: PropietarioDatosPersonales,
    },
    {
      path: '/propietario/espacios_registrados',
      component: PropietarioEspaciosRegistrados,
    },
    {
      path: '/propietario/registrar_direcciones',
      component: RegistrarDirecciones,
    },
    {
      path: '/propietario/direcciones',
      component: PropietarioDirecciones,
    },
    {
      path: '/vendedor/',
      name: 'VendedorLanding',
      component: VendedorLanding,
    },
    {
      path: '/vendedor/datos_personales',
      component: VendedorDatosPersonales,
    },
    {
      path: '/vendedor/datos_comerciales',
      component: VendedorDatosComerciales,
    },
    {
      path: '/vendedor/espacios',
      name: 'Espacios',
      component: Espacios,
    },
    {
      path: '/vendedor/mis_propietarios',
      name: 'VendedorMisPropietarios',
      component: VendedorMisPropietarios,
    },
    {
      path: '/vendedor/pro',
      name: 'Pro',
      component: Pro,
    },
    {
      path: '/vendedor/registrar_direcciones',
      component: VendedorRegistrarDirecciones,
    },
    {
      path: '/propietario/datos_bancarios',
      name: 'PropietarioDatosBancarios',
      component: PropietarioDatosBancarios
    },
    {
      path: '/asistente/',
      name: 'AsistenteLanding',
      component: AsistenteLanding,
    },
    {
      path: '/asistente/propietarios',
      name: 'AsistentePropietarios',
      component: AsistentePropietarios,
    },
    {
      path: '/asistente/propietarios/propietario',
      name: 'AsistentePropietario',
      component: AsistentePropietario,
    },
    {
      path: '/asistente/propietario/datos_bancarios',
      name: 'AsistenteDatosBancarios',
      component: AsistenteDatosBancarios,
    },
    {
      path: '/asistente/espacios',
      name: 'AsistenteEspacios',
      component: AsistenteEspacios,
    },
    {
      path: '/asistente/espacios/espacio',
      name: 'AsistenteEspacio',
      component: AsistenteEspacio,
    },
    //--------------------------
    {
      path: '/asistente/locales',
      name: 'AsistenteLocales',
      component: AsistenteLocales,
    },
    {
      path: '/asistente/locales/local',
      name: 'AsistenteLocal',
      component: AsistenteLocal,
    },
    {
      path: '/asistente/propietarios/propietario/espacio',
      name: 'AsistentePropietarioEspacio',
      component: AsistentePropietarioEspacio
    },
    {
      path: '/asistente/propietarios/propietario/local',
      name: 'AsistentePropietarioLocal',
      component: AsistentePropietarioLocal
    },
  ]
})
