import Vue from 'vue'
import VueResource from 'vue-resource'
import store from './store.js'

Vue.use(VueResource)

export default {
  get (url) {
    return Vue.http.get(url)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  post (url, request) {
    request.csrfmiddlewaretoken = getCookie('csrftoken')
    var config = {emulateJSON: true,}
    return Vue.http.post(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))

  },
  post_special (url, request) {
    request.csrfmiddlewaretoken = getCookie('csrftoken')
    var config = {emulateJSON: true,}
    return Vue.http.post(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))

  },
  post_file (url, request) {
    request.append('csrfmiddlewaretoken', getCookie('csrftoken'))
    request.append('username', store.state.username)
    var config =
      {headers: {'Content-Type': 'multipart/form-data'},
      }

    return Vue.http.post(url, request, config)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))

  },
  patch (url, request) {
    return Vue.http.patch(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  },
  delete (url, request) {
    return Vue.http.delete(url, request)
      .then((response) => Promise.resolve(response))
      .catch((error) => Promise.reject(error))
  }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
