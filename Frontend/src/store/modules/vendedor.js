import api from '@/store/api'
import router from '@/router'
import env from '@/env'

const apiRoot = env.apiRoot
const state = {
  apiRoot: apiRoot,
  propietario: '',
  authenticated: false,
  username: '',
  user: {
  },
}

const getters = {
  get_propietarios: state => state.propietarios,
  get_nr_propietarios: state => state.nr_propietarios,
  get_nr_espacios: state => state.nr_espacios,
  get_propietario: state => state.propietario,
  get_giros: state => state.giros,
  get_materiales: state => state.materiales,
  get_tipos: state => state.tipos,
  get_restricciones: state => state.restricciones,
  espacios: state  => state.espacios,
  // get_propietarioNew: state => state.newPropietarioNew[0]
  'get_propietarioNew':function(state){
    let vendedor_propietario=['']
    let k=0
    for(var i=0; i<state.newPropietarioNew.length; i++){
      let propietario = state.newPropietarioNew[i]
      vendedor_propietario[k++]={
        'index' : k,
        'id' : propietario.pk,
        'nr' : propietario.pk,
        'propietario' : propietario.primer_nombre +' '+propietario.apellido_paterno,
        'direcciones' : propietario.nr_locales,
        'locales' : propietario.nr_espacios,
        'email' : propietario.email,
        'telefono' : propietario.telefono,
        'estatus' : propietario.estatus
      }
    }
    return vendedor_propietario
  }
}

const actions = {
  DatosPersonalesPost({state, commit}, datos_personales) {
    datos_personales.username = state.username
    return api.post(apiRoot + '/pilot/addowner', datos_personales)
    .then((response) => commit('DATOS_PERSONALES_RESPONSE', response))
    .catch((error) => commit('API_FAIL', error))
  },
  VendedorDatosPersonalesPost({state, commit}, datos_personales){
    return api.post(apiRoot + '/pilot/update_datos_personales_vendedor', datos_personales)
    .then(function(response){
      if(response.body.respuesta == true){
        commit('UPDATE_DATOS_PERSONALES', response)
      }
      else {
        return false
      }
    })
    .catch((error) => commit('VENDEDOR_API_FAIL', error))
  },
  getPropietarios({state, commit}) {
    return api.get(apiRoot + '/pilot/getpropietarios')
    .then((response) => commit('GET_PROPIETARIOS_RESPONSE', response))
    .catch((error) => commit('VENDEDOR_API_FAIL', error))
  },
  getPropietario(store, propietario) {
    this.state.propietario = []
    return api.post(apiRoot + '/pilot/getpropietario/' + propietario)
    .then((response) => store.commit('GET_PROPIETARIO_RESPONSE', response))
    .catch((error) => store.commit('VENDEDOR_API_FAIL', error))
  },
  vendedorPostEspacio(store, espacio) {
    return api.post(apiRoot + '/pilot/addcomercialspace', espacio)
    .then((response) => store.commit('VENDEDOR_POST_ESPACIO_RESPONSE', response))
    .catch((error) => store.commit('VENDEDOR_API_FAIL', error))
  },
  vendedorUploadFoto(store, foto) {
    return api.post_file(apiRoot + '/pilot/uploadaddspacephoto', foto)
    .then((response) => store.commit('VENDEDOR_POST_FOTO_RESPONSE', response))
    .catch((error) => store.commit('VENDEDOR_API_FAIL', error))
  },
  vendedorAddLocal(store, data){
    return api.post(apiRoot + '/pilot/addLocal', data)
    .then((response) => store.commit('VENDEDOR_ADD_LOCAL', response))
    .catch((error) => store.commit('VENDEDOR_API_FAIL', error))
  },
  vendedorUploadFotoLocal(store, foto) {
    return api.post_file(apiRoot + '/pilot/uploadlocalphoto', foto)
    .then((response) => store.commit('VENDEDOR_POST_FOTO_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
}

const mutations = {
  'VENDEDOR_ADD_LOCAL': function(state, response){
    state.response = response.body
  },
  'UPDATE_DATOS_PERSONALES': function(state, response){
  },
  'GET_REGISTER': function(state, response) {
    if(response.body.respuesta == true) {
      var datos_personales_response = JSON.parse(response.body.datos_personales)
      var datos_personales= datos_personales_response[0].fields
      datos_personales.pk = datos_personales_response[0].pk
      state.register = datos_personales
    }
    else {
      state.register = false
    }
  },
  'POST_REGISTER': function(state, response) {
    if(response.body.respuesta == false) {
      state.register.error_message = response.body.mensaje
    }
    else{
      state.register.error_message = false
      state.register.success_message = response.body.mensaje
    }
  },
  'SET_USER_VENDEDOR': function(state, response) {

    state.group = 'vendedor'
    state.user = response.body.user
    state.nr_propietarios = [response.body.promociones[0], response.body.promociones[1], response.body.promociones[0]-response.body.promociones[1]]
    state.nr_espacios = response.body.promociones[2]
    state.propietarios = response.body.propietarios
    //se agrego
    state.newPropietarioNew = response.body.propietarios

    for (var i in response.body.espacios) {
      state.propietarios.find(item => item.pk === response.body.espacios[i].propietario).nr_espacios = response.body.espacios[i].nr_espacios
      state.propietarios.find(item => item.pk === response.body.espacios[i].propietario).nr_locales = response.body.espacios[i].nr_locales
    }
    state.materiales = response.body.materiales
    state.giros = response.body.giros
    state.tipos = response.body.tipos
    state.restricciones = response.body.restricciones

    return true
  },
  'DATOS_PERSONALES_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      state.nr_propietarios = [response.body.promociones[0], response.body.promociones[1], response.body.promociones[0]-response.body.promociones[1]]
      state.nr_espacios = response.body.promociones[2]
      state.propietarios.push(response.body.propietario)
      state.propietario = response.body.propietario
      for (var i in response.body.espacios) {
        state.propietarios.find(item => item.pk === response.body.espacios[i].propietario).nr_espacios = response.body.espacios[i].nr_espacios
        state.propietarios.find(item => item.pk === response.body.espacios[i].propietario).nr_locales = response.body.espacios[i].nr_locales
      }
    }
  },
  'GET_PROPIETARIOS_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      state.propietarios = response.body.propietarios

    }
  },
  'GET_PROPIETARIO_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      // state.propietario = response.body.propietario
      console.log(response.body)
      store.commit('SAVE_LOCALSTATE')
      return true
    }
  },
  'VENDEDOR_POST_ESPACIO_RESPONSE': function(state, response) {
    state.response = response.body
    state.nr_propietarios = [response.body.promociones[0], response.body.promociones[1], response.body.promociones[0]-response.body.promociones[1]]
    state.nr_espacios = response.body.promociones[2]
    for (var i in response.body.espacios) {
      state.propietarios.find(item => item.pk === response.body.espacios[i].propietario).nr_espacios = response.body.espacios[i].nr_espacios
      state.propietarios.find(item => item.pk === response.body.espacios[i].propietario).nr_locales = response.body.espacios[i].nr_locales
    }
  },
  'VENDEDOR_POST_FOTO_RESPONSE': function(state, response) {
    if (response.body.respuesta[0] == true) {

    }
  },
  'VENDEDOR_API_FAIL': function (state, error) {
    console.error(error)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
