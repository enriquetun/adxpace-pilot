import api from '@/store/api'
import router from '@/router'
import env from '@/env'

const apiRoot = env.apiRoot
const state = {
  apiRoot: apiRoot,
  authenticated: false,
  username: '',
}

const getters = {
  is_authenticated: state => state.authenticated,
}

const actions = {
  loginPost ({state, commit}, credentials) {
    return api.post(apiRoot + '/pilot/login', credentials)
    .then((response) => {
      console.log(response.body)
      commit('LOGIN_RESPONSE', response)
      if(response.body.group == 'promotor') {
        commit('SET_USER_VENDEDOR', response)
      }
      else if (response.body.group == 'propietario') {
        commit('SET_USER_PROPIETARIO', response)
      }
      else if (response.body.group == 'asistente') {

        commit('SET_USER_ASISTENTE', response)
      }
      if (response.body.group == 'promotor'){
        return 'vendedor'
      }
      else {
        return response.body.group        
      }
    })
    .catch((error) => commit('API_FAIL', error))
  },
  logout(store) {
    var params = {
      'params': {},
    }
    return api.get(apiRoot + '/pilot/logout', params)
    .then((response) => store.commit('LOGOUT', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
}

const mutations = {

  'LOGIN_RESPONSE': function (state, response) {
    if (response.body.authenticated) {
      state.username = response.body.authenticated
      state.authenticated = true
    }
  },
  'LOGOUT': function(state, response) {
    state.authenticated = false
    var django_session = {
      'authenticated' : state.authenticated,
      'username' : state.username,
      'csrfmiddlewaretoken' : '',
    }

    window.location.href = state.apiRoot
  },
  'API_FAIL': function (state, error) {
    console.error(error)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
