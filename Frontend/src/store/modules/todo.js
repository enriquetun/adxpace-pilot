import api from '@/store/api'
import router from '@/router'
import env from '@/env'

const apiRoot = env.apiRoot
const state = {
  apiRoot: apiRoot,
  propietario: '',
  authenticated: false,
  username: '',
  user: {
    datos_bancarios:{

    },
    datos_comerciales:{

    },
  },
  register: '',
  espacios: [],
  carousel_adds: [],
  carousel_class:[
    'first-slide',
    'second-slide',
    'third-slide',
  ],
  carousel_alt:[
    'First slide',
    'Second slide',
    'Third slide',
  ],
  n: 1,
}

const getters = {
  authenticated: state => state.authenticated,
  get_map_modal: state => () => state.map_modal,
  get_user: state => () => state.user,
  get_user_datos_personales: state => () => state.user.datos_personales,
  get_promotor: state => () => state.user.username,
  get_propietarios: state => () => state.propietarios,
  get_propietario: state => () => state.propietario,
  get_nr_propietarios: state => () => state.nr_propietarios,
  get_nr_espacios: state => () =>state.nr_espacios,
  get_giros: state => () => state.giros,
  get_materiales: state => () => state.materiales,
  get_tipos: state => () => state.tipos,
  get_restricciones: state => () => state.restricciones,
  get_register: state => () => state.register,
  getN: state => () => state.n,
  espacios: state => () => state.espacios,
}

const actions = {
  datos_bancarios(store, datos_bancarios){
    datos_bancarios.pk = store.state.user.datos_personales.pk
    return api.post(apiRoot + '/pilot/datos_bancarios', datos_bancarios)
    .then((response) => store.commit('DATOS_BANCARIOS', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  datos_comerciales(store, datos_comerciales){
    datos_comerciales.pk = store.state.user.datos_personales.pk
    return api.post(apiRoot + '/pilot/datos_comerciales', datos_comerciales)
    .then((response) => store.commit('DATOS_COMERCIALES', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  set_datos_bancarios(store){
    return api.get(apiRoot + '/pilot/datos_bancarios')
    .then((response) => store.commit('SET_DATOS_BANCARIOS', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  set_espacios(store){
    return api.get(apiRoot + '/pilot/get_espacios/' + store.state.user.datos_personales.pk)
    .then((response) => store.commit('SET_ESPACIOS', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  set_propietarios() {
    store.commit('SET_PROPIETARIOS')
  },
  // The URLs come from the Django URL router
  getCarouselAdds (store) {
    return api.get(apiRoot + '/get_anuncios')
    //return api.get('/get_anuncios')
    .then((response) => store.commit('GET_CAROUSEL_ADDS', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  getRegister(store, id) {
    return api.get(apiRoot + '/pilot/register/' + id)
    .then((response) => store.commit('GET_REGISTER', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  postRegister(store, register) {
    return api.post_special(apiRoot + '/pilot/register/0', register)
    .then((response) => store.commit('POST_REGISTER', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  loginPost ({state, commit}, credentials) {
    console.log('credentials');
    console.log(credentials)
    state.authenticated = false
    state.username = credentials.username

    return api.post(apiRoot + '/pilot/login', credentials)
    .then((response) => {
      commit('LOGIN_RESPONSE', response)
      if(response.body.group == 'vendedor') {
        commit('SET_USER_VENDEDOR', response)
      }
      else if (response.body.group == 'propietario') {
        commit('SET_USER_PROPIETARIO', response)
      }
      return response.body.group
    })
    .catch((error) => store.commit('API_FAIL', error))
  },
  DatosPersonalesPost(store, datos_personales) {
    datos_personales.username = this.state.username
    return api.post(apiRoot + '/pilot/addowner', datos_personales)
    .then((response) => store.commit('DATOS_PERSONALES_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  PropietarioDatosPersonalesPost(store, datos_personales){
    datos_personales.username = this.state.username
    return api.post(apiRoot + '/pilot/update_datos_personales', datos_personales)
    .then((response) => store.commit('PROPIETARIO_DATOS_PERSONALES_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  VendedorDatosComerciales(store, datos_personales){
    datos_personales.username = this.state.username
    return api.get(apiRoot + '/pilot/update_datos_comerciales/' + datos_personales.pk)
    .then((response) => store.commit('VENDEDOR_DATOS_COMERCIALES_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  VendedorDatosPersonalesPost(store, datos_personales){
    return api.post(apiRoot + '/pilot/update_datos_personales_vendedor', datos_personales)
    .then(function(response){
      console.log(response.body)
      if(response.body.respuesta == true){
        return true
      }
      else {
        return false
      }
    })
    .catch((error) => store.commit('API_FAIL', error))
  },
  // UpdatePropietariosList(store, datos_personales){
  //   store.commit('UPDATE_PROPIETARIOS_LIST', datos_personales)
  // },
  getPropietarios(store) {
    let promotor = {}
    promotor.username = this.state.user.username
    this.state.propietarios = []
    return api.post(apiRoot + '/pilot/getpropietarios', promotor)
    .then((response) => store.commit('GET_PROPIETARIOS_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  getPropietario(store, propietario) {
    this.state.propietario = []
    return api.post(apiRoot + '/pilot/getpropietario/' + propietario)
    .then((response) => store.commit('GET_PROPIETARIO_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  postEspacio(store, espacio) {
    return api.post(apiRoot + '/pilot/addcomercialspace', espacio)
    .then((response) => store.commit('POST_ESPACIO_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  uploadFoto(store, foto) {
    return api.post_file(apiRoot + '/pilot/uploadaddspacephoto', foto)
    .then((response) => store.commit('POST_FOTO_RESPONSE', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  logout(store) {
    console.log(this.state);
    var params = {
      'params': {},
    }
    return api.get(apiRoot + '/pilot/logout', params)
    .then((response) => store.commit('LOGOUT', response))
    .catch((error) => store.commit('API_FAIL', error))
  },
  storeCSRF (store, csrfmiddlewaretoken) {
    return store.commit('STORE_CSRF', csrfmiddlewaretoken)
  },
  loginStatus (store) {
    return this.state.authenticated
  },
  restoreSessionState (store, django_session) {
    return store.commit('RESTORE_SESSIONSTATE', django_session)
  },
  resotreLocalUser(store, user_local) {
    return store.commit('RESTORE_LOCALUSER', user_local)
  },
  restoreLocalState(store, state_local) {
    return store.commit('RESTORE_LOCALSTATE', state_local)
  }
}

const mutations = {
  // Mutations are responsible for updating the client state.
  // The response is the HTTP response
  // returned by the Promise.
  'DATOS_BANCARIOS': function(state, response){
    console.log(response.body)
  },
  'DATOS_COMERCIALES': function(state, response){
    console.log(response.body)
  },
  'SET_DATOS_BANCARIOS': function(state, response){
    state.user.datos_bancarios = response.body.datos_bancarios
    state.user.datos_comerciales = response.body.datos_comerciales
    console.log(state.user);
  },
  'SET_ESPACIOS': function(state, response){
    state.espacios = response.body.espacios
  },
  'GET_REGISTER': function(state, response) {
    if(response.body.respuesta == true) {
      var datos_personales_response = JSON.parse(response.body.datos_personales)
      var datos_personales= datos_personales_response[0].fields
      datos_personales.pk = datos_personales_response[0].pk
      state.register = datos_personales
    }
    else {
      state.register = false
    }
  },
  'POST_REGISTER': function(state, response) {
    if(response.body.respuesta == false) {
      state.register.error_message = response.body.mensaje
    }
    else{
      state.register.error_message = false
      state.register.success_message = response.body.mensaje
    }
  },
  'SET_PROPIETARIOS': function(state) {

  },
  'RESTORE_SESSIONSTATE': function (state, session_state) {
    state.authenticated = session_state.authenticated
    store.state.csrfmiddlewaretoken = session_state.csrfmiddlewaretoken
    store.state.username = session_state.username
    store.state.group = session_state.group
  },
  'RESTORE_LOCALUSER': function(state, user_local){
    if(state.group == 'vendedor') {
      state.user = user_local
    }
    else if (state.group == 'propietario') {
      console.log('FROM RESTORE_LOCALUSER')
      console.log(user_local)
      state.user.datos_personales = user_local
      console.log(state.user.datos_personales)
    }
  },
  'RESTORE_LOCALSTATE': function (state, state_local) {
    state.group = state_local.group
    state.nr_propietarios = state_local.nr_propietarios
    state.nr_espacios = state_local.nr_espacios
    state.propietarios = state_local.propietarios
    state.materiales = state_local.materiales
    state.giros = state_local.giros
    state.tipos = state_local.tipos
    state.restricciones = state_local.restricciones
  },
  'STORE_CSRF': function (state, csrfmiddlewaretoken) {
    state.csrfmiddlewaretoken = csrfmiddlewaretoken
  },
  'GET_CAROUSEL_ADDS': function (state, response) {
    console.log(response)
    state.carousel_adds = JSON.parse(response.body.anuncios)
    for (var i in state.carousel_adds) {
      state.carousel_adds[i].carousel_class = state.carousel_class[i]
      state.carousel_adds[i].carousel_alt = state.carousel_alt[i]
    }
  },
  'SAVE_LOCALSTATE': function(state) {
    var state_local = {
      'group': state.group,
      'propietarios': state.propietarios,
      'nr_propietarios': state.nr_propietarios,
      'nr_espacios': state.nr_espacios,
      'materiales': state.materiales,
      'giros': state.giros,
      'tipos': state.tipos,
      'restricciones': state.restricciones
    }
    localStorage.setItem('state_local', JSON.stringify(state_local))
  },
  'SET_USER_VENDEDOR': function(state, response) {
    state.group = response.body.group
    var local_session = localStorage.getItem('session')
    var session = JSON.parse(local_session)
    session.group = state.group
    localStorage.setItem('session', JSON.stringify(session))
    state.user = response.body.user
    localStorage.setItem('user_local', JSON.stringify(state.user))
    state.nr_propietarios = response.body.promociones[0]
    state.nr_espacios = response.body.promociones[1]
    state.propietarios = response.body.propietarios
    state.materiales = response.body.materiales
    state.giros = response.body.giros
    state.tipos = response.body.tipos
    state.restricciones = response.body.restricciones

    return true
  },
  'SET_USER_PROPIETARIO': function(state, response) {
    state.group = response.body.group
    var local_session = localStorage.getItem('session')
    var session = JSON.parse(local_session)
    session.group = state.group
    localStorage.setItem('session', JSON.stringify(session))
    state.user.datos_personales = response.body.datos_personales
    localStorage.setItem('user_local', JSON.stringify(state.user.datos_personales))
    state.user.datos_personales.pk = response.body.propietario
    state.materiales = response.body.materiales
    state.giros = response.body.giros
    state.tipos = response.body.tipos
    state.restricciones = response.body.restricciones
  },
  'LOGIN_RESPONSE': function (state, response) {
    console.log(response.body)
    if (response.body.authenticated) {
      state.authenticated = true
      console.log('STATE FROM LOGIN RESPONSE')
      console.log(state)
    }
  },
  'DATOS_PERSONALES_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      state.map_modal = false
      state.nr_propietarios = response.body.promociones[0]
      state.nr_espacios = response.body.promociones[1]
      state.propietarios.push(response.body.propietario)
      state.propietario = response.body.propietario
      store.commit('SAVE_LOCALSTATE')
      return true
    }
  },
  'PROPIETARIO_DATOS_PERSONALES_RESPONSE': function(state, response) {
    console.log(response.body)
    if (response.body.respuesta == true) {
      state.user.datos_personales = response.body.datos_personales
      store.commit('SAVE_LOCALSTATE')
    }
    return response.body.respuesta
  },
  // 'UPDATE_PROPIETARIOS_ENTRY': function(state, index) {
  //   state.propietarios[index] = state.user.datos_personales
  //   store.commit('SAVE_LOCALSTATE')
  //   return true
  // },
  'GET_PROPIETARIOS_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      state.propietarios = response.body.propietarios
      store.commit('SAVE_LOCALSTATE')
      return true
    }
  },
  'GET_PROPIETARIO_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      // state.propietario = response.body.propietario
      console.log(response.body)
      store.commit('SAVE_LOCALSTATE')
      return true
    }
  },
  'POST_ESPACIO_RESPONSE': function(state, response) {
    state.response = response.body
    state.nr_propietarios = response.body.promociones[0]
    state.nr_espacios = response.body.promociones[1]
    return true
  },
  'VENDEDOR_DATOS_COMERCIALES_RESPONSE': function(state, response){
    console.log(response)
  },
  'POST_FOTO_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      // do something
      return true
    }
  },
  'LOGOUT': function(state, response) {
    console.log('FROM LOGOUT')
    console.log(state)
    state.authenticated = false
    state.csrfmiddlewaretoken = ''
    var django_session = {
      'authenticated' : state.authenticated,
      'username' : state.username,
      'csrfmiddlewaretoken' : '',
    }
    localStorage.setItem('session', JSON.stringify(django_session))
    window.location.href = state.apiRoot
  },
  'ADD_SINGLE_ADD': function (state, response) {
    state.carousel_adds.push(response.body)
  },
  'CLEAR_ADDS': function (state) {
    const carousel_adds = state.carousel_adds
    carousel_adds.splice(0, carousel_adds.length)
  },
  // Note that we added one more for logging out errors.
  'API_FAIL': function (state, error) {
    console.error(error)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
