import api from '@/store/api'
import router from '@/router'
import env from '@/env'

const apiRoot = env.apiRoot
const state = {
  apiRoot: apiRoot,
  api:{
    codigos_postales:{
      colonias: '',
      municipio: '',
      estado: '',
    }
  },
  propietario: {
    espacios:[],
  },
  espacios:[],
  locales:[],
  authenticated: false,
  username: '',
  user: {
    datos_bancarios:{

    },
    datos_comerciales:{

    },
  },
  espacioNr: '',
  localNr: '',
}

const getters = {
  get_espacios: state => state.espacios,
  propietario_get_user: state => state.user,
  espacioNr: state => state.espacioNr,
  propietario_get_locales: state => state.locales,
  // get_localesNew:  state => state.localesNew,
  'get_localesNew' :  function(state){
    let direccion_propietario = ['']
    let k=0
    for( var i=0; i<state.localesNew.length; i++){
      let propíetario = state.localesNew[i]
      direccion_propietario[k++]={
        'id' : k,
        'nombre_comercial' : propíetario.nombre_comercial,
        'espacio_tipo' : propíetario.espacio_tipo,
         'foto' : propíetario.foto
      }
    }
    return direccion_propietario
  },
  get_datos_personales: state => state.user.datos_personales
}

const actions = {
  datos_bancarios(store, datos_bancarios){
    datos_bancarios.pk = store.state.user.datos_personales.pk
    return api.post(apiRoot + '/pilot/datos_bancarios', datos_bancarios)
    .then((response) => store.commit('DATOS_BANCARIOS', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  datos_comerciales(store, datos_comerciales){
    datos_comerciales.pk = store.state.user.datos_personales.pk
    return api.post(apiRoot + '/pilot/datos_comerciales', datos_comerciales)
    .then((response) => store.commit('DATOS_COMERCIALES', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  set_datos_bancarios(store){
    return api.get(apiRoot + '/pilot/datos_bancarios')
    .then((response) => store.commit('SET_DATOS_BANCARIOS', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  set_espacios({state, commit}){
    return api.get(apiRoot + '/pilot/get_espacios/' + state.user.datos_personales.pk)
    .then((response) => commit('SET_ESPACIOS', response))
    .catch((error) => commit('PROPIETARIO_API_FAIL', error))
  },
  PropietarioDatosPersonalesPost({state, commit}, datos_personales){
    datos_personales.username = state.username
    return api.post(apiRoot + '/pilot/update_datos_personales', datos_personales)
    .then((response) => commit('PROPIETARIO_DATOS_PERSONALES_RESPONSE', response))
    .catch((error) => commit('PROPIETARIO_API_FAIL', error))
  },
  getPropietario(store, propietario) {
    this.state.propietario = []
    return api.post(apiRoot + '/pilot/getpropietario/' + propietario)
    .then((response) => store.commit('GET_PROPIETARIO_RESPONSE', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  propietarioPostEspacio(store, espacio) {
    return api.post(apiRoot + '/pilot/addcomercialspace', espacio)
    .then((response) =>{ store.commit('PROPIETARIO_POST_ESPACIO_RESPONSE', response)
                  console.log(response)
    })
      
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  propietarioUploadFoto(store, foto) {
    return api.post_file(apiRoot + '/pilot/uploadaddspacephoto', foto)
    .then((response) => store.commit('PROPIETARIO_POST_FOTO_RESPONSE', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  propietarioUploadFotoLocal(store, foto) {
    return api.post_file(apiRoot + '/pilot/uploadlocalphoto', foto)
    .then((response) => store.commit('PROPIETARIO_POST_FOTO_RESPONSE', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  propietarioAddLocal(store, data){
    return api.post(apiRoot + '/pilot/addLocal', data)
    .then((response) => store.commit('PROPIETARIO_ADD_LOCAL', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  propietarioSetLocales(store){
    return api.get(apiRoot + '/pilot/addLocal')
    .then((response) => store.commit('PROPIETARIO_SET_LOCALES', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
  codigoPostal(store, codigo_postal){
    return api.get(apiRoot + '/api/codigos_postales/v2/codigo_postal/' + codigo_postal)
    .then((response) => store.commit('CODIGO_POSTAL', response))
    .catch((error) => store.commit('PROPIETARIO_API_FAIL', error))
  },
}

const mutations = {
  'CODIGO_POSTAL': function(state, response){
    state.api.codigos_postales.colonias = response.body.colonias
    state.api.codigos_postales.estado = response.body.estado
    state.api.codigos_postales.municipio = response.body.municipio
  },
  'DATOS_BANCARIOS': function(state, response){

  },
  'DATOS_COMERCIALES': function(state, response){
    state.estatus = response.body.estatus

  },
  'SET_DATOS_BANCARIOS': function(state, response){
    state.user.datos_bancarios = response.body.datos_bancarios
    state.user.datos_comerciales = response.body.datos_comerciales
  },
  'SET_USER_PROPIETARIO': function(state, response) {
    state.group = response.body.group
    state.user.datos_personales = response.body.datos_personales
    state.user.datos_personales.pk = response.body.propietario
    state.materiales = response.body.materiales
    state.giros = response.body.giros
    state.tipos = response.body.tipos
    state.estatus = response.body.estatus
    state.restricciones = response.body.restricciones
    state.locales = response.body.locales
    //add
    state.localesNew =  response.body.locales

  },
  'SET_ESPACIOS': function(state, response){
    state.espacios = response.body.espacios[0]
  },
  'SET_ESPACIO': function (state, nr) {
      state.espacioNr = nr
    },
  'SET_LOCAL':function (state, nr){
    state.localNr = nr
  },
  'GET_REGISTER': function(state, response) {
    if(response.body.respuesta == true) {
      var datos_personales_response = JSON.parse(response.body.datos_personales)
      var datos_personales= datos_personales_response[0].fields
      datos_personales.pk = datos_personales_response[0].pk
      state.register = datos_personales
    }
    else {
      state.register = false
    }
  },
  'POST_REGISTER': function(state, response) {
    if(response.body.respuesta == false) {
      state.register.error_message = response.body.mensaje
    }
    else{
      state.register.error_message = false
      state.register.success_message = response.body.mensaje
    }
  },
  'PROPIETARIO_DATOS_PERSONALES_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      state.user.datos_personales = response.body.datos_personales
    }
  },
  'GET_PROPIETARIO_RESPONSE': function(state, response) {
    if (response.body.respuesta == true) {
      store.commit('SAVE_LOCALSTATE')
    }
  },
  'PROPIETARIO_POST_ESPACIO_RESPONSE': function(state, response) {

    state.response = response.body
    state.nr_propietarios = response.body.promociones[0]
    state.nr_espacios = response.body.promociones[1]
  },
  'PROPIETARIO_POST_FOTO_RESPONSE': function(state, response) {
    if(state.response.response == true && state.response.local){
      state.locales.find(item => item.pk === state.response.local.pk).foto = [response.body.respuesta]
    }

  },
  'PROPIETARIO_ADD_LOCAL': function(state, response){
    state.response = response.body    
    if(response.body.api_call == 'actualizar'){
      state.locales.find(item => item.pk === state.response.local.pk).nombre_comercial = response.body.local.nombre_comercial
      state.locales.find(item => item.pk === state.response.local.pk).direccion = response.body.local.direccion
      state.locales.find(item => item.pk === state.response.local.pk).foto = response.body.local.foto
      state.locales.find(item => item.pk === state.response.local.pk).espacio_tipo = response.body.local.espacio_tipo
      state.locales.find(item => item.pk === state.response.local.pk).geo_localidad = response.body.local.geo_localidad
    }
    else{
    state.locales.push(response.body.local)
    }
  },
  'PROPIETARIO_SET_LOCALES': function(state, response){
    state.locales = response.body.locales
  },
  'PROPIETARIO_API_FAIL': function (state, error) {
    console.log(error)
    console.error(error)
  
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
