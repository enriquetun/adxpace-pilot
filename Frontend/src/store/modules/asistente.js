
import api from '@/store/api'
import router from '@/router'
import env from '@/env'

import moment from 'moment'

const apiRoot = env.apiRoot
const state = {
  apiRoot: apiRoot,
  authenticated: false,
  group: '',
  username: '',
  user: {},
  row_nr: -1,
  row_pk: -1,
  row_id1:-1,
  nr_promotores: 0,
  promotores: [''],
  nr_propietarios: [0, 0, 0],
  propietarios: [''],
  propietario: {
    datos_bancarios:{ },
    datos_comerciales: { },
  },
  nr_espacios: 0,
  espacios: [''],
  espacio: [''],
  materiales: [''],
  giros: [''],
  tipos: [''],
  restricciones: [''],
  data_promotores:[],
  data_propietarios:[],
  data_espacios:[],
  data_locales:[],
  edit_propietario_espacio:{},
  edit_propietario_local:{},
  data_propietariosEspacios:[],
  data_propietariosLocales:[]
}

const getters = {
  'asistente_get_promotores' : function(state){
      return state.data_promotores
  },
  'asistente_get_propietarios' : function(state){
    let propietarios = ['']
    let k=0
    for(var i=0; i<state.data_propietarios.length; i++){
      let propietario = state.data_propietarios[i]
      let nombre_promotor = ''
      let username = ''
      if(propietario.promotor.datos_personales){
        nombre_promotor = propietario.promotor.datos_personales.primer_nombre + ' ' + propietario.promotor.datos_personales.apellido_paterno
      }
      if(propietario.usuario){
        username = propietario.usuario.username
      }
      propietarios[k++]={
        'k' : k,
        'id1' : i,
        'pk':propietario.pk,
        'estatus' : propietario.estatus,
        'nombre_promotor': nombre_promotor,
        'nombre' : propietario.datos_personales.primer_nombre +' '+propietario.datos_personales.apellido_paterno+' '+ propietario.datos_personales.apellido_materno,
        'primer_nombre' : propietario.datos_personales.primer_nombre,
        'segundo_nombre' : propietario.datos_personales.segundo_nombre,
        'apellido_paterno' : propietario.datos_personales.apellido_paterno,
        'apellido_materno' : propietario.datos_personales.apellido_materno,
        'estatus_bancario' : propietario.check_bancarios,
        'fecha_de_registro': moment(propietario.fecha_de_registro),
        'telefono' : propietario.datos_personales.telefono,
        'email' : propietario.datos_personales.email,
        'username' : username
      }
      username = null
    }
    return propietarios
  },
  'asistente_get_espacios' : function(state){
    let espacios = ['']
    let k=0
    let foto = null
    let username = ''
    for(var i=0; i<state.data_espacios.length; i++){
      let espacio = state.data_espacios[i]
      if(espacio.foto){
        foto = espacio.foto
      }
      if(espacio.propietario.usuario){
        username = espacio.propietario.usuario.username
      }
      espacios[k++]={
        'id1' : i,
        'k':k,
        'pk' : espacio.pk,
        'propietario' : espacio.propietario.datos_personales.primer_nombre + ' ' + espacio.propietario.datos_personales.apellido_paterno,
        'propietario_telefono': espacio.propietario.datos_personales.telefono,
        'propietario_email': espacio.propietario.datos_personales.email,
        'nombre' : espacio.nombre,
        'registro' : moment(espacio.fecha_de_registro),
        'alto' : espacio.caracteristicas_fisicas.alto,
        'ancho': espacio.caracteristicas_fisicas.ancho,
        'digital' : espacio.caracteristicas_fisicas.digital,
        'foto' : foto,
        'geo_localidad' : espacio.geo_localidad,
        'tipo' : espacio.tipo.designacion,
        'giro' : espacio.giro.designacion,
        'material' : espacio.caracteristicas_fisicas.material.designacion,
        'descripcion' : espacio.caracteristicas_fisicas.descripcion,
        'username': username
      }
      foto = null
      username = null
    }
    return espacios
  },
  'asistente_get_locales' : function(state){
    let locales = ['']
    let k=0
    let foto = null
    let username = ''
    for(var i=0; i<state.data_locales.length; i++){
      let local = state.data_locales[i]
      if(local.foto){
        foto=local.foto
      }
      if(local.propietario.usuario){
        username = local.propietario.usuario.username
      }
      locales[k++]={
        'id1':i,
        'k': k,
        'pk' : local.pk,
        'nombre_comercial' : local.nombre_comercial,
        'espacio_tipo' : local.espacio_tipo,
        'colonia' : local.direccion.colonia,
        'codigo_postal' : local.direccion.codigo_postal,
        'foto' : foto,
        'geo_localidad' : local.geo_localidad,
        'direccion' : local.direccion,
        'propietario': local.propietario.datos_personales.primer_nombre + ' ' + local.propietario.datos_personales.apellido_paterno,
        'propietario_telefono': local.propietario.datos_personales.telefono,
        'propietario_email': local.propietario.datos_personales.email,
        'username': username
      }
      foto = null
      username = null
    }
    return locales
  },
  'asistente_propietario_espacio': function(state){
    let propietario_espacio = ['']
    let k=0
    for(var i=0; i<state.data_propietariosEspacios.length; i++){
      let espacio = state.data_propietariosEspacios[i]
      propietario_espacio[k++]={
        'pk': espacio.pk,
        'i_espacio' : i,
        'nombre': espacio.nombre,
        'registro': moment(espacio.registro),
        'giro' : espacio.giro[1],
        'foto' : espacio.foto
      }
    }
    return propietario_espacio
  },
  // 'asistente_propietario_data_local': function(state){
  //   let propietario_local = ['']
  //   let k=0
  //   for(var i=0; i<state.data_propietariosLocales.length; i++){
  //     let local = state.data_propietariosLocales[i]
  //     propietario_local[k++]={
  //       'pk': local.pk,
  //       'i_local' : i,
  //       'nombre_comercial':local.nombre_comercial,
  //       'municipio' : local.municipio,
  //       'colonia' : local.colonia,
  //       'espacio_tipo' : local.espacio_tipo,
  //       'foto' : local.foto

  //     }
  //   }
  // },
  'asistente_propietario_edit_espacio' : function(state){
    return state.edit_propietario_espacio
  },
  'asistente_propietario_edit_local' : function(state){
    return state.edit_propietario_local
  }
}
const actions = {
  //-------POST
  async AsistenteDatosPersonalesPost({ state, commit }, datos_personales){
    await commit('ASISTENTE_PROPIETARIO_EDIT', datos_personales)
    return api.post(apiRoot + '/pilot/update_datos_personales', datos_personales)
    .then((response) => {
      console.log(response)
      commit('ASISTENTE_DATOS_PERSONALES_RESPONSE', response)})
      .catch((error) =>commit('ASISTENTE_API_FAIL', error))
    },
  async AsistenteEspacioPost({state, commit}, espacio){
      console.log(espacio)
      await commit('ASISTENTE_ESPACIO_EDIT', espacio)
      return api.post(apiRoot + '/pilot/addcomercialspace', espacio)
      .then((response) => {
        // console.log(response)
        commit('ASISTENTE_ESPACIO_RESPONSE', response)
      })
      .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
  async AsistenteLocalPost({state, commit}, local){
      await commit('ASISTENTE_LOCAL_EDIT', local)
      return api.post(apiRoot + '/pilot/addLocal', local)
      .then((response) => {
        commit('ASISTENTE_LOCAL_RESPONSE', response)
      })
      .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
  //---------
    asistenteGetDatosBancarios(store, propietario){
      var request = propietario
      console.log('asistenteGetDatosBancarios: %s', request)
      return api.post(apiRoot + '/pilot/datos_bancarios', request)
      .then((response) => store.commit('ASISTENTE_GET_DATOS_BANCARIOS_RESPONSE', response))
      .catch((error) => store.commit('API_FAIL', error))
    },
    asistenteUploadFoto({state, commit}, foto) {
      return api.post_file(apiRoot + '/pilot/uploadaddspacephoto', foto)
      .then((response) =>{
        commit('ASISTENTE_POST_FOTO_RESPONSE', response)
      })
      .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },
    asistenteUploadFotoLocal({state, commit}, foto) {
      return api.post_file(apiRoot + '/pilot/uploadlocalphoto', foto)
      .then((response) =>{
        commit('ASISTENTE_POST_FOTO_LOCAL_RESPONSE', response)
      })
      .catch((error) => commit('ASISTENTE_API_FAIL', error))
    },

    //-------nuevos accciones de url---------
    asistentePromotores(store){
      return api.get(apiRoot + '/staff/promotores')
      .then((response) =>{
        store.commit('ASISTENTE_PROMOTOR_GET', response)
      })
      .catch((error) =>{
        console.log(error)
      })
    },
    async asistentePropietarios(store, url){
      var instance = this
      if(!url){
        url = apiRoot + '/staff/propietarios?format=json'
      }
      return api.get(url)
      .then((response) =>{
        store.commit('PROPIETARIOS_COUNT', response.body.count)
        for (let i = 0; i < response.body.results.length; i++) {
          store.commit('ASISTENTE_PROPIETARIO_GET', response.body.results[i])
        }
        if(response.body.next){
          instance.dispatch('asistentePropietarios', response.body.next)
        }
      })
      .catch((error)=>{
        console.log(error)
      })
    },
    async asistenteEspacios(store, url){
      var instance = this
      if(!url){
        url = apiRoot + '/staff/espacios?format=json'
      }
      return api.get(url)
      .then((response) => {
        store.commit('ESPACIOS_COUNT', response.body.count)
        for(var i = 0; i < response.body.results.length; i++){
          store.commit('ASISTENTE_ESPACIO_GET', response.body.results[i])
        }
        if(response.body.next){
          instance.dispatch('asistenteEspacios', response.body.next)
        }
      })
      .catch((error) => {
        console.log(error)
      })
    },
    async asistenteLocales(store, url){
      var instance = this
      if(!url)
      {
        url = apiRoot + '/staff/locales?format=json'
      }
      return api.get(url)
      .then((response) =>{
        store.commit('LOCALES_COUNT', response.body.count)
        for (let i = 0; i < response.body.results.length; i++) {
          store.commit('ASISTENTE_LOCALES_GET', response.body.results[i])
        }
        if(response.body.next){
          instance.dispatch('asistenteLocales', response.body.next)
        }

      })
      .catch((error) =>{
        console.log(error)
      })
    },
    async asistenteData(store){
      await store.dispatch('asistentePromotores')
      await store.dispatch('asistentePropietarios', false)
      await store.dispatch('asistenteEspacios', false)
      await store.dispatch('asistenteLocales', false)
    },


    asistentePropietarioEspacios(store, pk){
      return api.get(apiRoot + '/staff/propietarios/espacios/' + pk)
      .then((response) => {

        store.commit('ASISTENTE_PROPIETARIO_DATA_ESPACIO', response.body)
      })
      .catch((error) =>{
        console.log(error)
      })
    },
    asistentePropietarioLocal(store, pk){
      return api.get(apiRoot + '/staff/propietarios/locales/' + pk)
      .then((response) =>{
        store.commit('ASISTENTE_PROPIETARIO_DATA_LOCAL', response.body)
      })
      .catch((error) =>{
        console.log(error)
      })
    },


    asistentePropietarioEditarEspacio(store, pk){
      return api.get(apiRoot + '/staff/espacios/' + pk)
      .then((response) =>{
        store.commit('ASISTENTE_PROPIETARIO_ESPACIO_EDITAR',  response.body)
      }).catch((error)=>{
        console.log(error)
      })
    },
    asistentePropietarioEditarLocal(store, pk){
      return api.get(apiRoot + '/staff/locales/' + pk)
      .then((response) =>{

        store.commit('ASISTENTE_PROPIETARIO_LOCAL_EDITAR', response.body)
      })
      .catch((error) =>{
        console.log(error)
      })
    }
  }
  const mutations = {
    //New Mutations

    'LOCALES_COUNT': function(state, count){
      state.data_locales_count = count
    },
    'PROPIETARIOS_COUNT': function(state, count){
      state.data_propietarios_count = count
    },
    'ESPACIOS_COUNT': function(state, count){
      state.data_espacios_count = count
    },
    'ASISTENTE_PROMOTOR_GET': function(state, response)
    {
      state.data_promotores = response.body
    },
    'ASISTENTE_ESPACIO_GET' : function(state, espacio){
      state.data_espacios.push(espacio)
      
    },
    'ASISTENTE_PROPIETARIO_GET': function(state, propietario){
      state.data_propietarios.push(propietario)
      //console.log(state.data_propietarios)
    },
    'ASISTENTE_LOCALES_GET': function(state, locales){
      // state.data_locales = response.results
      state.data_locales.push(locales)
    },
    'ASISTENTE_PROPIETARIO_ESPACIO_EDITAR': function(state,response){
      state.edit_propietario_espacio = response
    },
    'ASISTENTE_PROPIETARIO_LOCAL_EDITAR' : function(state,response){
      state.edit_propietario_local = response
    },
    'ASISTENTE_PROPIETARIO_DATA_ESPACIO': function(state, response)
    {
      state.data_propietariosEspacios = response
    },
    'ASISTENTE_PROPIETARIO_DATA_LOCAL': function(state, response)
    {
      state.data_propietariosLocales = response
    },

    //---------------------------
    'ASISTENTE_PROPIETARIO_EDIT': function(state, datos_personales){
      state.current_propietario_edit = datos_personales
    },
    'ASISTENTE_DATOS_PERSONALES_RESPONSE': function(state, response){
      //console.log(response.body)
      if(response.body.respuesta == true){
        state.data_propietarios[state.current_propietario_edit.id1].datos_personales.primer_nombre = response.body.datos_personales.primer_nombre
        state.data_propietarios[state.current_propietario_edit.id1].datos_personales.apellido_paterno = response.body.datos_personales.apellido_paterno
        state.data_propietarios[state.current_propietario_edit.id1].datos_personales.apellido_materno = response.body.datos_personales.apellido_materno
        state.data_propietarios[state.current_propietario_edit.id1].datos_personales.telefono = response.body.datos_personales.telefono
        state.data_propietarios[state.current_propietario_edit.id1].datos_personales.email = response.body.datos_personales.email
      }
    },
    //----------------Espacio-----------------------------
    'ASISTENTE_ESPACIO_EDIT': function(state, espacio){
      state.current_espacio_edit = espacio
    },
    'ASISTENTE_ESPACIO_RESPONSE': function(state, response){
      state.response = response.body
      if(response.body.respuesta[0] == true){
          state.data_espacios[state.current_espacio_edit.id1].nombre = state.current_espacio_edit.nombre
          state.data_espacios[state.current_espacio_edit.id1].caracteristicas_fisicas.alto = state.current_espacio_edit.alto
          state.data_espacios[state.current_espacio_edit.id1].caracteristicas_fisicas.ancho = state.current_espacio_edit.ancho
          state.data_espacios[state.current_espacio_edit.id1].caracteristicas_fisicas.digital = state.current_espacio_edit.digital
          state.data_espacios[state.current_espacio_edit.id1].giro.designacion = state.current_espacio_edit.giro
          state.data_espacios[state.current_espacio_edit.id1].tipo.designacion = state.current_espacio_edit.tipo
          state.data_espacios[state.current_espacio_edit.id1].caracteristicas_fisicas.material.designacion =state.current_espacio_edit.material
          state.data_espacios[state.current_espacio_edit.id1].caracteristicas_fisicas.descripcion = state.current_espacio_edit.descripcion
          state.data_espacios[state.current_espacio_edit.id1].geo_localidad = [state.current_espacio_edit.latitud, state.current_espacio_edit.longitud]
      }

    },
    //----------------Local----------------------------
    'ASISTENTE_LOCAL_EDIT':function(state, local){
      state.current_local_edit = local
    },
    'ASISTENTE_LOCAL_RESPONSE': function(state, response){
      state.response = response.body
      if(response.body.response == true){
        // if(state.current_local_edit.source == 1){
        //   state.promotores[state.current_local_edit.id1].propietarios[state.current_local_edit.id2].locales[state.current_local_edit.id3].nombre_comercial = state.response.local.nombre_comercial
        //   state.promotores[state.current_local_edit.id1].propietarios[state.current_local_edit.id2].locales[state.current_local_edit.id3].colonia = state.response.local.direccion.colonia
        //   state.promotores[state.current_local_edit.id1].propietarios[state.current_local_edit.id2].locales[state.current_local_edit.id3].geo_localidad = state.response.local.geo_localidad
        //   console.log(state.promotores[state.current_local_edit.id1].propietarios[state.current_local_edit.id2].locales[state.current_local_edit.id3])
        // }else{
        //   state.autoRegistrados[state.current_local_edit.id1].locales[state.current_local_edit.id2].nombre_comercial =  state.response.local.nombre_comercial
        //   state.autoRegistrados[state.current_local_edit.id1].locales[state.current_local_edit.id2].colonia = state.response.local.direccion.colonia
        //   state.autoRegistrados[state.current_local_edit.id1].locales[state.current_local_edit.id2].geo_localidad = state.response.local.geo_localidad
        //   console.log(state.autoRegistrados[state.current_local_edit.id1].locales[state.current_local_edit.id2])
        // }
        state.data_locales[state.current_local_edit.id1].nombre_comercial = state.response.local.nombre_comercial
        state.data_locales[state.current_local_edit.id1].direccion.colonia = state.response.local.direccion.colonia
        state.data_locales[state.current_local_edit.id1].espacio_tipo = state.response.local.espacio_tipo
        state.data_locales[state.current_local_edit.id1].geo_localidad = state.response.local.geo_localidad
      }

    },
    //-----------------------Importante--------------------------
    'SET_USER_ASISTENTE': function(state, response){
      state.giros = response.body.giros
      state.tipos = response.body.tipos
      state.materiales = response.body.materiales
    },
    'ASISTENTE_SET_ROW_NR': function (state, row_id) {
      state.row_nr = row_id
    },
    'ASISTENTE_PK': function(state, valor_pk){
      state.row_pk = valor_pk
    },
    'ASISTENTE_ID': function(state, id1)
    {
      state.row_id1 = id1
    },
    // 'ASISTENTE_UPDATE_ESPACIO': function(state, espacio) {
    //   let index = state.row_nr
    //   console.log('valor_delIndexUpdate_espacio: %s', index)
    //   state.espacios[index].nombre = espacio.nombre
    //   state.espacios[index].geo_localidad = espacio.geo_localidad
    //   state.espacios[index].giro[0] = espacio.giro
    //   state.espacios[index].tipo[0] = espacio.tipo
    //   state.espacios[index].material[0] = espacio.material
    //   state.espacios[index].digital = espacio.digital
    //   state.espacios[index].descripcion = espacio.descripcion
    //   state.espacios[index].alto = espacio.alto
    //   state.espacios[index].ancho = espacio.ancho
    //   state.espacios[index].actualizacion = moment(espacio.actualizacion)

    // },
    'ASISTENTE_GET_DATOS_BANCARIOS_RESPONSE': function(state, response){
      state.propietario.datos_bancarios = response.body.datos_bancarios
      state.propietario.datos_comerciales = response.body.datos_comerciales
    },

    // 'ASISTENTE_POST_ESPACIO_RESPONSE': function(state, response) {
    //   state.response = response.body
    //   console.log('post espacio response: %s', response.body)
    // },
    'ASISTENTE_POST_FOTO_RESPONSE': function(state, response) {
      //  console.log(response.body.respuesta[0])
      //  console.log(response.body.respuesta[1])
      if (response.body.respuesta[0] == true) {
        // if(state.current_espacio_edit.source == 1){
        //   state.promotores[state.current_espacio_edit.id1].propietarios[state.current_espacio_edit.id2].espacios[state.current_espacio_edit.id3].foto = response.body.respuesta[1]
        // }else
        // {
        //   state.autoRegistrados[state.current_espacio_edit.id1].espacios[state.current_espacio_edit.id2].foto = response.body.respuesta[1]
        // }
        state.data_espacios[state.current_espacio_edit.id1].foto = response.body.respuesta[1]
      }
    },
    'ASISTENTE_POST_FOTO_LOCAL_RESPONSE': function(state, response) {
      console.log(response.body)
      state.data_locales[state.current_local_edit.id1].foto = response.body.respuesta
      //state.promotores[state.current_local_edit.id1].propietarios[state.current_local_edit.id2].locales[state.current_local_edit.id3].foto = response.body.respuesta
    },
    'ASISTENTE_API_FAIL': function (state, error) {
      console.error(error)
    }
  }

  export default {
    state,
    getters,
    actions,
    mutations
  }
