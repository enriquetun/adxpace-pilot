import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/store/api'
import router from '@/router'
import env from '@/env'
import login from '@/store/modules/login'
import vendedor from '@/store/modules/vendedor'
import propietario from '@/store/modules/propietario'
import asistente from '@/store/modules/asistente'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules:{
    propietario,
    vendedor,
    asistente,
    login
  }
})

export default store
