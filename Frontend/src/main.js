import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store.js'

Vue.config.productionTip = false

require('@/assets/saas/app.scss')

require('daterangepicker/daterangepicker.css')

require('holderjs')

require('daterangepicker/daterangepicker.js')

require('moment/moment.js')

import('@/assets/css/carousel.css')

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {

}

// store.dispatch('storeCSRF', getCookie('csrftoken'))

var vm = new Vue({
  el: '#app',
  store: store,
  router: router,
  template: '<App/>',
  components: { App }
})

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// Menu class

$(function () {
  var nav=$('.nav-item')
  nav.click(function(){
    nav.removeClass('active')
    $(this).addClass('active')
  })
})
