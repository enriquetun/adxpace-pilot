
#!/bin/bash

docker-compose -f docker-compose.prod.pilot.adxpace.yml build
docker-compose -f docker-compose.prod.pilot.adxpace.yml up --remove-orphans -d nginx django vuejs postgres codigos_postales_api
