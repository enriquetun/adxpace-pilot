#!/bin/bash

pg_dump -U postgres addixdb | gzip > /var/lib/postgresql/addixdb.gz
pg_dump -U postgres admindb | gzip > /var/lib/postgresql/admindb.gz
