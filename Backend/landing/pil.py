from PIL import Image, ExifTags
from pilot import models


def run_me():
    try:
        espacio = models.EspacioPublicitario.objects.get(pk=2464)
        foto = models.Foto.objects.get(pk=espacio.foto.pk)
        print('HOLA')
        file = foto.src.open()

        image=Image.open(file)
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation]=='Orientation':
                break
        exif=dict(image._getexif().items())
        print(exif[orientation])

        if exif[orientation] == 3:
            image=image.rotate(180, expand=True)
        elif exif[orientation] == 6:
            image=image.rotate(270, expand=True)
        elif exif[orientation] == 8:
            image=image.rotate(90, expand=True)

        foto.src = image
        foto.save()
        print(foto)
        image.save(file)
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation]=='Orientation':
                break
        exif=dict(image._getexif().items())
        print(exif[orientation])
        image.close()

    except (AttributeError, KeyError, IndexError):
        # cases: image don't have getexif
        pass
