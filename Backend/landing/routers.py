

class LandingDBRouter(object):
    """
    A router to control all database operations on models in the
    addixdb application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read addixdb models go to addixdb db.
        """
        if model._meta.app_label == 'landing':
            return 'addixdb'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write addixdb models go to addixdb db.
        """
        if model._meta.app_label == 'landing':
            return 'addixdb'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the addixdb app is involved.
        """
        if obj1._meta.app_label == 'landing' or \
           obj2._meta.app_label == 'landing':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the landing app only appears in the 'addixdb'
        database.
        """
        if app_label == 'landing':
            return db == 'addixdb'
        return None
