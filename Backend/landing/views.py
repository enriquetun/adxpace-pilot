from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import render,render_to_response
from django.middleware.csrf import get_token
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.views.decorators.csrf import ensure_csrf_cookie
from pilot import models
import json

@ensure_csrf_cookie
def foto(request):
    return render(request, 'provisional/foto.html')


def index(request):
    link = 'https://' + request.get_host() + '/pilot'
    context = {
    'link': link
    }
    return render(request, 'provisional/index.html', context)

def daniela(request):
    return render(request, 'provisional/daniela.html')

def propuesta(request, slug):
    return HttpResponse(slug)

def dashboard(request):

    return render(request, 'provisional/dashboard.html')

def test(request):
    url = 'https://maps.googleapis.com/maps/api/geocode/json?'
    params = {'latlng': '21.0323194, -89.6093164', 'key': 'AIzaSyDAmvouc8wiOPjIXp_CdOLlJvtYTL6RwTo'}
    r = requests.get(url, params=params)
    response = r.json()
    data = response['results']

    for x in range(0, len(data)):
        if (data[x]['address_components'][0]['types'][0] == 'postal_code'):
            zip_code = data[x]['address_components'][0]['long_name']
            break

    return JsonResponse({'zip_code': zip_code}, safe=False)


    return JsonResponse(response['results'][3]['address_components'][0]['types'], safe=False)

def zip(request):

    return render(request, 'provisional/zip_code.html')

def collapse(request):

    return render(request, 'provisional/collapse.html')

def casetas(request):
    # data = RadioFormula.objects.all()
    # casetas = {
    # 'data': [
    # {
    # 'nombre': data.nombre,
    # 'serie': data.serie,
    # 'direccion': data.direccion,
    # 'entre': data.entre,
    # 'y': data.y,
    # 'colonia': data.colonia,
    # 'delegacion': data.delegacion,
    # 'ciudad': data.ciudad,
    # 'longitud': data.longitud,
    # 'latitud': data.latitud,
    # } for data in data
    # ]
    # }

    data = EspacioPublicitario.objects.all()
    espacios = {
    'data':[
    {
    'latitud': data.geo_localidad.latitud,
    'longitud': data.geo_localidad.longitud,
    } for data in data
    ]
    }
    return JsonResponse({
    # 'casetas': casetas,
    'espacios': espacios
    })

def filtros(request):
    tipos = Tipo.objects.all()
    giros = Giro.objects.all()

    return JsonResponse({
    'tipos': [{
    'pk': tipo.pk,
    'designacion': tipo.designacion,
    'count': check_espacio_tipos(tipo)
    } for tipo in tipos],
    'giros': [{
    'pk': giro.pk,
    'designacion': giro.designacion,
    'count': check_espacio_giros(giro)
    }for giro in giros]
    })

@csrf_exempt
def espacios_filtrados(request):
    tipos = request.POST.getlist('tipos_filter[]')
    giros = request.POST.getlist('giros_filter[]')
    digital = request.POST.get('digital')
    if (digital == '1' or digital == 'true'):
        digital= True
    elif (digital == '0' or digital == 'false'):
        digital = False

    espacios = EspacioPublicitario.objects.filter(tipo__pk__in=tipos, giro__pk__in=giros, caracteristicas_fisicas__digital=digital)
    espacios_count = espacios.count()
    # espacios_count = EspacioPublicitario.objects.filter(caracteristicas_fisicas__digital=True).count()

    return JsonResponse({
    'espacios': [{
    'pk': espacio.pk,
    'giro': espacio.giro.designacion,
    'tipo': espacio.tipo.designacion,
    'nombre': espacio.nombre,
    'latitud': espacio.geo_localidad.latitud,
    'longitud': espacio.geo_localidad.longitud,
    'digital': espacio.caracteristicas_fisicas.digital
    }for espacio in espacios],
    'espacios_count': espacios_count,
    'tipos': tipos,
    'giros': giros,
    'digital': digital
    })

def check_espacio_tipos(tipo):
    return EspacioPublicitario.objects.filter(tipo=tipo).count()

def check_espacio_giros(giro):
    return EspacioPublicitario.objects.filter(giro=giro).count()


@csrf_exempt
def get_csrf(request):
    token = get_token(request)
    return JsonResponse({'csrf' : token})

def curves(request):
    sucursales = CurvesSucursal.objects.all()
    context = {
    'sucursales': sucursales,
    }
    return render(request, 'provisional/curves/curves.html', context)

def curves_propietarios(request):
    # propietarios_list = [567, 596, 600, 643, 607, 661, 664, 665, 546,
    # 549, 547, 601, 620, 621, 622, 574, 575, 630, 624, 553, 552, 561,
    # 572, 594, 595, 642, 644, 645, 656, 660, 649, 662, 663, 557, 569,
    # 565, 566, 573, 577, 578, 579, 580, 581, 582, 583, 591, 608, 610,
    # 613, 614, 616, 617, 618, 625, 626, 627, 636, 641, 648, 650, 658, 659, 670]
    propietarios_list = [567, 596, 600, 643, 607, 661, 664, 546,
    549, 547, 601, 620, 621, 622, 574, 575, 630, 624, 553, 561,
    572, 594, 642, 644, 645, 656, 660, 649, 662, 663, 557, 569,
    566, 573, 579, 582, 583, 591, 608,
    613, 617, 618, 625, 627, 636, 641, 648, 650, 659]
    propietarios_object = models.Propietario.objects.filter(pk__in=propietarios_list)
    propietarios = [{
    'pk': propietario.pk,
    'nombre': propietario.datos_personales.primer_nombre,
    'apellido_paterno': propietario.datos_personales.apellido_paterno,
    } for propietario in propietarios_object]
    context = {
    'propietarios': propietarios,
    }
    return render(request, 'provisional/curves/curves_propietarios.html', context)
    # return JsonResponse([{
    # 'pk': promotor.pk,
    # }for promotor in promotores], safe=False)

def curves_propietario(request, id):
    propietario = models.Propietario.objects.get(pk=id)
    espacios = models.EspacioPublicitario.objects.filter(propietario=propietario)
    locales = models.Local.objects.filter(propietario=propietario)
    context = {
    'pk': propietario.pk,
    'nombre_completo': propietario.datos_personales.primer_nombre + ' ' + propietario.datos_personales.apellido_paterno,
    'nr_espacios': espacios.count(),
    'nr_locales': locales.count(),
    'espacios': [{
    'pk': espacio.pk,
    'nombre': espacio.nombre,
    'foto': espacio.get_foto(),
    }for espacio in espacios],
    'locales': [{
    'pk': local.pk,
    'nombre': local.nombre_comercial,
    'foto': local.get_foto(),
    }for local in locales]
    }
    return render(request, 'provisional/curves/curves_propietario.html', context)

def curves_local(request, id):
    local = models.Local.objects.get(pk=id)
    context = {
    'local': local,
    'id': local.pk,
    'nombre': local.nombre_comercial,
    'foto': local.get_foto(),
    'latitud': local.geo_localidad.latitud,
    'longitud': local.geo_localidad.longitud,
    }
    return render(request, 'provisional/curves/curves_local.html', context)

def curves_espacio(request, id):
    espacio = models.EspacioPublicitario.objects.get(pk=id)
    context = {
    'espacio': espacio,
    'foto': espacio.get_foto(),
    'latitud': espacio.geo_localidad.latitud,
    'longitud': espacio.geo_localidad.longitud,
    }
    return render(request, 'provisional/curves/curves_espacio.html', context)

def curves_form(request):
    return render(request, 'provisional/curves/curves_form.html')

def curves_sucursales(request):
    if request.method == 'GET':
        sucursales = CurvesSucursal.objects.all()
        context = {
            'sucursales' : sucursales
        }
        return render(request, 'provisional/curves/curves_sucursales.html', context)

    elif request.method == 'POST':
        propietario = models.Propietario.objects.get(pk=request.POST.get('propietario'))
        sucursal = CurvesSucursal.objects.get(pk=request.POST.get('sucursal'))
        asociados = CurvesAsociados(curves_sucursal=sucursal, propietario=propietario).save()
        sucursales = CurvesSucursal.objects.all()
        context = {
            'sucursales' : sucursales
        }

        return render(request, 'provisional/curves/curves_sucursales.html', context)

def curves_sucursal(request, id):
    sucursal = CurvesSucursal.objects.get(pk=id)
    # context = {
    #     'latitud' : sucursal.geo_localidad.latitud,
    #     'longitud': sucursal.geo_localidad.longitud,
    #     'pk': sucursal.pk,
    # }
    context = {
        'sucursal':sucursal
    }
    return render(request, 'provisional/curves/curves_sucursal.html', context)

def curves_asociados(request, id):
    propietarios = [{
    'pk': asociado.propietario.pk,
    'espacios':[{
    'pk': espacio.pk,
    'latitud': espacio.geo_localidad.latitud,
    'longitud': espacio.geo_localidad.longitud,
    'foto' : espacio.get_foto(),
    'nombre' : espacio.nombre,
    }for espacio in asociado.propietario.espacios()],
    'locales':[{
    'pk': local.pk,
    'latitud': local.geo_localidad.latitud,
    'longitud': local.geo_localidad.longitud,
    'foto': local.get_foto(),
    'nombre' : local.nombre_comercial,
    }for local in asociado.propietario.locales()],
    }for asociado in CurvesAsociados.objects.filter(curves_sucursal=id)]

    return JsonResponse({
    'propietarios': propietarios,
    })


def dashboard(request):

    return render(request, 'provisional/dashboard.html')

def collapse(request):

    return render(request, 'provisional/collapse.html')

def casetas(request):
    # data = RadioFormula.objects.all()
    # casetas = {
    # 'data': [
    # {
    # 'nombre': data.nombre,
    # 'serie': data.serie,
    # 'direccion': data.direccion,
    # 'entre': data.entre,
    # 'y': data.y,
    # 'colonia': data.colonia,
    # 'delegacion': data.delegacion,
    # 'ciudad': data.ciudad,
    # 'longitud': data.longitud,
    # 'latitud': data.latitud,
    # } for data in data
    # ]
    # }

    data = models.EspacioPublicitario.objects.all()
    espacios = {
    'data':[
    {
    'latitud': data.geo_localidad.latitud,
    'longitud': data.geo_localidad.longitud,
    } for data in data
    ]
    }
    return JsonResponse({
    # 'casetas': casetas,
    'espacios': espacios
    })

def filtros(request):
    tipos = models.Tipo.objects.all()
    giros = models.Giro.objects.all()

    return JsonResponse({
    'tipos': [{
    'pk': tipo.pk,
    'designacion': tipo.designacion,
    'count': check_espacio_tipos(tipo)
    } for tipo in tipos],
    'giros': [{
    'pk': giro.pk,
    'designacion': giro.designacion,
    'count': check_espacio_giros(giro)
    }for giro in giros]
    })

@csrf_exempt
def espacios_filtrados(request):
    tipos = request.POST.getlist('tipos_filter[]')
    giros = request.POST.getlist('giros_filter[]')
    digital = request.POST.get('digital')
    if (digital == '1' or digital == 'true'):
        digital= True
    elif (digital == '0' or digital == 'false'):
        digital = False

    espacios = models.EspacioPublicitario.objects.filter(tipo__pk__in=tipos, giro__pk__in=giros, caracteristicas_fisicas__digital=digital)
    espacios_count = espacios.count()
    # espacios_count = EspacioPublicitario.objects.filter(caracteristicas_fisicas__digital=True).count()

    return JsonResponse({
    'espacios': [{
    'pk': espacio.pk,
    'giro': espacio.giro.designacion,
    'tipo': espacio.tipo.designacion,
    'nombre': espacio.nombre,
    'latitud': espacio.geo_localidad.latitud,
    'longitud': espacio.geo_localidad.longitud,
    'digital': espacio.caracteristicas_fisicas.digital
    }for espacio in espacios],
    'espacios_count': espacios_count,
    'tipos': tipos,
    'giros': giros,
    'digital': digital
    })

def check_espacio_tipos(tipo):
    return models.EspacioPublicitario.objects.filter(tipo=tipo).count()

def check_espacio_giros(giro):
    return models.EspacioPublicitario.objects.filter(giro=giro).count()

def lety(request):
    locales = models.Local.objects.all()
    context = {
    'locales': locales,
    }
    return render(request, 'provisional/lety.html', context)

def dixon(request):
    sucursales = DixonSucursal.objects.all()
    context = {
    'sucursales': sucursales,
    }
    return render(request, 'provisional/dixon/dixon.html', context)

def dixon_asociar(request):
    if (request.method == 'GET'):
        return render(request, 'provisional/dixon/dixon_form.html')
    else:
        sucursal = request.POST.get('sucursal')
        latitud = request.POST.get('latitud')
        longitud = request.POST.get('longitud')
        geo_localidad = models.Geolocalidad(latitud=latitud, longitud=longitud)
        geo_localidad.save()
        dixon_sucursal = DixonSucursal(sucursal=sucursal, geo_localidad=geo_localidad)
        dixon_sucursal.save()
        sucursales = DixonSucursal.objects.all()
        context = {
        'sucursales': sucursales,
        }
        return render(request, 'provisional/dixon/dixon.html', context)

def dixon_sucursales(request):
    if request.method == 'GET':
        sucursales = DixonSucursal.objects.all()
        context = {
            'sucursales' : [{
            'pk': sucursal.pk,
            'sucursal': sucursal.sucursal,
            'geo_localidad': sucursal.geo_localidad,
            'contador': sucursal.contador()
            } for sucursal in sucursales]
        }
        return render(request, 'provisional/dixon/dixon_sucursales.html', context)
    else:
        sucursal = DixonSucursal.objects.get(pk=request.POST.get('sucursal'))
        espacio = models.EspacioPublicitario.objects.get(pk=request.POST.get('espacio'))

        if DixonAsociados.objects.filter(espacio_publicitario=espacio).exists():
            raise Http404("Espacio asignado previamente")

        DixonAsociados(dixon_sucursal=sucursal, espacio_publicitario=espacio).save()
        sucursales = DixonSucursal.objects.all()
        context = {
            'sucursales' : sucursales
        }
        return render(request, 'provisional/dixon/dixon_sucursales.html', context)

def dixon_sucursal(request, id):
    sucursal = DixonSucursal.objects.get(pk=id)
    # context = {
    #     'latitud' : sucursal.geo_localidad.latitud,
    #     'longitud': sucursal.geo_localidad.longitud,
    #     'pk': sucursal.pk,
    # }
    context = {
        'sucursal':sucursal
    }
    return render(request, 'provisional/dixon/dixon_sucursal.html', context)

def dixon_espacio(request, id):
    espacio = models.EspacioPublicitario.objects.get(pk=id)
    context = {
    'espacio': espacio,
    'foto': espacio.get_foto(),
    'latitud': espacio.geo_localidad.latitud,
    'longitud': espacio.geo_localidad.longitud,
    }
    return render(request, 'provisional/dixon/dixon_espacio.html', context)



def dixon_asociados(request, id):
    asociados = DixonAsociados.objects.filter(dixon_sucursal=id)
    espacios = [{
    'pk': asociado.espacio_publicitario.pk,
    'latitud': asociado.espacio_publicitario.geo_localidad.latitud,
    'longitud': asociado.espacio_publicitario.geo_localidad.longitud,
    'foto' : asociado.espacio_publicitario.get_foto(),
    'nombre' : asociado.espacio_publicitario.nombre,
    }for asociado in asociados]

    return JsonResponse({
    'espacios': espacios,
    })

def dixon_roma(request):
    sucursal = DixonSucursal.objects.get(pk=6)
    context = {
        'sucursal':sucursal
    }
    return render(request, 'provisional/dixon/dixon_sucursal_roma.html', context)

def dixon_roma_asociados(request):
    asociados = DixonAsociados.objects.filter(dixon_sucursal=6).exclude(precio=None)
    # espacios_list = [2334, 2339, 2340, 2341, 2342, 2345, 2352, 2353, 2445, 2446, 2447, 2448, 2449, 2453, 2454, 2456, 2458, 2459, 2462, 2463, 2464]
    # precios_list = [105000, 63200, 63200, 87000, 69500, 179000, 7500, 7500, 7000, 7000, 7000, 7000, 7000, 14000, 7000, 7000, 7000, 34000, 28000, 30000, 30000]
    espacios = [{
    'pk': asociado.espacio_publicitario.pk,
    'latitud': asociado.espacio_publicitario.geo_localidad.latitud,
    'longitud': asociado.espacio_publicitario.geo_localidad.longitud,
    'foto' : asociado.espacio_publicitario.get_foto(),
    'nombre' : asociado.espacio_publicitario.nombre,
    'precio': asociado.precio
    }for asociado in asociados]

    return JsonResponse({
    'espacios': espacios,
    })
