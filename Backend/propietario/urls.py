from django.urls import path
from . import views

urlpatterns = [
    path('', views.Index.as_view(), name='index_propietario'),
    path('login/', views.PropietarioLoginView.as_view(), name='login_propietario'),
    path('datos/', views.PropietarioView.as_view()),
    path('espacios/', views.EspaciosView.as_view()),
    path('locales/', views.LocalesView.as_view()),
    path('tipos/', views.TiposView.as_view()),
    path('giros/', views.GirosView.as_view()),
    path('materiales/', views.MaterialesView.as_view())
]
