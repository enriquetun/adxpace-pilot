from django.contrib.auth.views import LoginView, LogoutView
from django.http import Http404
from django.shortcuts import render
from django.views.generic.base import TemplateView
from rest_framework import exceptions, status
from rest_framework.generics import ListAPIView, CreateAPIView, ListCreateAPIView, UpdateAPIView, RetrieveAPIView, RetrieveUpdateAPIView
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from . import serializers, mixins
from pilot import models
from django.http import HttpResponse
from django.utils.translation import gettext as _

class PropietarioLoginView(LoginView):
    template_name = 'propietario/login.html'
    next = '/propietario/'
    redirect_field_name = 'index_propietario'

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""
        return self.render_to_response(self.get_context_data())

    def get_redirect_url(self):
        """Return the user-originating redirect URL if it's safe."""
        redirect_to = self.next
        return redirect_to

class Index(mixins.PropietarioMixin, TemplateView):
    template_name = 'propietario/index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

class PropietarioView(RetrieveUpdateAPIView):
    serializer_class = serializers.PropietarioSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def get_queryset(self, *args, **kwargs):
        username = self.request.user.username
        propietario = models.Propietario.objects.get(usuario__username=username)
        queryset = propietario

        return queryset

    def get_object(self):

        queryset = self.get_queryset()

        try:
            obj = queryset
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

class EspaciosView(ListAPIView, CreateAPIView):
    serializer_class = serializers.EspacioPublicitarioSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def get_queryset(self, *args, **kwargs):
        username = self.request.user.username
        propietario = models.Propietario.objects.get(usuario__username=username)
        espacios_publicitarios = models.EspacioPublicitario.objects.filter(propietario=propietario)
        queryset = espacios_publicitarios

        return queryset

class LocalesView(ListAPIView, CreateAPIView):
    serializer_class = serializers.LocalSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def get_queryset(self, *args, **kwargs):
        username = self.request.user.username
        propietario = models.Propietario.objects.get(usuario__username=username)
        locales = models.Local.objects.filter(propietario=propietario)
        queryset = locales

        return queryset

class TiposView(ListAPIView):
    serializer_class = serializers.TipoSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    queryset = models.Tipo.objects.all()

class GirosView(ListAPIView):
    serializer_class = serializers.GiroSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    queryset = models.Giro.objects.all()

class MaterialesView(ListAPIView):
    serializer_class = serializers.MaterialSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    queryset = models.Material.objects.all()
