from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.validators import UniqueValidator
from pilot import models

class DatosPersonalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DatosPersonales
        fields = ('primer_nombre', 'segundo_nombre', 'apellido_paterno', 'apellido_materno', 'email', 'telefono')

class DatosBancariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DatosBancarios
        fields = ('banco', 'numero_de_cuenta', 'titular')

class DatosComercialesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DatosComerciales
        fields = ('rfc', 'factura')

class FotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Foto
        fields = ('src',)

class PropietarioSerializer(serializers.ModelSerializer):
    datos_personales = DatosPersonalesSerializer()
    datos_bancarios = DatosBancariosSerializer()
    datos_comerciales = DatosComercialesSerializer()
    class Meta:
        model = models.Propietario
        fields = ('datos_personales', 'datos_bancarios', 'datos_comerciales')

    def update(self, instance, validated_data):
        propietario = instance
        datos_personales_data = validated_data.pop('datos_personales', False)
        datos_comerciales_data = validated_data.pop('datos_comerciales', False)
        datos_bancarios_data = validated_data.pop('datos_bancarios', False)

        if(datos_personales_data):
            datos_personales = DatosPersonalesSerializer(instance.datos_personales, data=datos_personales_data, partial=True)
            datos_personales.is_valid()
            datos_personales.save()

        if(datos_comerciales_data):
            if(instance.datos_comerciales):
                datos_comerciales = DatosComercialesSerializer(instance.datos_comerciales, data=datos_comerciales_data, partial=True)
                datos_comerciales.is_valid()
                datos_comerciales.save()
            else:
                datos_comerciales = models.DatosComerciales.objects.create(**datos_comerciales_data)
                instance.datos_comerciales = datos_comerciales
                instance.save()

        if(datos_bancarios_data):
            if(instance.datos_bancarios):
                datos_bancarios = DatosBancariosSerializer(instance.datos_bancarios, data=datos_bancarios_data, partial=True)
                datos_bancarios.is_valid()
                datos_bancarios.save()
            else:
                datos_bancarios = models.DatosBancarios.objects.create(**datos_bancarios_data)
                instance.datos_bancarios = datos_bancarios
                instance.save()

        return propietario

class GeolocalidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Geolocalidad
        fields = ('latitud', 'longitud')

class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Material
        fields = ('designacion',)
        # read_only_fields = ('designacion',)
        extra_kwargs = {
        'designacion':{
            'validators': []
        },
        }

class CaracteristicasFisicasSerializer(serializers.ModelSerializer):
    material = MaterialSerializer()

    class Meta:
        model = models.CaracteristicasFisicas
        fields = ('alto', 'ancho', 'digital', 'descripcion', 'material')

class GiroSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Giro
        fields = ('pk', 'designacion',)
        # read_only_fields = ('pk', 'designacion',)
        extra_kwargs = {
        'designacion':{
            'validators': []
        },
        }

class TipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tipo
        fields = ('pk', 'designacion')
        # read_only_fields = ('pk', 'designacion')
        extra_kwargs = {
        'designacion':{
            'validators': []
        },
        }

class DireccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Direccion
        fields = ('calle', 'numero_exterior', 'numero_interior', 'colonia', 'codigo_postal', 'municipio', 'estado')


class EspacioPublicitarioSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()
    caracteristicas_fisicas = CaracteristicasFisicasSerializer()
    giro = GiroSerializer()
    tipo = TipoSerializer()
    foto = FotoSerializer()

    class Meta:
        model = models.EspacioPublicitario
        fields = ('pk', 'nombre', 'fecha_de_registro', 'fecha_de_actualizacion', 'geo_localidad', 'caracteristicas_fisicas', 'giro', 'tipo', 'foto')
        read_only_fields = ('pk', 'giro', 'tipo', 'fecha_de_registro', 'fecha_de_actualizacion')
        extra_kwargs = {
        'tipo':{
            'validators': [UniqueValidator(queryset=models.Tipo.objects.all())]
        },
        }


    def create(self, validated_data):
        geo_localidad_data = validated_data.pop('geo_localidad', False)
        foto_data = validated_data.pop('foto', False)

        giro_data = validated_data.pop('giro', False)
        tipo_data = validated_data.pop('tipo', False)
        nombre = validated_data.pop('nombre', False)

        caracteristicas_fisicas_data = validated_data.pop('caracteristicas_fisicas', False)
        caracteristicas_fisicas_data['material'] = models.Material.objects.get(**caracteristicas_fisicas_data['material'])

        geo_localidad = models.Geolocalidad.objects.create(**geo_localidad_data)
        foto = models.Foto.objects.create(**foto_data)
        caracteristicas_fisicas = models.CaracteristicasFisicas.objects.create(**caracteristicas_fisicas_data)
        giro = models.Giro.objects.get(**giro_data)
        tipo = models.Tipo.objects.get(**tipo_data)

        username = self.context['request'].user.username
        propietario = models.Propietario.objects.get(usuario__username=username)

        espacio_publicitario = models.EspacioPublicitario(
        propietario = propietario,
        nombre = nombre,
        geo_localidad = geo_localidad,
        giro = giro,
        tipo = tipo,
        caracteristicas_fisicas = caracteristicas_fisicas,
        )

        espacio_publicitario.save()

        return espacio_publicitario


class LocalSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()
    foto = FotoSerializer()
    direccion = DireccionSerializer()

    class Meta:
        model = models.Local
        fields = ('pk', 'nombre_comercial', 'espacio_tipo', 'geo_localidad', 'foto', 'direccion',)
        read_only_fields = ('pk',)

    def create(self, validated_data):
        geo_localidad_data = validated_data.pop('geo_localidad', False)
        foto_data = validated_data.pop('foto', False)
        direccion_data = validated_data.pop('direccion', False)
        nombre_comercial = validated_data.pop('nombre_comercial', False)
        espacio_tipo = validated_data.pop('espacio_tipo', False)

        geo_localidad = models.Geolocalidad.objects.create(**geo_localidad_data)
        foto = models.Foto.objects.create(**foto_data)
        direccion = models.Direccion.objects.create(**direccion_data)

        username = self.context['request'].user.username
        propietario = models.Propietario.objects.get(usuario__username=username)

        local = models.Local(
        propietario = propietario,
        geo_localidad = geo_localidad,
        foto = foto,
        direccion = direccion,
        nombre_comercial = nombre_comercial,
        espacio_tipo = espacio_tipo,
        )

        local.save()

        return local


class CreateLocalSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()
    foto = FotoSerializer()
    direccion = DireccionSerializer()

    class Meta:
        model = models.Local
        fields = ('nombre_comercial', 'espacio_tipo', 'geo_localidad', 'foto', 'direccion',)

    def create(self, validated_data):
        geo_localidad_data = validated_data.pop('geo_localidad', False)
        foto_data = validated_data.pop('foto', False)
        direccion_data = validated_data.pop('direccion', False)
        nombre_comercial = validated_data.pop('nombre_comercial', False)
        espacio_tipo = validated_data.pop('espacio_tipo', False)

        geo_localidad = models.Geolocalidad.objects.create(**geo_localidad_data)
        foto = models.Foto.objects.create(**foto_data)
        direccion = models.Direccion.objects.create(**direccion_data)

        username = self.context['request'].user.username
        propietario = models.Propietario.objects.get(usuario__username=username)

        local = models.Local(
        propietario = propietario,
        geo_localidad = geo_localidad,
        foto = foto,
        direccion = direccion,
        nombre_comercial = nombre_comercial,
        espacio_tipo = espacio_tipo,
        )

        local.save()

        return local
