from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

class PropietarioPassesTestMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.groups.filter(name__in=['propietario']).exists()

class PropietarioMixin(LoginRequiredMixin, PropietarioPassesTestMixin):
    login_url = '/propietario/login/'
    redirect_field_name = 'redirect_to'

# class VendedorMixin():
#     pass
