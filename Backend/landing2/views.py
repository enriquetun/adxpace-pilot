from django.shortcuts import render

def index(request):
    return render(request, 'landing2/index.html',)

def privacidad(request):
    return render(request, 'landing2/privacidad.html')
