from django.urls import path
from pilot.views import logout_view
from . import views

urlpatterns = [
    path('giros/', views.Giros.as_view()),
    path('tipos/', views.Tipos.as_view()),
    path('ooh_tipos/', views.TiposOoh.as_view()),
    path('materiales/', views.Materiales.as_view()),
    path('direccion/', views.Direccion.as_view()),
]
