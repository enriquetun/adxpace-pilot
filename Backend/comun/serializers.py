from django.contrib import auth
from rest_framework import serializers
import pilot

class GiroSerializer(serializers.ModelSerializer):
    class Meta:
        model = pilot.models.Giro
        fields = ('pk', 'designacion',)

class TipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = pilot.models.Tipo
        fields = ('pk', 'designacion',)
        extra_kwargs = {
        'designacion':{
            'validators': []
        },
        }

class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = pilot.models.Material
        fields = ('pk', 'designacion',)

class UserSerializer(serializers.ModelSerializer):    

    class Meta:
        model = auth.models.User
        fields = ('username', 'password')
