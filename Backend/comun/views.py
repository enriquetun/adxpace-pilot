from django.contrib.auth import models as auth_models, logout, views, login as auth_login, logout as auth_logout
from django.contrib import messages
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic import View
from rest_framework import generics, permissions
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
import googlemaps
from . import serializers
import pilot
from django.contrib.gis.geos import Point
from django.http import Http404

class TiposOoh(View):

    def get(self, request, *args, **kwargs):
        fields = pilot.models.EspacioInstitucional.objects.values('data__tipo').distinct()
        json = [{'tipo': field['data__tipo']} for field in fields]
        return JsonResponse(json, safe=False)


class Giros(generics.ListAPIView):
    serializer_class =  serializers.GiroSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.Giro.objects.all().order_by('designacion')

class Materiales(generics.ListAPIView):
    serializer_class =  serializers.MaterialSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = giros = pilot.models.Material.objects.all().order_by('designacion')

class Tipos(generics.ListAPIView):
    serializer_class =  serializers.TipoSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.Tipo.objects.all().order_by('designacion')

class Direccion(View):

    def get(self, request, *args, **kwargs):
        if(request.GET.get('direccion')):
            point = self.set_point()
            return JsonResponse(point, safe=False)
        else:
            raise Http404()

    def set_point(self):
        if(self.request.GET.get('direccion')):
            gmaps = googlemaps.Client(key='AIzaSyDAmvouc8wiOPjIXp_CdOLlJvtYTL6RwTo')
            geocode_result = gmaps.geocode(self.request.GET.get('direccion') + ', Mexico')
            if(geocode_result):
                lat = geocode_result[0]['geometry']['location']['lat']
                lng = geocode_result[0]['geometry']['location']['lng']
                json = {'latitud': lat, 'longitud': lng}
                return json
                # point = Point(lng, lat)
                #
                # return point
            else:
                return {}


class Index(TemplateView):
    pass

class LogoutView(RedirectView):

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

class LoginView(views.LoginView):
    template_name = 'login/login.html'

    def get_group(self):
        self.extra_context = {'group': self.group}

    def get(self, request, *args, **kwargs):
        self.get_group()
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        self.get_group()
        form = self.get_form()
        if form.is_valid() and auth_models.User.objects.filter(username=request.POST.get('username'), groups__name=self.group).exists():
            return self.form_valid(form)
        else:
            if form.is_valid():
                user =  auth_models.User.objects.get(username=request.POST.get('username'))
                user_groups = user.groups.values('name')
                messages.error(request, 'No tiene los persmisos necesarios para entrar como ' + self.group + '. Favor de revisar sus persmisos y entrar a la página correcta.')
                for group in user_groups:
                    link = request.build_absolute_uri('/') + group['name']
                    messages.info(request, 'Para ingresar como ' + group['name'] + ', ingrese a la siguiente liga: ' + '<a href=' + link + '>Iniciar sesión</a>')
                    # + 'a la siguiente liga <a>' + request.build_absolute_uri('/') + group '.'
            else:
                messages.error(request, 'Usuario o contraseña incorrectos')
            # messages.add_message(request, messages.INFO, 'Hello world.')
            return self.form_invalid(form)

    def get_redirect_url(self):
        """Return the user-originating redirect URL if it's safe."""
        redirect_to = self.next
        return redirect_to
