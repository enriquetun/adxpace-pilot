# backend

> Backend for Addix

# Database setup 

python manage.py migrate

# Database superuser setup

python manage.py createsuperuser

# Build docker image

docker-compose build

# Run docker image

docker-compose up
