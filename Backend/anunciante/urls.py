from django.urls import path
from . import views
from pilot.views import logout_view

urlpatterns = [
    path('', views.AnuncianteIndex.as_view(), name='index_anunciante'),
    path('cliente/', views.AnuncianteCliente.as_view()),
    path('relaciones/', views.RelacionesView.as_view()),
    path('relaciones/inventarios/<int:relacion>', views.InventariosView.as_view()),
    path('espacios/', views.Espacios.as_view()),
    path('locales/', views.Locales.as_view()),
    path('espacios_institucionales/', views.EspaciosInstitucionales.as_view()),
    path('login/', views.AnuncianteLoginView.as_view(), name='login_anunciante'),
    path('logout/', views.AnuncianteLogoutView.as_view(), name='logout_anunciante'),
]
