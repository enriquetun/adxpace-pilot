from django.apps import AppConfig


class AnuncianteConfig(AppConfig):
    name = 'anunciante'
