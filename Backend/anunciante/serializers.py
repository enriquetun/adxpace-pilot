from rest_framework import serializers
from vendedor.models import RelacionClienteUsuario

class RelacionClienteUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = RelacionClienteUsuario
        fields = ('pk', 'user', 'cliente_json')
