from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

class AnunciantePassesTestMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.groups.filter(name__in=['anunciante']).exists()

class AnuncianteMixin(LoginRequiredMixin, AnunciantePassesTestMixin):
    login_url = '/anunciante/login/'
    redirect_field_name = 'redirect_to'

# class AnuncianteMixin():
#     pass
