from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout
from django.shortcuts import render, render_to_response
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.middleware.csrf import get_token
from rest_framework.generics import ListAPIView, CreateAPIView, ListCreateAPIView, UpdateAPIView
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from vendedor.views import Relaciones, Inventarios
from vendedor.models import RelacionesClientesJson, RelacionClienteUsuario
from . import models
from . import serializers
from .mixins import AnuncianteMixin
import staff, comun

class AnuncianteIndex(AnuncianteMixin, comun.views.Index):
    template_name = 'anunciante/index.html'
    group = 'anunciante'
    auth_url = '/anunciante'

class AnuncianteLoginView(comun.views.LoginView):
    next = '/anunciante/'
    group = 'anunciante'
    auth_url = '/vendedor'

class AnuncianteLogoutView(comun.views.LogoutView):
    url = '/anunciante/login/'

class AnuncianteCliente(ListAPIView):
    serializer_class = serializers.RelacionClienteUsuarioSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    queryset = RelacionClienteUsuario.objects.all()

    def get_queryset(self, *args, **kwargs):
        username = self.request.user.username
        queryset = RelacionClienteUsuario.objects.filter(user=username)
        return queryset


class RelacionesView(Relaciones):

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        if RelacionClienteUsuario.objects.filter(user=self.request.user.username).exists():
            try:
                relacion = RelacionClienteUsuario.objects.get(user=self.request.user.username)
                cliente_json = relacion.cliente_json
                relaciones_clientes_json = RelacionesClientesJson.objects.filter(
                cliente_json=cliente_json)
            except:
                relaciones_clientes_json = RelacionesClientesJson.objects.filter(
                cliente_json__name='xxxxxxxxxxxxxxxx')

        elif (self.request.user.username == 'jennyl'):
            relaciones_clientes_json = RelacionesClientesJson.objects.filter(
            cliente_json__pk__in=[4])
        elif (self.request.user.username == 'dixon'):
            relaciones_clientes_json = RelacionesClientesJson.objects.filter(
            cliente_json__pk__in=[9, 10])
        elif (self.request.user.username == 'neria'):
            relaciones_clientes_json = RelacionesClientesJson.objects.filter(
            cliente_json__pk__in=[16])
        elif (self.request.user.username == 'sabormex'):
            relaciones_clientes_json = RelacionesClientesJson.objects.filter(
            cliente_json__pk__in=[22])

        elif (self.request.user.username == 'vasconia1'):
            relaciones_clientes_json = RelacionesClientesJson.objects.filter(
            cliente_json__pk__in=[26])

        elif (self.request.user.username == 'pgarcia'):
            relaciones_clientes_json = RelacionesClientesJson.objects.filter(
            cliente_json__pk__in=[23])
        else:
            relaciones_clientes_json = RelacionesClientesJson.objects.all()
        queryset_list = relaciones_clientes_json

        return queryset_list

    def test_func(self):
        return self.request.user.groups.filter(name__in=['anunciante']).exists()

class InventariosView(Inventarios):
    def test_func(self):
        return self.request.user.groups.filter(name__in=['anunciante']).exists()

class Espacios(staff.views.Espacios):
    pass

class Locales(staff.views.Locales):
    pass

class EspaciosInstitucionales(staff.views.EspaciosInstitucionales):
    pass
