from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.files.storage import FileSystemStorage
from django.core.files.storage import default_storage
from twilio.rest import Client
from django.core.serializers import serialize
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from django.http import Http404
from django.utils import timezone
from django.core.mail import send_mail
from django.db import transaction, connections
from json import loads
import math
import sys
import os.path
import logging
# Get an instance of a logger
logger = logging.getLogger(__name__)

from Backend.settings import MEDIA_URL

from pilot import models
from pilot import forms


def user_registered(username):
    return User.objects.filter(username=username).exists()

def get_usuario(username):
    return User.objects.get(username=username)

def get_propietario_por_usuario(usuario):
    return models.Propietario.objects.get(usuario=usuario.username)

def get_propietario_por_id(id):
    return models.Propietario.objects.get(id=id)

def get_promotor(usuario):
    try:
        return models.Promotor.objects.get(usuario=usuario.username)
    except ObjectDoesNotExist:
        return False

def get_foto_espacio(espacio):
    try:
        return models.Foto.objects.get(espacio_publicitario=espacio).src.url
    except:
        return False
def get_foto_local(local):
    try:
        return local.foto.src.url,
    except:
        return False

def get_promociones(promotor):
    return models.Promociones.objects.filter(promotor=promotor)

def registrados(promotores):
    return sum([registrado for registrado in (promotores[1][i]['promociones'][1] for i in range(len(promotores[1])))])

# def get_material(espacio, cursor):
    # cursor.execute("select designacion from material where id =\
    #               (select material_id from caracteristicas_fisicas where id = %s)", [espacio])
    # return cursor.fetchone()


#modificar promotores para obtener la lista de objetos
def get_promotores():
    promotores = models.Promotor.objects.all()
    propietarios_promociones = models.Promociones.objects.all()
    propietarios_list = [promocion.propietario.pk for promocion in propietarios_promociones]
    propietarios_autoregistro = models.Propietario.objects.exclude(pk__in = propietarios_list)
    return {
    'autoregistro' : [{
        'pk': autoregistro.pk,
            'estatus' : autoregistro.estatus,
            'estatus_bancario': autoregistro.check_bancarios(),
            'primer_nombre' : autoregistro.datos_personales.primer_nombre,
            'apellido_paterno' : autoregistro.datos_personales.apellido_paterno,
            'apellido_materno' : autoregistro.datos_personales.apellido_materno,
            'espacios': [{
                'pk': espacio.pk,
                'nombre' : espacio.nombre,
                'local' : get_local(espacio.local),
                'geo_localidad' : [espacio.geo_localidad.latitud, espacio.geo_localidad.longitud],
                'giro' :  [espacio.giro.pk, espacio.giro.designacion],
                'tipo' : [espacio.tipo.pk, espacio.tipo.designacion],
                'material' : [espacio.caracteristicas_fisicas.material.pk, espacio.caracteristicas_fisicas.material.designacion],
                'digital' : espacio.caracteristicas_fisicas.digital,
                'descripcion' : espacio.caracteristicas_fisicas.descripcion,
                'dimensiones' : [espacio.caracteristicas_fisicas.alto, espacio.caracteristicas_fisicas.ancho],
                'foto' : get_foto_espacio(espacio),
                'registro' : espacio.fecha_de_registro,
                'actualizacion' : espacio.fecha_de_actualizacion,
            }for espacio in autoregistro.espacios()],
            'locales':[{
                'pk': local.pk,
                'geo_localidad' : [local.geo_localidad.latitud, local.geo_localidad.longitud],
                'nombre_comercial': local.nombre_comercial,
                'espacio_tipo': local.espacio_tipo,
                'calle': local.direccion.calle,
                'numero_exterior': local.direccion.numero_exterior,
                'numero_interior': local.direccion.numero_interior,
                'colonia': local.direccion.colonia,
                'codigo_postal': local.direccion.codigo_postal,
                'municipio': local.direccion.municipio,
                'estado': local.direccion.estado,
                'foto': local.get_foto(),
            }for local in autoregistro.locales()],
            'fecha_de_registro' : autoregistro.fecha_de_registro,
            'fecha_de_actualizacion' : autoregistro.fecha_de_actualizacion,
            'email' : autoregistro.datos_personales.email,
            'telefono' : autoregistro.datos_personales.telefono
    }for autoregistro in propietarios_autoregistro],
    'promotores' : [{
        'pk' : promotor.pk,
        'primer_nombre' : promotor.datos_personales.primer_nombre,
        'apellido_paterno' : promotor.datos_personales.apellido_paterno,
        'apellido_materno' : promotor.datos_personales.apellido_materno,
        'promociones' : promociones_por_promotor(promotor),
        'propietarios' : [{
            'pk': propietario.pk,
            'estatus' : propietario.estatus,
            'estatus_bancario': propietario.check_bancarios(),
            'primer_nombre' : propietario.datos_personales.primer_nombre,
            'apellido_paterno' : propietario.datos_personales.apellido_paterno,
            'apellido_materno' : propietario.datos_personales.apellido_materno,
            'espacios': [{
                'pk': espacio.pk,
                'nombre' : espacio.nombre,
                'local' : get_local(espacio.local),
                'geo_localidad' : [espacio.geo_localidad.latitud, espacio.geo_localidad.longitud],
                'giro' :  [espacio.giro.pk, espacio.giro.designacion],
                'tipo' : [espacio.tipo.pk, espacio.tipo.designacion],
                'material' : [espacio.caracteristicas_fisicas.material.pk, espacio.caracteristicas_fisicas.material.designacion],
                'digital' : espacio.caracteristicas_fisicas.digital,
                'descripcion' : espacio.caracteristicas_fisicas.descripcion,
                'dimensiones' : [espacio.caracteristicas_fisicas.alto, espacio.caracteristicas_fisicas.ancho],
                'foto' : get_foto_espacio(espacio),
                'registro' : espacio.fecha_de_registro,
                'actualizacion' : espacio.fecha_de_actualizacion,
            }for espacio in propietario.espacios()],
            'locales':[{
                'pk': local.pk,
                'geo_localidad' : [local.geo_localidad.latitud, local.geo_localidad.longitud],
                'nombre_comercial': local.nombre_comercial,
                'espacio_tipo': local.espacio_tipo,
                'calle': local.direccion.calle,
                'numero_exterior': local.direccion.numero_exterior,
                'numero_interior': local.direccion.numero_interior,
                'colonia': local.direccion.colonia,
                'codigo_postal': local.direccion.codigo_postal,
                'municipio': local.direccion.municipio,
                'estado': local.direccion.estado,
                'foto': local.get_foto(),
            }for local in propietario.locales()],
            'fecha_de_registro' : propietario.fecha_de_registro,
            'fecha_de_actualizacion' : propietario.fecha_de_actualizacion,
            'email' : propietario.datos_personales.email,
            'telefono' : propietario.datos_personales.telefono
        }for propietario in promotor.propietarios()],
        'email' : promotor.datos_personales.email,
        'telefono' : promotor.datos_personales.telefono,
        }for promotor in promotores]
    }

def get_promotores_old():
    try:
        promotores = models.Promotor.objects.all()
        nr_promotores = promotores.count()
        return [nr_promotores,
                [{
                'pk' : promotor.pk,
                'primer_nombre' : promotor.datos_personales.primer_nombre,
                'apellido_paterno' : promotor.datos_personales.apellido_paterno,
                'apellido_materno' : promotor.datos_personales.apellido_materno,
                'promociones' : promociones_por_promotor(promotor),
                'email' : promotor.datos_personales.email,
                'telefono' : promotor.datos_personales.telefono,
                } for promotor in promotores]]
    except:
        return [0, False]

def espacios_por_promocion(promotor):
    promociones = get_promociones(promotor)
    return [{
    'propietario': promocion.propietario.pk,
    'nr_espacios': models.EspacioPublicitario.objects.filter(propietario=promocion.propietario).count(),
    'nr_locales': models.Local.objects.filter(propietario=promocion.propietario).count()
    }for promocion in promociones]

def get_locales(propietario):
    try:
        locales = models.Local.objects.filter(propietario=propietario)
        return [{
        'pk': local.pk,
        'nombre_comercial': local.nombre_comercial,
        'espacio_tipo': local.espacio_tipo,
        'foto': get_foto_local(local),
        'geo_localidad':[local.geo_localidad.latitud, local.geo_localidad.longitud],
        'direccion': {
        'calle': local.direccion.calle,
        'numero_exterior': local.direccion.numero_exterior,
        'numero_interior': local.direccion.numero_interior,
        'colonia': local.direccion.colonia,
        'municipio': local.direccion.municipio,
        'estado': local.direccion.estado,
        'codigo_postal': local.direccion.codigo_postal,
        },
        } for local in locales]
    except:
        return [False]
def get_local(local):
    if(local):
        return local.pk
    else:
        return ''

def espacios_por_propietario(propietario):
    # cursor = connections['addixdb'].cursor()
    espacios = models.EspacioPublicitario.objects.filter(propietario=propietario).order_by('-fecha_de_registro')
    # try:
    return [[{
            'espacio': espacio.pk,
            'nombre': espacio.nombre,
            'local': get_local(espacio.local),
            'geo_localidad': [espacio.geo_localidad.latitud, espacio.geo_localidad.longitud],
            'giro': [espacio.giro.pk, espacio.giro.designacion],
            'tipo': [espacio.tipo.pk, espacio.tipo.designacion],
            'material': [espacio.caracteristicas_fisicas.material.pk, espacio.caracteristicas_fisicas.material.designacion],
            'digital': espacio.caracteristicas_fisicas.digital,
            'descripcion': espacio.caracteristicas_fisicas.descripcion,
            'dimensiones': [espacio.caracteristicas_fisicas.alto, espacio.caracteristicas_fisicas.ancho],
            'foto': get_foto_espacio(espacio),
            'registro': espacio.fecha_de_registro,
            'actualizacion': espacio.fecha_de_actualizacion,
            } for espacio in espacios], len(espacios)]
    # return espacios
    # finally:
    #     cursor.close()
# ---------------------------------------------Modificar-----------------------------------------------
def get_espacios(propietario):
    espacios = models.EspacioPublicitario.objects.filter(propietario=propietario)
    return [{
    'pk': espacio.pk,
    'nombre':espacio.nombre,
    'local':get_local(espacio.local),
    'registro':espacio.fecha_de_registro

    }for espacio in espacios]

def get_espacios_old():
    return [[[propietario.pk, propietario.datos_personales.primer_nombre + ' ' + propietario.datos_personales.apellido_paterno],
            espacios_por_propietario(propietario=propietario.pk)] for propietario in models.Propietario.objects.all()]

def promociones_por_promotor(promotor):
    nr_promociones = [0,0,0]
    if bool(promotor):
        promociones = get_promociones(promotor)
        nr_promociones[0] = promociones.count()
        propietarios = [promocion.propietario for promocion in promociones]
        ids = [promocion.propietario_id for promocion in promociones]
        nr_promociones[1] = models.Propietario.objects.filter(id__in = ids, estatus = 'R').count()
        nr_promociones[2] = models.EspacioPublicitario.objects.filter(propietario__in = propietarios).count()
    return nr_promociones

def propietario(propietario):
    return {'pk' : propietario.id,
            'estatus' : propietario.estatus,
            'primer_nombre' : propietario.datos_personales.primer_nombre,
            'apellido_paterno' : propietario.datos_personales.apellido_paterno,
            'apellido_materno': propietario.datos_personales.apellido_materno,
            'espacios': models.EspacioPublicitario.objects.filter(propietario = propietario.id).count(),
            'fecha_de_registro': propietario.fecha_de_registro,
            'fecha_de_actualizacion': propietario.fecha_de_actualizacion,
            'email' : propietario.datos_personales.email,
            'telefono' : propietario.datos_personales.telefono,
            }

def propietarios_por_promotor(promotor):
    propietarios = []
    promociones = get_promociones(promotor)
    if bool(promociones):
        propietarios = [propietario(promocion.propietario) for promocion in promociones]
    return propietarios, len(propietarios)
#-------------------------------------------------Modificar------------------------------------
def get_propietarios(promotor):
    promociones = models.Promociones.objects.filter(promotor=promotor)
    return [{
    'pk': promocion.propietario.pk,
    'estatus':promocion.estatus,
    'primer_nombre':promocion.datos_personales.primer_nombre,
    'apellido_paterno':promocion.datos_personales.apellido_paterno,
    'apellido_materno':promocion.datos_personales.apellido_materno,
    'espacios': get_espacios(promocion.propietario),
    'fecha_de_registro':promocion.fecha_de_registro,
    'fecha_de_actualizacion':promocion.fecha_de_actualizacion,
    'email':promocion.datos_personales.email,
    'telefono':promocion.datos_personales.telefono

    }for promocion in promociones]

def get_propietarios_old():
    promotores = get_promotores_old()
    pks = [promotores[1][i].get('pk') for i in range(promotores[0])]
    promovidos = [propietarios_por_promotor(pk) for pk in pks]
    pks = [propietario.get('propietario') for propietario in models.Promociones.objects.all().values('propietario')]
    auto_reg = [propietario(auto_reg) for auto_reg in models.Propietario.objects.exclude(pk__in=pks)]
    auto_reg = [auto_reg, len(auto_reg)]
    return {'promovidos': promovidos, 'auto_reg': auto_reg}

def register_propietario(username, password, propietario):
    with transaction.atomic():
        auth_user = create_auth_user(username, password)
        usuario = create_propietario(username, propietario)
        return True

def create_propietario(username, propietario):
    grupo = models.Grupo.objects.get(nombre='propietario')
    usuario = models.Usuario(username=username, grupo=grupo)
    usuario.save()
    # Falta diferenciar de un link que viene sin usuario pre-registrado
    propietario.usuario = usuario
    propietario.estatus = 'I'
    propietario.save()
    return True

def create_auth_user(username, password):
    auth_user = User(username=username)
    auth_user.set_password(password)
    auth_user.save()
    group = Group.objects.get(name='propietario')
    auth_user.groups.add(group)
    auth_user.save()
    return True

def update_local(request):
    with transaction.atomic():
        local = models.Local.objects.get(pk=request.POST.get('pk'))
        local.espacio_tipo = request.POST.get('espacio_tipo')
        local.geo_localidad.latitud = request.POST.get('latitud')
        local.geo_localidad.longitud = request.POST.get('longitud')
        local.geo_localidad.save()
        local.giro = models.Giro.objects.get(pk=request.POST.get('giro'))
        local.direccion.calle = request.POST.get('calle')
        local.direccion.numero_interior = request.POST.get('numero_interior')
        local.direccion.numero_exterior = request.POST.get('numero_exterior')
        local.direccion.colonia = request.POST.get('colonia')
        local.direccion.codigo_postal = request.POST.get('codigo_postal')
        local.direccion.municipio = request.POST.get('municipio')
        local.direccion.estado = request.POST.get('estado')
        local.direccion.save()
        local.nombre_comercial = request.POST.get('nombre_comercial')
        local.save()
        return local

def add_new_local(request):
    propietario = models.Propietario.objects.get(id=request.POST.get('propietario'))
    espacio_tipo = request.POST.get('espacio_tipo')
    direccion = add_direccion(request)
    geo_localidad = add_geolocalidad(request)
    giro = models.Giro.objects.get(pk=request.POST.get('giro'))
    local = models.Local(
    nombre_comercial=request.POST.get('nombre_comercial'),
    espacio_tipo=request.POST.get('espacio_tipo'),
    propietario=propietario,
    direccion=direccion,
    geo_localidad=geo_localidad,
    giro=giro
    )
    local.save()
    return local

def add_direccion(request):
    direccion = models.Direccion(
    calle=request.POST.get('calle'),
    numero_interior=request.POST.get('numero_interior'),
    numero_exterior=request.POST.get('numero_exterior'),
    colonia=request.POST.get('colonia'),
    codigo_postal=request.POST.get('codigo_postal'),
    municipio=request.POST.get('municipio'),
    estado=request.POST.get('estado'),
    )
    direccion.save()
    return direccion

def add_geolocalidad(request):
    geo_localidad = models.Geolocalidad(longitud = request.POST.get('longitud'),
                                        latitud = request.POST.get('latitud'))
    geo_localidad.save()
    return geo_localidad

def get_datos_personales(propietario, datos_personales):
    datospersonales = {
        'pk' : propietario.pk,
        'estatus' : propietario.estatus,
        'primer_nombre' : datos_personales.primer_nombre,
        'apellido_paterno': datos_personales.apellido_paterno,
        'apellido_materno': datos_personales.apellido_materno,
        'email': datos_personales.email,
        'telefono': datos_personales.telefono,
    }
    return datospersonales

def get_datos_bancarios(propietario):
    datos_bancarios = {}
    datos_comerciales = {}
    if not propietario.datos_bancarios:
        datos_bancarios['exists'] = False
    else:
        datos_bancarios['exists'] = True
        datos_bancarios['banco'] = propietario.datos_bancarios.banco
        datos_bancarios['numero_de_cuenta'] = propietario.datos_bancarios.numero_de_cuenta
        datos_bancarios['titular'] = propietario.datos_bancarios.titular
    if not propietario.datos_comerciales:
        datos_comerciales['exists'] = False
    else:
        datos_comerciales['exists'] = True
        datos_comerciales['factura'] = propietario.datos_comerciales.factura
        datos_comerciales['rfc'] = propietario.datos_comerciales.rfc
    return datos_bancarios, datos_comerciales

def set_datos_bancarios(request):
    datos_bancarios = {}
    propietario = get_propietario_por_usuario(request.user)
    if not propietario.datos_bancarios:
        datos_bancarios = models.DatosBancarios()
    else:
        datos_bancarios = propietario.datos_bancarios
    datos_bancarios.banco = request.POST.get('banco')
    datos_bancarios.numero_de_cuenta = request.POST.get('numero_de_cuenta')
    datos_bancarios.titular = request.POST.get('titular')
    datos_bancarios.save()
    propietario.datos_bancarios = datos_bancarios
    propietario.save()

def update_datospersonales(request):
    with transaction.atomic():
        usuario = models.Usuario.objects.get(username=request.user.username)
        propietario = models.Propietario.objects.get(pk=request.POST.get('pk'))
        if bool(propietario):
            if (usuario.grupo.nombre == 'staff' or (usuario.grupo.nombre == 'promotor' and propietario.estatus == 'P') or
                (usuario.grupo.nombre == 'propietario' and propietario.estatus != 'P')):
                datos_personales = models.DatosPersonales.objects.get(pk=propietario.datos_personales.pk)
                datos_personales.apellido_paterno = request.POST.get('apellido_paterno')
                datos_personales.apellido_materno = request.POST.get('apellido_materno')
                datos_personales.primer_nombre = request.POST.get('primer_nombre')
                datos_personales.email = request.POST.get('email')
                datos_personales.telefono = request.POST.get('telefono')
                datos_personales.save()
                return get_datos_personales(propietario, datos_personales)

def get_materiales():
    return [[material.designacion, material.id] for material in models.Material.objects.all()]

def get_giros():
    return [[giro.designacion, giro.id] for giro in models.Giro.objects.all()]

def get_restricciones():
    return [[restriccion.designacion, restriccion.id] for restriccion in models.Restriccion.objects.all()]

def get_tipos():
    return [[tipo.designacion, tipo.id] for tipo in models.Tipo.objects.all()]

def archivar_foto(data_foto):
    f = forms.FotoEspacioComercialForm(data_foto.POST)
    # if f.is_valid():
    if True:
        foto_espacio = data_foto.FILES['imageFile']
        # fs = default_storage
        # filename = fs.get_available_name(foto_espacio.name)
        # archivo = fs.save(filename, foto_espacio)
        # filepath = '/media/' + filename
        espacio_publicitario = models.EspacioPublicitario.objects.get(id = data_foto.POST.get('espacioImagen'))
        foto_espacio_publicitario = models.Foto(src = foto_espacio, espacio_publicitario = espacio_publicitario)
        foto_espacio_publicitario.save()
        return True
    else:
        return False

def archivar_foto_local(data_foto):
    f = forms.FotoEspacioComercialForm(data_foto.POST)
    if True:
        foto_espacio = data_foto.FILES['imageFile']
        # fs = FileSystemStorage()
        # filename = fs.get_available_name(foto_espacio.name)
        # archivo = fs.save(filename, foto_espacio)
        # filepath = MEDIA_URL + filename
        foto = models.Foto(src = foto_espacio)
        foto.save()
        local = models.Local.objects.get(id=data_foto.POST.get('localImagen'))
        local.foto = foto
        local.save()
        return foto.src.url
    else:
        return False

def nuevo_espacio_comercial(espacio):
    f = forms.EspacioPublicitarioForm(espacio)
    if f.is_valid():
        with transaction.atomic():
            propietario = models.Propietario.objects.get(pk = espacio.get('propietario'))
            caracteristicas_fisicas = models.CaracteristicasFisicas(alto = espacio.get('alto'),
                                                                    ancho = espacio.get('ancho'),
                                                                    digital = loads(espacio.get('digital')),
                                                                    material = models.Material.objects.get(id = espacio.get('material')),
                                                                    descripcion = espacio.get('descripcion'))
            caracteristicas_fisicas.save()
            geo_localidad = models.Geolocalidad(longitud = espacio.get('longitud'),
                                                latitud = espacio.get('latitud'))
            geo_localidad.save()
            clasificacion = models.Clasificacion()
            clasificacion.save()
            giro = models.Giro.objects.get(id = espacio.get('giro'))
            tipo = models.Tipo.objects.get(id = espacio.get('tipo'))
            if not espacio.get('local'):
                espacio_publicitario = models.EspacioPublicitario(propietario = propietario,
                                                                  geo_localidad = geo_localidad,
                                                                  nombre = espacio.get('nombre'),
                                                                  giro = giro,
                                                                  tipo = tipo,
                                                                  caracteristicas_fisicas = caracteristicas_fisicas)
            else:
                local = models.Local.objects.get(id= espacio.get('local'))
                espacio_publicitario = models.EspacioPublicitario(propietario = propietario,
                                                                  geo_localidad = geo_localidad,
                                                                  nombre = espacio.get('nombre'),
                                                                  local = local,
                                                                  giro = giro,
                                                                  tipo = tipo,
                                                                  caracteristicas_fisicas = caracteristicas_fisicas)


            espacio_publicitario.save()
            return [True, espacio_publicitario.id]
    else:
        return [False]

def actualizar_foto(data_foto):
    f = forms.FotoEspacioComercialForm(data_foto.POST)
    # if f.is_valid():
    if True:
        with transaction.atomic():
            fs = FileSystemStorage()
            foto_espacio = data_foto.FILES['imageFile']
            # filename = fs.get_available_name(foto_espacio.name)
            espacio_publicitario = models.EspacioPublicitario.objects.get(id = data_foto.POST.get('espacioImagen'))
            filepath = get_foto_espacio(espacio_publicitario)
            if (filepath == False):
                foto_espacio_publicitario = models.Foto(src = foto_espacio, espacio_publicitario = espacio_publicitario)
                foto_espacio_publicitario.save()
            else:

                foto_espacio_publicitario = models.Foto.objects.get(espacio_publicitario = espacio_publicitario)
                foto_espacio_publicitario.src = foto_espacio
                foto_espacio_publicitario.save()
            return foto_espacio_publicitario.src.url
    else:
        return False

def actualizar_espacio_comercial(espacio):
    f = forms.EspacioPublicitarioForm(espacio)
    # if f.is_valid():
    if True:
        with transaction.atomic():
            espacio_publicitario = models.EspacioPublicitario.objects.get(pk = espacio.get('espacio'))
            espacio_publicitario.caracteristicas_fisicas.alto = espacio.get('alto')
            espacio_publicitario.caracteristicas_fisicas.ancho = espacio.get('ancho')
            espacio_publicitario.caracteristicas_fisicas.digital = loads(espacio.get('digital'))
            espacio_publicitario.caracteristicas_fisicas.material =  models.Material.objects.get(id = espacio.get('material'))
            espacio_publicitario.caracteristicas_fisicas.descripcion = espacio.get('descripcion')
            espacio_publicitario.caracteristicas_fisicas.save()
            espacio_publicitario.geo_localidad.longitud = espacio.get('longitud')
            espacio_publicitario.geo_localidad.latitud = espacio.get('latitud')
            espacio_publicitario.geo_localidad.save()
            espacio_publicitario.nombre = espacio.get('nombre')
            if espacio.get('local') != '':
                espacio_publicitario.local = models.Local.objects.get(id=espacio.get('local'))
            espacio_publicitario.giro = models.Giro.objects.get(id = espacio.get('giro'))
            espacio_publicitario.tipo = models.Tipo.objects.get(id=espacio.get('tipo'))
            espacio_publicitario.fecha_de_actualizacion = timezone.now()
            espacio_publicitario.save()
            return [True, espacio_publicitario.id]
    else:
        return [False]

def enviar_correo_registro(email, telefono, pk, base_url):

    if (email):
        subject = 'Registro ADXPACE'
        message = 'Para registrarse de click aquí: https://' + base_url + '/pilot/register/' + str(pk)
        from_email = 'register@adxpace.com'
        to_email = email
        send_mail(subject,message,from_email,[to_email],fail_silently=False,)

    if(telefono):
        account_sid = 'ACc3eece31b9a15e2767d22ec4c10a438a'
        auth_token = 'f160d91d83b8e29d9f415be5e7cd07ae'
        body_message = 'Registro adxpace: https://' + base_url + '/pilot/register/' + str(pk)
        twilio_number = '+18643107614'
        recipient = '+52' + telefono
        client = Client(account_sid, auth_token)
        message = client.messages.create(
                              body=body_message,
                              from_=twilio_number,
                              to=recipient
                          )

def nuevo_propietario(datos_personales, promotor):
    f = forms.DatosPersonalesForm(datos_personales.POST)
    base_url = datos_personales.get_host()
    if f.is_valid():
        with transaction.atomic():
                datos_personales = models.DatosPersonales(primer_nombre = datos_personales.POST.get('primer_nombre'),
                                                        segundo_nombre = datos_personales.POST.get('segundo_nombre'),
                                                        apellido_paterno = datos_personales.POST.get('apellido_paterno'),
                                                        apellido_materno = datos_personales.POST.get('apellido_materno'),
                                                        email = datos_personales.POST.get('email'),
                                                        telefono = datos_personales.POST.get('telefono'))
                datos_personales.save()
                propietario = models.Propietario(datos_personales = datos_personales)
                propietario.estatus = 'P'
                propietario.save()
                promocion = models.Promociones(promotor = promotor, propietario = propietario)
                promocion.save()
                enviar_correo_registro(datos_personales.email, datos_personales.telefono, propietario.pk, base_url)
                return [True, get_datos_personales(propietario, datos_personales)]
    else:
        return False

@ensure_csrf_cookie
def index(request):
    return render(request, 'landing/index.html')

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')
    return JsonResponse({'loggedout' : 'True'})

@require_http_methods(["POST"])
@ensure_csrf_cookie
def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            user_object = get_usuario(username)
            user = {'username' : user_object.username, 'first_name': user_object.first_name,
                    'last_name' : user_object.last_name,'email' : user_object.email}
            if (request.user.groups.filter(name__in=['propietario']).exists()):
                propietario = get_propietario_por_usuario(user_object)
                datos_personales = get_datos_personales(propietario, propietario.datos_personales)
                giros = get_giros()
                restricciones = get_restricciones()
                materiales = get_materiales()
                tipos = get_tipos()
                locales = get_locales(propietario)
                return JsonResponse({
                    'authenticated' : True,
                    'group': 'propietario',
                    'user': user_object.pk,
                    'propietario': propietario.pk,
                    'datos_personales': datos_personales,
                    'locales': locales,
                    'giros' : giros,
                    'tipos' : tipos,
                    'materiales' : materiales,
                    'restricciones' : restricciones,
                    'estatus' : propietario.estatus,
                })
            elif (request.user.groups.filter(name__in=['promotor']).exists()):
                promotor = get_promotor(user_object)
                promociones = promociones_por_promotor(promotor)
                espacios = espacios_por_promocion(promotor)
                propietarios, nr_propietarios = propietarios_por_promotor(promotor)
                giros = get_giros()
                restricciones = get_restricciones()
                materiales = get_materiales()
                tipos = get_tipos()
                return JsonResponse({
                    'authenticated' : True,
                    'group': 'promotor',
                    'promociones' : promociones,
                    'propietarios' : propietarios,
                    'espacios': espacios,
                    'giros' : giros,
                    'tipos' : tipos,
                    'materiales' : materiales,
                    'restricciones' : restricciones,
                    'user' : user,
                })
            elif (request.user.groups.filter(name__in=['staff']).exists()):
                #promotores = get_promotores() #change this
                # nr_promotores = promotores[0]
                # propietarios = get_propietarios_old()
                # nr_propietarios = models.Propietario.objects.all().count()
                # nr_registrados = registrados(promotores)
                # espacios = get_espacios_old()
                # nr_espacios = models.EspacioPublicitario.objects.all().count()
                giros = get_giros()
                restricciones = get_restricciones()
                materiales = get_materiales()
                tipos = get_tipos()
                return JsonResponse({
                    'authenticated' : True,
                    'group': 'staff',
                    'giros' : giros,
                    'tipos' : tipos,
                    'materiales' : materiales,
                })
            else:
                return JsonResponse({
                    'data' : 'authenticated',
                    'next' : 'main/datos_personales',
                    'user' : request.user,
                })
        else:
            return JsonResponse({
                'authenticated' : False,
            })
    else:
        raise PermissionDenied

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def add_owner(request):
    # logger.info('request %s', request)
    if (request.method != 'POST') or (not request.user.is_authenticated):
        # logger.error('Bad request')
        raise PermissionDenied
    else:
        promotor = get_promotor(get_usuario(username=request.user.username))
        respuesta = nuevo_propietario(request, promotor)
        promociones = promociones_por_promotor(promotor)
        espacios = espacios_por_promocion(promotor)
        return JsonResponse({
            'respuesta' : respuesta[0],
            'promociones' : promociones,
            'propietario' : respuesta[1],
            'espacios' : espacios
    })

def validate_post_request(request):
    try:
        if (request.method != 'POST') or (not request.user.is_authenticated):
            raise PermissionDenied
        else:
            return True
    except PermissionDenied:
        return False


@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def add_comercial_space(request):
    post_validation = validate_post_request(request)
    if not post_validation:
        return JsonResponse({
        'respuesta': False,
        'msg': 'PermissionDenied'
        })
    api = request.POST.get('api_call')
    if api == 'agregar':
        respuesta = nuevo_espacio_comercial(request.POST)
    elif api == 'actualizar':
        respuesta = actualizar_espacio_comercial(request.POST)
    else:
        respuesta = False
    promotor = get_promotor(get_usuario(request.user.username))
    if (promotor):
        promociones = promociones_por_promotor(promotor)
        espacios = espacios_por_promocion(promotor)
        return JsonResponse({
            'respuesta' : respuesta,
            'promociones' : promociones,
            'espacios': espacios
            })
    else:
        return JsonResponse({
            'respuesta' : respuesta,
            'promociones': False
            })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def upload_addspace_photo(request):
    if (request.method != 'POST') or (not request.user.is_authenticated):
        raise PermissionDenied
    else:
        api = request.POST.get('api_call')
        if api == 'agregar':
            respuesta = [True, archivar_foto(request)]
        elif api == 'actualizar':
            respuesta = [True, actualizar_foto(request)]
        else:
            respuesta = [False, '']
        return JsonResponse({
            'respuesta' : respuesta,
    })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def upload_local_photo(request):
    if (request.method != 'POST') or (not request.user.is_authenticated):
        raise PermissionDenied
    else:
        api = request.POST.get('api_call')
        if api == 'agregar':
            respuesta = [True, archivar_foto(request)]
        elif api == 'actualizar':
            respuesta = [True, actualizar_foto(request)]
        else:
            respuesta = [False, '']
        return JsonResponse({
            'respuesta' : respuesta,
    })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def upload_local_photo(request):
    if (request.method != 'POST') or (not request.user.is_authenticated):
        raise PermissionDenied
    else:
        respuesta = archivar_foto_local(request)
    return JsonResponse({
            'respuesta' : respuesta,
    })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def get_propietarios_por_promotor(request):
    # logger.info('request %s', request)
    if (request.method != 'GET') or (not request.user.is_authenticated):
        # logger.error('Bad request')
        raise PermissionDenied
    else:
        promotor = get_promotor(get_usuario(request.user.username))
        propietarios = propietarios_por_promotor(promotor)
        propietarios, nr_propietarios = propietarios_por_promotor(promotor)
        return JsonResponse({
            'respuesta' : True,
            'propietarios' : propietarios
        })

def register(request, id):
    if(request.method == 'GET'):
        if(id != 0):
            propietario = models.Propietario.objects.get(pk = id)
            datos_personales = propietario.datos_personales
            context = {
            'id': id,
            'datos_personales' : datos_personales,
            'error_message': False,
            }
        else:
            context = {}
        return render(request, 'pilot/register.html', context)
    else:
        username = request.POST.get('username')

        if user_registered(username):
            return JsonResponse({
                'respuesta': False,
                'mensaje': 'Usuario ya existe '
            })
        if(id != 0):
            propietario = models.Propietario.objects.get(pk = id)
            if (propietario.estatus != 'P'):
                return JsonResponse({
                    'respuesta': False,
                    'mensaje': 'Usuario ya registrado '
                })
        else:
            datos_personales = models.DatosPersonales(primer_nombre='', apellido_paterno='', apellido_materno='', email='', telefono='')
            datos_personales.save()
            propietario = models.Propietario(estatus='P', datos_personales=datos_personales)
            propietario.save()
        password = request.POST.get('password1')
        registro = register_propietario(username, password, propietario)
        link = 'https://' + request.get_host() + '/pilot'
        return JsonResponse({
            'respuesta': True,
            'mensaje': 'Usuario registrado.  Favor de ingresar a: ',
            'link': link
        })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def update_datos_personales(request):
    datos_personales = update_datospersonales(request)
    return JsonResponse({
        'respuesta': True,
        'datos_personales': datos_personales,
    })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def update_datos_personales_vendedor(request):
    propietario = models.Propietario.objects.get(pk=request.POST.get('pk'))
    datos_personales_pk = propietario.datos_personales.pk
    datos_personales = models.DatosPersonales.objects.get(pk=datos_personales_pk)
    datos_personales.apellido_paterno = request.POST.get('apellido_paterno')
    datos_personales.apellido_materno = request.POST.get('apellido_materno')
    datos_personales.primer_nombre = request.POST.get('primer_nombre')
    datos_personales.email = request.POST.get('email')
    datos_personales.telefono = request.POST.get('telefono')
    datos_personales.save()
    datos_personales_json = {
    'pk': request.POST.get('pk'),
    'apellido_paterno': datos_personales.apellido_paterno,
    'apellido_materno': datos_personales.apellido_materno,
    'primer_nombre': datos_personales.primer_nombre,
    'email': datos_personales.email,
    'telefono': datos_personales.telefono,
    }
    return JsonResponse({
        'respuesta': True,
        'datos_personales': datos_personales_json,
    })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def load_espacios(request, id):
    if request.method == 'GET':
        propietario = models.Propietario.objects.get(pk=id)
        espacios = espacios_por_propietario(propietario)
        return JsonResponse({
        'response': True,
        'id': id,
        'propietario': propietario.usuario.username,
        'espacios': espacios,
        })
    else:
        return JsonResponse({
        'response': False,
        'id': '',
        'propietario': '',
        'espacios': '',
        })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def update_datos_bancarios(request):
    datos_bancarios = {}
    datos_comerciales = {}
    response = True
    if request.method == 'POST':
        if (request.user.groups.filter(name__in=['propietario']).exists()):
            set_datos_bancarios(request)
        elif (request.user.groups.filter(name__in=['staff']).exists()):
            datos_bancarios, datos_comerciales = get_datos_bancarios(get_propietario_por_id(request.POST.get('id')))
            if datos_bancarios['exists'] == True:
                datos_bancarios['numero_de_cuenta'] = '********'
        else:
            response = False
    elif ((request.method == 'GET') and (request.user.groups.filter(name__in=['propietario']).exists())):
        datos_bancarios, datos_comerciales = get_datos_bancarios(get_propietario_por_usuario(request.user))
    else:
        response = False
    return JsonResponse({
    'response': response,
    'datos_bancarios': datos_bancarios,
    'datos_comerciales': datos_comerciales,
    })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def update_datos_comerciales(request):
    if request.method == 'GET':
        return JsonResponse({
        'response': True,
        })
    propietario = get_propietario_por_usuario(request.user)
    if request.method == 'POST':
        if not propietario.datos_comerciales:
            datos_comerciales = models.DatosComerciales()
        else:
            datos_comerciales = propietario.datos_comerciales
        datos_comerciales.rfc = request.POST.get('rfc')
        datos_comerciales.factura = request.POST.get('factura')
        datos_comerciales.save()
        propietario.datos_comerciales = datos_comerciales
        propietario.estatus = 'R'
        propietario.save()
        return JsonResponse({
        'response': True,
        'estatus': propietario.estatus
        })

@require_http_methods(["GET", "POST"])
@login_required(login_url='/#')
@ensure_csrf_cookie
def add_local(request):
    if request.method == 'POST':
        if request.POST.get('api_call') == 'agregar':
            local = add_new_local(request)
        elif request.POST.get('api_call') == 'actualizar':
            local = update_local(request)
        return JsonResponse({
        'response': True,
        'api_call': request.POST.get('api_call'),
        'local':{
            'pk': local.pk,
            'nombre_comercial': local.nombre_comercial,
            'espacio_tipo': local.espacio_tipo,
            'propietario': local.propietario.pk,
            'foto': local.get_foto(),
            'giro': local.giro.pk,
            'geo_localidad' : [local.geo_localidad.latitud, local.geo_localidad.longitud],
            'direccion':{
                'calle': local.direccion.calle,
                'numero_exterior': local.direccion.numero_exterior,
                'numero_interior': local.direccion.numero_interior,
                'colonia': local.direccion.colonia,
                'municipio': local.direccion.municipio,
                'codigo_postal': local.direccion.codigo_postal,
                'estado': local.direccion.estado
            }
        }
        })
    else:
        propietario = get_propietario_por_usuario(request.user)
        locales = get_locales(propietario)
        return JsonResponse({
        'locales': locales,
        })
