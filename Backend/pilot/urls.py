from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name="logout"),
    # path('statsvendedor', views.stats_vendedor, name='statsvendedor'),
    path('datos_bancarios', views.update_datos_bancarios),
    path('addLocal', views.add_local),
    path('datos_comerciales', views.update_datos_comerciales),
    path('get_espacios/<int:id>', views.load_espacios),
    path('getpropietarios', views.get_propietarios_por_promotor, name='get propietarios'),
    # path('getpropietario/<int:id>', views.get_propietario, name='get propietario'),
    path('addowner', views.add_owner, name='nuevo propietario'),
    path('addcomercialspace', views.add_comercial_space, name='nuevo espacio comercial'),
    path('uploadaddspacephoto', views.upload_addspace_photo, name='recivo foto espacio'),
    path('uploadlocalphoto', views.upload_local_photo),
    path('update_datos_personales', views.update_datos_personales),
    path('update_datos_comerciales/<int:id>', views.update_datos_comerciales), #Creo qye esta se debe borrar
    path('update_datos_personales_vendedor', views.update_datos_personales_vendedor),
    path('register', views.register),
    path('register/<int:id>', views.register),
]
