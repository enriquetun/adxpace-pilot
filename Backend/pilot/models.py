from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q
from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import JSONField
from django.utils import timezone

UserModel = get_user_model()

class Probemaster(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=100)   # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'ProbeMaster'

class Probeslave(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=120)   # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'ProbeSlave'

class Direccion(models.Model):
    calle = models.CharField(max_length=256)
    numero_interior = models.CharField(max_length=256, blank=True, null=True)
    numero_exterior = models.CharField(max_length=256, blank=True, null=True)
    colonia = models.CharField(max_length=256)
    codigo_postal = models.CharField(max_length=16)
    municipio = models.CharField(max_length=256)
    estado = models.CharField(max_length=256)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'direccion'
        verbose_name = 'Dirección'
        verbose_name_plural = 'Direcciones'

class Clasificacion(models.Model):
    sku = models.CharField(max_length=1024, blank=True, null=True)
    estatus = models.CharField(max_length=1024, default = 'INACTIVO')
    calificacion = models.IntegerField(blank=True, null=True)
    aprobado = models.BooleanField(default=False)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'calsificacion'
        verbose_name = 'Clasificacion'
        verbose_name_plural = 'Clasificacion'


class Material(models.Model):
    designacion = models.CharField(max_length=256)

    def __str__(self):
        return '%s' % (self.designacion)

    class Meta:
        managed = True
        db_table = 'material'
        verbose_name = 'Material'
        verbose_name_plural = 'Materiales'


class CaracteristicasFisicas(models.Model):
    alto = models.DecimalField(max_digits=6, decimal_places=3)
    ancho = models.DecimalField(max_digits=6, decimal_places=3)
    material = models.ForeignKey('Material', blank=True, null=True, on_delete=models.DO_NOTHING)
    digital = models.BooleanField(default=False)
    descripcion = models.TextField(blank=True, null=True)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'alto: %s ancho: %s material: %s' % (self.alto, self.ancho, self.material)

    class Meta:
        managed = True
        db_table = 'caracteristicas_fisicas'
        verbose_name = 'Caracteristica Fisica'
        verbose_name_plural = 'Caracteristicas Fisicas'


class DatosBancarios(models.Model):
    banco = models.CharField(max_length=256)
    numero_de_cuenta = models.CharField(max_length=64)
    titular = models.CharField(max_length=64, blank=True, null=True)
    fideicomiso = models.BooleanField(default=False)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return ('%s' % (self.pk))

    class Meta:
        managed = True
        db_table = 'datos_bancarios'
        verbose_name = 'Datos Bancario'
        verbose_name_plural = 'Datos Bancarios'


class DatosComerciales(models.Model):
    rfc = models.CharField(max_length=13)
    factura =models.BooleanField()
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'datos_comerciales'
        verbose_name = 'Datos Comercial'
        verbose_name_plural = 'Datos Comerciales'


class DatosPersonales(models.Model):
    primer_nombre = models.CharField(max_length=256)
    segundo_nombre = models.CharField(max_length=256, blank=True, null=True)
    apellido_paterno = models.CharField(max_length=256)
    apellido_materno = models.CharField(max_length=256, blank=True, null=True)
    email = models.CharField(max_length=256, blank=True, null=True)
    telefono = models.CharField(max_length=16, blank=True, null=True)
    # hash = models.CharField(max_length=97)
    # hash = models.CharField(max_length=64)
    # salt = models.CharField(max_length=32)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s %s' % (self.primer_nombre, self.apellido_paterno)

    class Meta:
        managed = True
        db_table = 'datos_personales'
        verbose_name = 'Dato Personal'
        verbose_name_plural = 'Datos Personales'
        # indexes = [
        #     models.Index(fields=['hash'])
        # ]

class Educacion(models.Model):
    sin_instruccion = models.IntegerField(blank=True, null=True)
    pre_escolar = models.IntegerField(blank=True, null=True)
    primaria_incompleta = models.IntegerField(blank=True, null=True)
    primaria_completa = models.IntegerField(blank=True, null=True)
    secundaria_incompleta = models.IntegerField(blank=True, null=True)
    secundaria_completa = models.IntegerField(blank=True, null=True)
    preparatoria_incompleta = models.IntegerField(blank=True, null=True)
    preparatoria_completa = models.IntegerField(blank=True, null=True)
    licenciatura_incompleta = models.IntegerField(blank=True, null=True)
    licenciatura_completa = models.IntegerField(blank=True, null=True)
    postgrado = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'educacion'
        verbose_name = 'Educación'
        verbose_name_plural = 'Eduaciones'

class DatosDemograficos(models.Model):
    educacion = models.OneToOneField('Educacion', blank=True, null=True, on_delete=models.CASCADE)
    trafico = models.OneToOneField('Trafico', blank=True, null=True, on_delete=models.CASCADE)
    propiedades = models.OneToOneField('Propiedades', on_delete=models.CASCADE)
    amai = models.OneToOneField('RangosSocioEconomicos', blank=True, null=True, on_delete=models.CASCADE)
    amai_numerico = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'datos_demograficos'
        verbose_name = 'Dato Demográfico'
        verbose_name_plural = 'Datos Demográficos'

class EspacioPublicitario(models.Model):
    propietario = models.ForeignKey('Propietario', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=256, blank=True, null=True)
    geo_localidad = models.ForeignKey('Geolocalidad', blank=True, null=True, on_delete=models.CASCADE)
    local = models.ForeignKey('local', blank=True, null=True, on_delete=models.DO_NOTHING)
    giro = models.ForeignKey('Giro', on_delete=models.DO_NOTHING)
    tipo = models.ForeignKey('Tipo', on_delete=models.DO_NOTHING)
    # restricciones = models.ForeignKey('Restricciones', on_delete=models.DO_NOTHING)
    caracteristicas_fisicas = models.OneToOneField('CaracteristicasFisicas', on_delete=models.CASCADE)
    clasificacion = models.OneToOneField('Clasificacion', blank=True, null=True, on_delete=models.CASCADE)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'espacio_publicitario'
        verbose_name = 'Espacio publicitario'
        verbose_name_plural = 'Espacios publicitarios'

    def get_foto(self):
        try:
            return Foto.objects.get(espacio_publicitario=self).src.url
        except:
            pass

class EspacioInstitucional(models.Model):
    geo_localidad = models.ForeignKey('Geolocalidad', blank=True, null=True, on_delete=models.CASCADE)
    data = data = JSONField()
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'espacio_institucional'
        verbose_name = 'Espacio instucional'
        verbose_name_plural = 'Espacios institucionales'

class Foto(models.Model):
    src = models.ImageField()
    alterna = models.BooleanField(default=False)
    espacio_publicitario = models.OneToOneField('EspacioPublicitario', blank=True, null=True, on_delete=models.CASCADE)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'foto'
        verbose_name = 'Foto'
        verbose_name_plural = 'Fotos'


class Geolocalidad(models.Model):
    latitud = models.CharField(max_length=32)
    longitud = models.CharField(max_length=32)
    pnt = PointField(blank=True, null=True)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'geo_localidad'
        verbose_name = 'Geolocalidad'
        verbose_name_plural = 'Geolocalidades'

    def __str__(self):
        return '%s %s' % (self.latitud, self.longitud)

    def save(self, *args, **kwargs):
        self.pnt = Point(float(self.longitud), float(self.latitud))
        super().save(*args, **kwargs)


class Giro(models.Model):
    designacion = models.CharField(max_length=64, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.designacion)

    class Meta:
        managed = True
        db_table = 'giro'
        verbose_name = 'Giro'
        verbose_name_plural = 'Giros'


class Hogar(models.Model):
    cuartos = models.IntegerField()
    internet = models.BooleanField()
    banos = models.IntegerField(blank=True, null=True)
    empleados = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'hogar'
        verbose_name = 'Hogar'
        verbose_name_plural = 'Hogares'

class Local(models.Model):
    TIPO_DE_ESPACIO = (
    ('C','Comercial'),
    ('P', 'Profesional')
    )
    nombre_comercial = models.CharField(max_length=256)
    espacio_tipo = models.CharField(max_length=1, choices=TIPO_DE_ESPACIO)
    propietario = models.ForeignKey('Propietario', blank=True, null=True, on_delete=models.CASCADE)
    direccion = models.OneToOneField('Direccion', blank=True, null=True, on_delete=models.CASCADE)
    geo_localidad = models.OneToOneField('Geolocalidad', blank=True, null=True, on_delete=models.CASCADE)
    foto = models.OneToOneField('Foto', blank=True, null=True, on_delete=models.CASCADE)
    giro = models.ForeignKey('Giro', blank=True, null=True, on_delete=models.DO_NOTHING)
    datos_demograficos = models.ForeignKey('DatosDemograficos', blank=True, null=True, on_delete=models.CASCADE)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    def get_foto(self):
        try:
            return self.foto.src.url
        except:
            return False

    class Meta:
        managed = True
        db_table = 'local'
        verbose_name = 'Local'
        verbose_name_plural = 'Locales'


class Promociones(models.Model):
    promotor = models.ForeignKey('Promotor', on_delete=models.CASCADE)
    propietario = models.OneToOneField('Propietario', on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'promociones'
        verbose_name = 'Promocion'
        verbose_name_plural = 'Promociones'


class Promotor(models.Model):
    usuario = models.OneToOneField('Usuario', on_delete=models.CASCADE)
    datos_personales = models.OneToOneField('DatosPersonales', on_delete=models.CASCADE)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        usuario = Usuario.objects.filter(username=self.usuario)
        return '%s' % (usuario[0].pk )

    def propietarios(self):
        promociones = Promociones.objects.filter(promotor=self)
        return [promocion.propietario for promocion in promociones]

    def get_promociones(self):
        promociones = Promociones.objects.filter(promotor=self)
        return promociones

    class Meta:
        managed = True
        db_table = 'promotor'
        verbose_name = 'Promotor'
        verbose_name_plural = 'Promotores'


class Propiedades(models.Model):
    automoviles = models.IntegerField(blank=True, null=True)
    hogar = models.ForeignKey('Hogar', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'propiedades'
        verbose_name = 'Propiedad'
        verbose_name_plural = 'Propiedades'

class Propietario(models.Model):
    ESTATUS_REGISTRO_PROPIETARIO = (
        ('R', 'REGISTRADO'),
        ('I', 'INCONCLUSO'),
        ('P', 'PREREGISTRADO'),
        ('D', 'DECLINADO'),
        ('S', 'SUSPENDIDO'),
        ('C', 'CANCELADO'),
    )
    estatus = models.CharField(max_length=2, choices=ESTATUS_REGISTRO_PROPIETARIO, default='P')
    usuario = models.OneToOneField('Usuario', blank=True, null=True, on_delete=models.CASCADE)
    datos_personales = models.OneToOneField('DatosPersonales', on_delete=models.CASCADE)
    datos_bancarios = models.ForeignKey('DatosBancarios', blank=True, null=True, on_delete=models.CASCADE)
    datos_comerciales = models.ForeignKey('DatosComerciales', blank=True, null=True, on_delete=models.CASCADE)
    # hash = models.CharField(max_length=97)
    # hash = models.CharField(max_length=64)
    # salt = models.CharField(max_length=32)
    fecha_de_registro = models.DateTimeField(auto_now_add=True)
    fecha_de_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        datos_personales = DatosPersonales.objects.get(pk=self.datos_personales.pk)
        return '%s %s' % (datos_personales.primer_nombre, datos_personales.apellido_paterno)

    def get_modelo_de_usuario(self):
        try:
            return UserModel.objects.get(username=self.usuario.username)
        except:
            return False

    def promotor(self):
        try:
            query = Promociones.objects.filter(propietario=self)
            promotor = query[0].promotor
        except:
            promotor = Promotor()

        return promotor

    def espacios(self):
        espacios = EspacioPublicitario.objects.filter(propietario=self)
        return [espacio for espacio in espacios]

    def locales(self):
        locales = Local.objects.filter(propietario=self)
        return [local for local in locales]

    def check_bancarios(self):
        # 0 = 'IE' Inexistente
        # 1 = 'ID' Indefinido
        # 2 = 'OK' OK
        try:
            if self.datos_bancarios.banco == 'undefined' or self.datos_bancarios.numero_de_cuenta == 'undefined' or self.datos_bancarios.titular == 'undefined':
                return 'ID'
            else:
                return 'OK'
        except:
            return 'IE'

    class Meta:
        managed = True
        db_table = 'propietario'

class RangosSocioEconomicos(models.Model):
    nivel_ab = models.IntegerField()
    nivel_c_mas = models.TextField()    # This field type is a guess.
    nivel_c = models.TextField()        # This field type is a guess.
    nivel_c_menos = models.TextField()  # This field type is a guess.
    nivel_d_mas = models.TextField()    # This field type is a guess.
    nivel_d = models.TextField()        # This field type is a guess.
    nivel_e = models.TextField()        # This field type is a guess.

    class Meta:
        managed = True
        db_table = 'rangos_socio_economicos'
        verbose_name = 'Rango Socioenonomico'
        verbose_name_plural = 'Rangos Socioeconomicos'


class Restriccion(models.Model):
    designacion = models.CharField(max_length=256, unique=True)

    def __str__(self):
        return '%s' % (self.designacion)

    class Meta:
        managed = True
        db_table = 'restriccion'


class Tipo(models.Model):
    designacion = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return '%s' % (self.designacion)

    class Meta:
        managed = True
        db_table = 'tipo'


class Trafico(models.Model):
    diario = models.TextField(blank=True, null=True)        # This field type is a guess.
    masculino = models.IntegerField(blank=True, null=True)
    femenino = models.IntegerField(blank=True, null=True)
    por_edad = models.TextField(blank=True, null=True)      # This field type is a guess.

    class Meta:
        managed = True
        db_table = 'trafico'


class Grupo(models.Model):
    nombre = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        managed = True
        db_table = 'grupo'

class Usuario(models.Model):
    username = models.CharField(max_length=64, primary_key=True)
    grupo = models.ForeignKey('Grupo', to_field="nombre", db_column="grupo", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.username)

    class Meta:
        managed = True
        db_table = 'usuario'
