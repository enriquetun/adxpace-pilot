# Generated by Django 2.1 on 2018-11-08 19:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CaracteristicasFisicas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alto', models.DecimalField(decimal_places=3, max_digits=6)),
                ('ancho', models.DecimalField(decimal_places=3, max_digits=6)),
                ('digital', models.BooleanField(default=False)),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Caracteristica Fisica',
                'verbose_name_plural': 'Caracteristicas Fisicas',
                'db_table': 'caracteristicas_fisicas',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Clasificacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku', models.CharField(blank=True, max_length=1024, null=True)),
                ('estatus', models.CharField(default='INACTIVO', max_length=1024)),
                ('calificacion', models.IntegerField(blank=True, null=True)),
                ('aprobado', models.BooleanField(default=False)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Clasificacion',
                'verbose_name_plural': 'Clasificacion',
                'db_table': 'calsificacion',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DatosBancarios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('banco', models.CharField(max_length=256)),
                ('numero_de_cuenta', models.CharField(max_length=64)),
                ('titular', models.CharField(blank=True, max_length=64, null=True)),
                ('fideicomiso', models.BooleanField(default=False)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Datos Bancario',
                'verbose_name_plural': 'Datos Bancarios',
                'db_table': 'datos_bancarios',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DatosComerciales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rfc', models.CharField(max_length=13)),
                ('factura', models.BooleanField()),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Datos Comercial',
                'verbose_name_plural': 'Datos Comerciales',
                'db_table': 'datos_comerciales',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DatosDemograficos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amai_numerico', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Dato Demográfico',
                'verbose_name_plural': 'Datos Demográficos',
                'db_table': 'datos_demograficos',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='DatosPersonales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('primer_nombre', models.CharField(max_length=256)),
                ('segundo_nombre', models.CharField(blank=True, max_length=256, null=True)),
                ('apellido_paterno', models.CharField(max_length=256)),
                ('apellido_materno', models.CharField(blank=True, max_length=256, null=True)),
                ('email', models.CharField(blank=True, max_length=256, null=True)),
                ('telefono', models.CharField(blank=True, max_length=16, null=True)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Dato Personal',
                'verbose_name_plural': 'Datos Personales',
                'db_table': 'datos_personales',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Direccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('calle', models.CharField(max_length=256)),
                ('numero_interior', models.CharField(blank=True, max_length=256, null=True)),
                ('numero_exterior', models.CharField(blank=True, max_length=256, null=True)),
                ('colonia', models.CharField(max_length=256)),
                ('codigo_postal', models.CharField(max_length=16)),
                ('municipio', models.CharField(max_length=256)),
                ('estado', models.CharField(max_length=256)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Dirección',
                'verbose_name_plural': 'Direcciones',
                'db_table': 'direccion',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Educacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sin_instruccion', models.IntegerField(blank=True, null=True)),
                ('pre_escolar', models.IntegerField(blank=True, null=True)),
                ('primaria_incompleta', models.IntegerField(blank=True, null=True)),
                ('primaria_completa', models.IntegerField(blank=True, null=True)),
                ('secundaria_incompleta', models.IntegerField(blank=True, null=True)),
                ('secundaria_completa', models.IntegerField(blank=True, null=True)),
                ('preparatoria_incompleta', models.IntegerField(blank=True, null=True)),
                ('preparatoria_completa', models.IntegerField(blank=True, null=True)),
                ('licenciatura_incompleta', models.IntegerField(blank=True, null=True)),
                ('licenciatura_completa', models.IntegerField(blank=True, null=True)),
                ('postgrado', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Educación',
                'verbose_name_plural': 'Eduaciones',
                'db_table': 'educacion',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='EspacioPublicitario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=256, null=True)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
                ('caracteristicas_fisicas', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pilot.CaracteristicasFisicas')),
                ('clasificacion', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Clasificacion')),
            ],
            options={
                'verbose_name': 'Espacio publicitario',
                'verbose_name_plural': 'Espacios publicitarios',
                'db_table': 'espacio_publicitario',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Foto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('src', models.TextField()),
                ('alterna', models.BooleanField(default=False)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
                ('espacio_publicitario', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.EspacioPublicitario')),
            ],
            options={
                'verbose_name': 'Foto',
                'verbose_name_plural': 'Fotos',
                'db_table': 'foto',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Geolocalidad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latitud', models.CharField(max_length=32)),
                ('longitud', models.CharField(max_length=32)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Geolocalidad',
                'verbose_name_plural': 'Geolocalidades',
                'db_table': 'geo_localidad',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Giro',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('designacion', models.CharField(blank=True, max_length=64, null=True)),
            ],
            options={
                'verbose_name': 'Giro',
                'verbose_name_plural': 'Giros',
                'db_table': 'giro',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Grupo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=64, unique=True)),
            ],
            options={
                'db_table': 'grupo',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Hogar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cuartos', models.IntegerField()),
                ('internet', models.BooleanField()),
                ('banos', models.IntegerField(blank=True, null=True)),
                ('empleados', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Hogar',
                'verbose_name_plural': 'Hogares',
                'db_table': 'hogar',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Local',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_comercial', models.CharField(max_length=256)),
                ('espacio_tipo', models.CharField(choices=[('C', 'Comercial'), ('P', 'Profesional')], max_length=1)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
                ('datos_demograficos', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.DatosDemograficos')),
                ('direccion', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Direccion')),
                ('foto', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Foto')),
                ('geo_localidad', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Geolocalidad')),
            ],
            options={
                'verbose_name': 'Local',
                'verbose_name_plural': 'Locales',
                'db_table': 'local',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('designacion', models.CharField(max_length=256)),
            ],
            options={
                'verbose_name': 'Material',
                'verbose_name_plural': 'Materiales',
                'db_table': 'material',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Probemaster',
            fields=[
                ('id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('name', models.CharField(db_column='Name', max_length=100)),
            ],
            options={
                'db_table': 'ProbeMaster',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Probeslave',
            fields=[
                ('id', models.IntegerField(db_column='Id', primary_key=True, serialize=False)),
                ('name', models.CharField(db_column='Name', max_length=120)),
            ],
            options={
                'db_table': 'ProbeSlave',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Promociones',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Promocion',
                'verbose_name_plural': 'Promociones',
                'db_table': 'promociones',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Promotor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
                ('datos_personales', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pilot.DatosPersonales')),
            ],
            options={
                'verbose_name': 'Promotor',
                'verbose_name_plural': 'Promotores',
                'db_table': 'promotor',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Propiedades',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('automoviles', models.IntegerField(blank=True, null=True)),
                ('hogar', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Hogar')),
            ],
            options={
                'verbose_name': 'Propiedad',
                'verbose_name_plural': 'Propiedades',
                'db_table': 'propiedades',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Propietario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estatus', models.CharField(choices=[('R', 'REGISTRADO'), ('I', 'INCONCLUSO'), ('P', 'PREREGISTRADO'), ('D', 'DECLINADO'), ('S', 'SUSPENDIDO'), ('C', 'CANCELADO')], default='P', max_length=2)),
                ('fecha_de_registro', models.DateTimeField(auto_now_add=True)),
                ('fecha_de_actualizacion', models.DateTimeField(auto_now=True)),
                ('datos_bancarios', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.DatosBancarios')),
                ('datos_comerciales', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.DatosComerciales')),
                ('datos_personales', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pilot.DatosPersonales')),
            ],
            options={
                'db_table': 'propietario',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='RangosSocioEconomicos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nivel_ab', models.IntegerField()),
                ('nivel_c_mas', models.TextField()),
                ('nivel_c', models.TextField()),
                ('nivel_c_menos', models.TextField()),
                ('nivel_d_mas', models.TextField()),
                ('nivel_d', models.TextField()),
                ('nivel_e', models.TextField()),
            ],
            options={
                'verbose_name': 'Rango Socioenonomico',
                'verbose_name_plural': 'Rangos Socioeconomicos',
                'db_table': 'rangos_socio_economicos',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Restriccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('designacion', models.CharField(max_length=256, unique=True)),
            ],
            options={
                'db_table': 'restriccion',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Tipo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('designacion', models.CharField(max_length=64, unique=True)),
            ],
            options={
                'db_table': 'tipo',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Trafico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('diario', models.TextField(blank=True, null=True)),
                ('masculino', models.IntegerField(blank=True, null=True)),
                ('femenino', models.IntegerField(blank=True, null=True)),
                ('por_edad', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'trafico',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('username', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('grupo', models.ForeignKey(db_column='grupo', on_delete=django.db.models.deletion.CASCADE, to='pilot.Grupo', to_field='nombre')),
            ],
            options={
                'db_table': 'usuario',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='propietario',
            name='usuario',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Usuario'),
        ),
        migrations.AddField(
            model_name='promotor',
            name='usuario',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pilot.Usuario'),
        ),
        migrations.AddField(
            model_name='promociones',
            name='promotor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pilot.Promotor'),
        ),
        migrations.AddField(
            model_name='promociones',
            name='propietario',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pilot.Propietario'),
        ),
        migrations.AddField(
            model_name='local',
            name='propietario',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Propietario'),
        ),
        migrations.AddField(
            model_name='espaciopublicitario',
            name='geo_localidad',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Geolocalidad'),
        ),
        migrations.AddField(
            model_name='espaciopublicitario',
            name='giro',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='pilot.Giro'),
        ),
        migrations.AddField(
            model_name='espaciopublicitario',
            name='local',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='pilot.Local'),
        ),
        migrations.AddField(
            model_name='espaciopublicitario',
            name='propietario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pilot.Propietario'),
        ),
        migrations.AddField(
            model_name='espaciopublicitario',
            name='tipo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='pilot.Tipo'),
        ),
        migrations.AddField(
            model_name='datosdemograficos',
            name='amai',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.RangosSocioEconomicos'),
        ),
        migrations.AddField(
            model_name='datosdemograficos',
            name='educacion',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Educacion'),
        ),
        migrations.AddField(
            model_name='datosdemograficos',
            name='propiedades',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pilot.Propiedades'),
        ),
        migrations.AddField(
            model_name='datosdemograficos',
            name='trafico',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pilot.Trafico'),
        ),
        migrations.AddField(
            model_name='caracteristicasfisicas',
            name='material',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='pilot.Material'),
        ),
    ]
