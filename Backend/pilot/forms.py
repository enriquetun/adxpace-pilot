import json
from django import forms

class DatosPersonalesForm(forms.Form):
    primer_nombre = forms.CharField()
    segundo_nombre = forms.CharField(required=False)
    apellido_paterno = forms.CharField()
    apellido_materno = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    telefono = forms.CharField(required=False)

class EspacioPublicitarioForm(forms.Form):
    # primer_nombre = forms.CharField()
    # apellido_paterno = forms.CharField()
    alto = forms.DecimalField()
    ancho = forms.DecimalField()
    material = forms.CharField()
    digital = forms.CharField()
    descripcion = forms.CharField()
    giro = forms.CharField()
    tipo = forms.CharField()
    # foto = forms.ImageField()

class FotoEspacioComercialForm(forms.Form):
    imagen = forms.ImageField()
