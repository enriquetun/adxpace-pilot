$(document).ready(function(){
  $('.main:first').css('paddingTop', '30px')
  validate_data()
  $('#submitButton').click(function(event){
    event.preventDefault()
    if(!validate_data() || !validate_password()){
      $('#errorMessage')[0].hidden = false
      $('#errorMessage')[0].textContent = 'Error en formulario'
    }
    else{
      // $('#registerForm').submit()
      $.post(window.location.href, {
        'username': $('input')[1].value,
        'password1': $('input')[2].value,
        'csrfmiddlewaretoken': $('input')[0].value})
        .then(function(response){
          console.log(response)
          if(!response.respuesta){

            $('#errorMessage')[0].hidden = false
            $('#errorMessage')[0].textContent = response.mensaje
          }
          else{
            $('#successMessage')[0].hidden = false
            // $('#successMessage')[0].textContent = response.mensaje
            $('#successMessage').children().text(response.link)
            $('#successMessage').children().attr('href', response.link)
            $('#submitButton').hide()
          }
        })
        .catch(function(error){
          console.log(error)
        })
    }
  })

  $('#username').keyup(function(){
    validate_data()
  })
  $('#password1').keyup(function(){
    validate_data()
  })
  $('#password2').keyup(function(){
    validate_data()
  })
})

function validate_password(){
  if(($('#password1').val() != $('#password2').val())){
    return false
  }
  else{
    return true
  }
}

function validate_data(){
  var elements = $('input')
  var validation_results = []
  var respuesta = false
  for (var i = 1; i <= 3; i++) {
    element_id = '#' + elements[i].id
    element = $(element_id)
  validation_results.push(validate_length(element))
  }

  return check_false(validation_results)
}

function check_false(array){
  var respuesta = true
  for(var i=0; i < array.length; i++){
    if((array[i] ==false)){
        respuesta =false
    }
    else if(respuesta == true && array[i] == true){
      respuesta = true
    }
  }

  return respuesta
}

function validate_length(element){

  if(element.val().length < 6){
    element.addClass('is-invalid')
    return false
  }
  else{
    element.removeClass('is-invalid')
    return true
  }
}
