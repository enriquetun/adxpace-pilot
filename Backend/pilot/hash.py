import math
import hashlib
import sys

import base64


def birthday_probability(n, d):
    """Calculate the probability of generating a duplicate random number after
    generating "n" random numbers in the range "d".
    """
    # Formula taken from: https://en.wikipedia.org/wiki/Birthday_problem
    birthday_prob = 1 - math.e ** (-n**2 / (2 * d))
    expected_coalitions = birthday_prob * n
    return birthday_prob, expected_coalitions

# max bits > 0 == width of the value in bits (e.g., int_16 -> 16)
rotl = lambda val, r_bits, max_bits : \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))

def ROTL64(val, r_bits):
    return rotl(val, r_bits, 64)

u64 = lambda key : sum([ord(key[i])*pow(2,8*(7-i)) for i in range(0,8)])
u32 = lambda key : sum([ord(key[i])*pow(2,8*(3-i)) for i in range(0,4)])
u16 = lambda key : sum([ord(key[i])*pow(2,8*(2-i)) for i in range(0,2)])

'''
rotl64(uint64_t x, int8_t r)
{
  return (x << r) | (x >> (64 - r));
}


//-----------------------------------------------------------------------------
// Block read - if your platform needs to do endian-swapping or can only
// handle aligned reads, do the conversion here

#define getblock(p, i) (p[i])

//-----------------------------------------------------------------------------
// Finalization mix - force all bits of a hash block to avalanche

static CK_CC_INLINE uint64_t
fmix64(uint64_t k)
{
  k ^= k >> 33;
  k *= 0xff51afd7ed558ccdLLU;
  k ^= k >> 33;
  k *= 0xc4ceb9fe1a85ec53LLU;
  k ^= k >> 33;

  return k;
}

void MurmurHash3_x64_128 ( const void * key, const int len,
                           const uint32_t seed, void * out )
{
  const uint8_t * data = (const uint8_t*)key;
  const int nblocks = len / 16;
  int i;

  uint64_t h1 = seed;
  uint64_t h2 = seed;

  uint64_t c1 = BIG_CONSTANT(0x87c37b91114253d5);
  uint64_t c2 = BIG_CONSTANT(0x4cf5ad432745937f);

  //----------
  // body

  const uint64_t * blocks = (const uint64_t *)(data);

  for(i = 0; i < nblocks; i++)
  {
    uint64_t k1 = getblock(blocks,i*2+0);
    uint64_t k2 = getblock(blocks,i*2+1);

    k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;

    h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729;

    k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;

    h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5;
  }

  //----------
  // tail

  const uint8_t * tail = (const uint8_t*)(data + nblocks*16);

  uint64_t k1 = 0;
  uint64_t k2 = 0;

  switch(len & 15)
  {
  case 15: k2 ^= (uint64_t)(tail[14]) << 48;
  case 14: k2 ^= (uint64_t)(tail[13]) << 40;
  case 13: k2 ^= (uint64_t)(tail[12]) << 32;
  case 12: k2 ^= (uint64_t)(tail[11]) << 24;
  case 11: k2 ^= (uint64_t)(tail[10]) << 16;
  case 10: k2 ^= (uint64_t)(tail[ 9]) << 8;
  case  9: k2 ^= (uint64_t)(tail[ 8]) << 0;
  case 13: k2 ^= (uint64_t)(tail[12]) << 32;
  case 12: k2 ^= (uint64_t)(tail[11]) << 24;
  case 11: k2 ^= (uint64_t)(tail[10]) << 16;
  case 10: k2 ^= (uint64_t)(tail[ 9]) << 8;
  case  9: k2 ^= (uint64_t)(tail[ 8]) << 0;
           k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;

  case  8: k1 ^= (uint64_t)(tail[ 7]) << 56;
  case  7: k1 ^= (uint64_t)(tail[ 6]) << 48;
  case  6: k1 ^= (uint64_t)(tail[ 5]) << 40;
  case  5: k1 ^= (uint64_t)(tail[ 4]) << 32;
  case  4: k1 ^= (uint64_t)(tail[ 3]) << 24;
  case  3: k1 ^= (uint64_t)(tail[ 2]) << 16;
  case  2: k1 ^= (uint64_t)(tail[ 1]) << 8;
  case  1: k1 ^= (uint64_t)(tail[ 0]) << 0;
           k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;
  };

  //----------
  // finalization

  h1 ^= len; h2 ^= len;

  h1 += h2;
  h2 += h1;

  h1 = fmix64(h1);
  h2 = fmix64(h2);

  h1 += h2;
  h2 += h1;

  ((uint64_t*)out)[0] = h1;
  ((uint64_t*)out)[1] = h2;
}
'''

def murmurhash3_x64_128(key, seed = 19820125):
    # nkey = key
    # len_key = len(key)
    # fill_len = 16 - (len_key % 16)
    # ext_fill = [key[i] for i in range(fill_len, 0, -1)]
    # for i in range(0, fill_len):
    #     nkey += ext_fill[i]

    # data = nkey.encode()
    # len8 = int(len(data)/8)
    # data64 = [data[i*8:((i+1)*8)] for i in range(0, int(len(data)/8))]
    # blocks = [base64.b64encode(data64[i]) for i in range(0, len(data64))]
    # blocks = [base64.b64encode(data[i*8:((i+1)*8)]) for i in range(0, len8)]
    # blocks = [int(data[i*8:((i+1)*8)], 16) for i in range(0, len8)]
    # nblocks = len(blocks)

    # nblocks = int(len(nkey)/8)
    # blocks = [u64(nkey[i:i+8]) for i in range(0,nblocks)]

    # print(nblocks)
    # print(blocks)

    h1 = seed
    h2 = seed

    c1 = 0x87c37b91114253d5
    c2 = 0x4cf5ad432745937f

    len_key = len(key)
    data = [ord(key[i]) for i in range(0, len_key)]

    print(data)

    nr16_blocks = len(data)*8 // 16
    rem16_blocks = len(data)*8 % 16

    print(nblocks)

    '''
    for(i = 0; i < nblocks; i++)
    {
        uint64_t k1 = getblock(blocks,i*2+0);
        uint64_t k2 = getblock(blocks,i*2+1);

        k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;
        h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729;
        k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;
        h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5;
    }
    '''
    # blocks = 
    for i in range(0, nblocks-1):
        print(i)
        k1 = rotl(blocks[i*2]*c1,31,64) * c2
        h1 = (rotl(h1^k1,27,64)+h2)*5
        h1 += 0x52dce729
        k2 = rotl(blocks[i*2+1]*c2,33,64) * c1
        h2 = (rotl(h2^k2,31,64)+h1)*5
        h2 += 0x38495ab5

    '''
    //----------
    // tail

    const uint8_t * tail = (const uint8_t*)(data + nblocks*16);

    uint64_t k1 = 0;
    uint64_t k2 = 0;

    switch(len & 15)
    {
    case 15: k2 ^= (uint64_t)(tail[14]) << 48;
    case 14: k2 ^= (uint64_t)(tail[13]) << 40;
    case 13: k2 ^= (uint64_t)(tail[12]) << 32;
    case 12: k2 ^= (uint64_t)(tail[11]) << 24;
    case 11: k2 ^= (uint64_t)(tail[10]) << 16;
    case 10: k2 ^= (uint64_t)(tail[ 9]) << 8;
    case  9: k2 ^= (uint64_t)(tail[ 8]) << 0;
    case 13: k2 ^= (uint64_t)(tail[12]) << 32;
    case 12: k2 ^= (uint64_t)(tail[11]) << 24;
    case 11: k2 ^= (uint64_t)(tail[10]) << 16;
    case 10: k2 ^= (uint64_t)(tail[ 9]) << 8;
    case  9: k2 ^= (uint64_t)(tail[ 8]) << 0;
            k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;

    case  8: k1 ^= (uint64_t)(tail[ 7]) << 56;
    case  7: k1 ^= (uint64_t)(tail[ 6]) << 48;
    case  6: k1 ^= (uint64_t)(tail[ 5]) << 40;
    case  5: k1 ^= (uint64_t)(tail[ 4]) << 32;
    case  4: k1 ^= (uint64_t)(tail[ 3]) << 24;
    case  3: k1 ^= (uint64_t)(tail[ 2]) << 16;
    case  2: k1 ^= (uint64_t)(tail[ 1]) << 8;
    case  1: k1 ^= (uint64_t)(tail[ 0]) << 0;
            k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;
    };
    '''

    k1 = k2 = 0

    sk = nk & 15

'''
def bytes_to_long(bytes):
    assert len(bytes) == 8
    return sum((b << (k * 8) for k, b in enumerate(bytes)))


def murmur64(data, seed = 19820125):
    m = 0xc6a4a7935bd1e995
    r = 47
    MASK = 2 ** 64 - 1
    data_as_bytes = bytearray(data)
    h = seed ^ ((m * len(data_as_bytes)) & MASK)
    off = len(data_as_bytes)/8*8
    for ll in range(0, off, 8):
        k = bytes_to_long(data_as_bytes[ll:ll + 8])
        k = (k * m) & MASK
        k = k ^ ((k >> r) & MASK)
        k = (k * m) & MASK
        h = (h ^ k)
        h = (h * m) & MASK

    l = len(data_as_bytes) & 7

    if l >= 7:
        h = (h ^ (data_as_bytes[off+6] << 48))
    if l >= 6:
        h = (h ^ (data_as_bytes[off+5] << 40))
    if l >= 5:
        h = (h ^ (data_as_bytes[off+4] << 32))
    if l >= 4:
        h = (h ^ (data_as_bytes[off+3] << 24))
    if l >= 3:
        h = (h ^ (data_as_bytes[off+2] << 16))
    if l >= 2:
        h = (h ^ (data_as_bytes[off+1] << 8))
    if l >= 1:
        h = (h ^ data_as_bytes[off])
        h = (h * m) & MASK

    h = h ^ ((h >> r) & MASK)
    h = (h * m) & MASK
    h = h ^ ((h >> r) & MASK)

    return h
'''

def murmur_mod64(key, seed = 19820125):
    m = 0xc6a4a7935bd1e995
    r = 47
    MASK = 2 ** 64 - 1
    nkey = key
    len_key = len(key)
    key_as_bytes = nkey.encode()
    h = seed ^ ((m * len(key_as_bytes)) & MASK)
    rem = len(key_as_bytes) % 8
    nbytes = len(key_as_bytes) - rem
    for i in range(0, nbytes, 8):
        k = sum((b << (k * 8) for k, b in enumerate(key_as_bytes[i:i+8])))
        k = (k * m) & MASK
        k = k ^ ((k >> r) & MASK)
        k = (k * m) & MASK
        h = (h ^ k)
        h = (h * m) & MASK
    for i in range(rem, 1, -1):
        h ^= (key_as_bytes[nbytes-1+i] << (i-1)*8)
    if rem >= 1:
        h = (h * m) & MASK
    h = h ^ ((h >> r) & MASK)
    h = (h * m) & MASK
    h = h ^ ((h >> r) & MASK)
    return [h, hex(h)]

