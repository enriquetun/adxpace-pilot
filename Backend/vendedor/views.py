from django.contrib.auth import models as auth_models, logout, views, login as auth_login, logout as auth_logout
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView, RedirectView
from rest_framework import authentication, permissions, response, status
from rest_framework.generics import ListAPIView, CreateAPIView, ListCreateAPIView, UpdateAPIView, RetrieveDestroyAPIView
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from . import models, mixins, serializers
import comun, staff

# Login view for vendedor

class VendedorLogoutView(comun.views.LogoutView):
    url = '/vendedor/login/'

class VendedorLoginView(comun.views.LoginView):
    next = '/vendedor/'
    group = 'vendedor'
    auth_url = '/vendedor'

# Template view to mount Vue.js app frontend-vendedor

class VendedorIndex(mixins.VendedorMixin, comun.views.Index):
    template_name = 'vendedor/index.html'

class User(mixins.VendedorMixin, ListCreateAPIView):
    serializer_class = comun.serializers.UserSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    queryset = auth_models.User.objects.all()

    def post(self, request, *args, **kwargs):
        username = self.request.data.get('username')
        password = self.request.data.get('password')
        pk_cliente = self.request.data.get('pk_cliente')
        if(auth_models.User.objects.filter(username=username).exists()):
            return JsonResponse({'mensaje': 'Usuario ya existe'}, safe=False)
        else:
            user = auth_models.User(username=username)
            user.save()
            user.set_password(password)
            user.save()
            group = auth_models.Group.objects.get(name='anunciante')
            group.user_set.add(user)
            cliente = models.ClientesJson.objects.get(pk=pk_cliente)
            models.RelacionClienteUsuario(cliente_json=cliente, user=username).save()

            return JsonResponse({'username': username}, safe=False)

# API clases for vendedor

class Clientes(mixins.VendedorMixin, ListCreateAPIView, RetrieveDestroyAPIView):
    serializer_class = serializers.ClientesJsonSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def get_queryset(self, *args, **kwargs):
        clientes_json = models.ClientesJson.objects.all()
        queryset_list = clientes_json

        return queryset_list

    def destroy(self, request, *args, **kwargs):
        self.pk = self.request.GET.get('pk')
        instance = models.ClientesJson.objects.get(pk=self.pk)
        self.perform_destroy(instance)
        return response.Response(status=status.HTTP_204_NO_CONTENT)

class Relaciones(mixins.VendedorMixin, ListCreateAPIView, RetrieveDestroyAPIView):
    serializer_class = serializers.RelacionesClientesJsonSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def get(self, request, *args, **kwargs):
        self.cliente = self.kwargs['cliente']
        return self.list(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        if self.cliente:
            relaciones_clientes_json = models.RelacionesClientesJson.objects.filter(
            cliente_json=self.cliente)
        else:
            relaciones_clientes_json = models.RelacionesClientesJson.objects.all()
        queryset_list = relaciones_clientes_json

        return queryset_list

    def destroy(self, request, *args, **kwargs):
        self.pk = self.request.GET.get('pk')
        instance = models.RelacionesClientesJson.objects.get(pk=self.pk)
        self.perform_destroy(instance)
        return response.Response(status=status.HTTP_204_NO_CONTENT)

class Inventarios(mixins.VendedorMixin, ListCreateAPIView, RetrieveDestroyAPIView):
    serializer_class = serializers.RelacionesInventarioJsonSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)

    def get_queryset(self, *args, **kwargs):
        relaciones_clientes_json = models.RelacionesInventarioJson.objects.filter(
        relacion_clientes_json=self.kwargs['relacion']
        )
        return relaciones_clientes_json

    def destroy(self, request, *args, **kwargs):
        self.pk = self.request.GET.get('pk')
        instance = models.RelacionesInventarioJson.objects.get(pk=self.pk)
        self.perform_destroy(instance)
        return response.Response(status=status.HTTP_204_NO_CONTENT)

class Espacios(staff.views.Espacios):
    pass

class Locales(staff.views.Locales):
    pass

class EspaciosInstitucionales(staff.views.EspaciosInstitucionales):
    pass
