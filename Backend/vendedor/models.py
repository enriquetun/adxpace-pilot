from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from django.db import models
from pilot.models import EspacioPublicitario, Local, EspacioInstitucional

class ClientesJson(models.Model):
    name = models.CharField(max_length=200)
    data = JSONField()

    def __str__(self):
        return self.name

class RelacionClienteUsuario(models.Model):
    cliente_json = models.ForeignKey('ClientesJson', on_delete=models.CASCADE)
    user = models.CharField(max_length=200)

class RelacionesClientesJson(models.Model):
    name = models.CharField(max_length=200)
    cliente_json = models.ForeignKey('ClientesJson', on_delete=models.CASCADE)
    data = JSONField()

    def __str__(self):
        return self.name

class RelacionesInventarioJson(models.Model):
    TIPO_DE_RELACION = (
    ('E','Espacio'),
    ('L', 'Local'),
    ('I', 'Institucional')
    )
    relacion_tipo = models.CharField(max_length=1, choices=TIPO_DE_RELACION)
    relacion_clientes_json = models.ForeignKey('RelacionesClientesJson', on_delete=models.CASCADE)
    data = JSONField()

    def espacio(self):
        if (self.relacion_tipo == 'E'):
            try:
                espacio = EspacioPublicitario.objects.get(pk=int(self.data['pk_relacion']))
                return espacio
            except:
                return EspacioPublicitario()
        else:
            pass

    def local(self):
        if (self.relacion_tipo == 'L'):
            try:
                local = Local.objects.get(pk=int(self.data['pk_relacion']))
                return local
            except:
                return Local()
        else:
            pass

    def institucional(self):
        if (self.relacion_tipo == 'I'):
            try:
                institucional = EspacioInstitucional.objects.get(pk=int(self.data['pk_relacion']))
                return institucional
            except:
                return EspacioInstitucional()
        else:
            pass
