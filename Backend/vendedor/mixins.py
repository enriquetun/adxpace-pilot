from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages

class VendedorPassesTestMixin(UserPassesTestMixin):
    def test_func(self):
        if self.request.user.groups.filter(name__in=['vendedor']).exists():
            return True
        else:
            user = self.request.user
            username = user.username
            user_groups = user.groups.values('name')
            for group in user_groups:
                link = self.request.build_absolute_uri('/') + group['name']
                messages.info(self.request, 'Tiene una sesión activa como: <a href=' + link + '>' + group['name'] + '</a>')
            return False



class VendedorMixin(LoginRequiredMixin, VendedorPassesTestMixin):
    login_url = '/vendedor/login/'
    redirect_field_name = 'redirect_to'

# class VendedorMixin():
#     pass
