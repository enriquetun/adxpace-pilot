from django.urls import path
from pilot.views import logout_view
from . import views

urlpatterns = [
    path('', views.VendedorIndex.as_view(), name='index_vendedor'),
    path('login/', views.VendedorLoginView.as_view(), name='login_vendedor'),
    path('logout/', views.VendedorLogoutView.as_view(), name='logout_vendedor'),
    path('clientes/', views.Clientes.as_view()),
    path('clientes/<int:cliente>/', views.Relaciones.as_view()),
    path('clientes/relaciones/<int:relacion>/', views.Inventarios.as_view()),
    path('clientes/relaciones/', views.Relaciones.as_view()),
    path('espacios/', views.Espacios.as_view()),
    path('locales/', views.Locales.as_view()),
    path('espacios_institucionales/', views.EspaciosInstitucionales.as_view()),
    path('user/', views.User.as_view())
]
