from rest_framework import serializers
from . import models
from staff.serializers import PropietarioSerializer, GeolocalidadSerializer, CaracteristicasFisicasSerializer, GiroSerializer, TipoSerializer, DireccionSerializer, EspacioInstitucionalSerializer

class EspacioPublicitarioSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField()
    geo_localidad = GeolocalidadSerializer()
    giro = GiroSerializer()
    tipo = TipoSerializer()
    foto = serializers.CharField(source='get_foto')
    # foto = serializers.SlugRelatedField(many=False, read_only=True, slug_field='src')

    class Meta:
        model = models.EspacioPublicitario
        fields = ('pk', 'geo_localidad', 'giro', 'foto', 'nombre', 'tipo')

class LocalSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(source='nombre_comercial')
    geo_localidad = GeolocalidadSerializer()
    foto = serializers.CharField(source='get_foto')
    direccion = DireccionSerializer()

    class Meta:
        model = models.Local
        fields = ('pk', 'geo_localidad', 'foto', 'nombre', 'direccion')

class ClientesJsonSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ClientesJson
        fields = ('pk', 'name', 'data')

class RelacionesClientesJsonSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RelacionesClientesJson
        fields = ('pk', 'name', 'cliente_json', 'data')

class RelacionesInventarioJsonSerializer(serializers.ModelSerializer):
    espacio = EspacioPublicitarioSerializer(required=False)
    local = LocalSerializer(required=False)
    institucional = EspacioInstitucionalSerializer(required=False)

    class Meta:
        model = models.RelacionesInventarioJson
        fields = ('pk', 'relacion_tipo', 'relacion_clientes_json', 'data', 'espacio', 'local', 'institucional')
