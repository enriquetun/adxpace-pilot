from rest_framework import serializers
from pilot import models
import comun

class DatosPersonalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DatosPersonales
        fields = ('primer_nombre', 'segundo_nombre', 'apellido_paterno', 'apellido_materno', 'email', 'telefono')

class DatosBancariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DatosBancarios
        fields = ('banco', 'numero_de_cuenta', 'titular')

class DatosComercialesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DatosComerciales
        fields = ('rfc', 'factura')

class PromotorSerializer(serializers.ModelSerializer):
    datos_personales = DatosPersonalesSerializer()
    class Meta:
        model = models.Promotor
        fields = ('pk', 'usuario', 'datos_personales')

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Usuario
        fields = ('username',)

class PropietarioSerializer(serializers.ModelSerializer):
    datos_personales = DatosPersonalesSerializer()
    datos_comerciales = DatosComercialesSerializer()
    datos_bancarios = DatosBancariosSerializer()
    promotor = PromotorSerializer(read_only=True)
    usuario = UsuarioSerializer(read_only=True)
    class Meta:
        model = models.Propietario
        fields = ('pk', 'estatus', 'datos_personales', 'datos_comerciales', 'datos_bancarios', 'fecha_de_registro', 'fecha_de_actualizacion', 'check_bancarios', 'promotor', 'usuario')

    def update(self, instance, validated_data):
        propietario = instance
        datos_personales_data = validated_data.pop('datos_personales', False)
        datos_comerciales_data = validated_data.pop('datos_comerciales', False)
        datos_bancarios_data = validated_data.pop('datos_bancarios', False)

        if(datos_personales_data):
            datos_personales = DatosPersonalesSerializer(instance.datos_personales, data=datos_personales_data, partial=True)
            datos_personales.is_valid()
            datos_personales.save()

        if(datos_comerciales_data):
            if(instance.datos_comerciales):
                datos_comerciales = DatosComercialesSerializer(instance.datos_comerciales, data=datos_comerciales_data, partial=True)
                datos_comerciales.is_valid()
                datos_comerciales.save()
            else:
                datos_comerciales = models.DatosComerciales.objects.create(**datos_comerciales_data)
                instance.datos_comerciales = datos_comerciales
                instance.save()

        if(datos_bancarios_data):
            if(instance.datos_bancarios):
                datos_bancarios = DatosBancariosSerializer(instance.datos_bancarios, data=datos_bancarios_data, partial=True)
                datos_bancarios.is_valid()
                datos_bancarios.save()
            else:
                datos_bancarios = models.DatosBancarios.objects.create(**datos_bancarios_data)
                instance.datos_bancarios = datos_bancarios
                instance.save()

        return propietario

class FotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Foto
        fields = ('src',)

class GeolocalidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Geolocalidad
        fields = ('latitud', 'longitud')

class GiroSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Giro
        fields = ('pk', 'designacion',)

class TipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tipo
        fields = ('pk', 'designacion',)
        extra_kwargs = {
        'designacion':{
            'validators': []
        },
        }

class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Material
        fields = ('pk', 'designacion',)

class CaracteristicasFisicasSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.CaracteristicasFisicas
        fields = ('pk', 'alto', 'ancho', 'digital', 'descripcion', 'material')

    def update(self, instance, validated_data):
        caracteristicas_fisicas = instance
        material_data = validated_data.pop('material', False)
        alto = validated_data.pop('alto', False)
        ancho = validated_data.pop('ancho', False)
        digital = validated_data.pop('digital', False)
        descripcion = validated_data.pop('descripcion', False)

        if(material_data):
            caracteristicas_fisicas.material = material_data

        if(alto):
            caracteristicas_fisicas.alto = alto

        if(ancho):
            caracteristicas_fisicas.ancho = ancho

        if(digital):
            caracteristicas_fisicas.digital = digital

        if(descripcion):
            caracteristicas_fisicas.descripcion = descripcion

        caracteristicas_fisicas.save()

        return caracteristicas_fisicas

class DireccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Direccion
        fields = ('calle', 'numero_exterior', 'numero_interior', 'colonia', 'codigo_postal', 'municipio', 'estado')

class EspacioPublicitarioSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()
    caracteristicas_fisicas = CaracteristicasFisicasSerializer()
    foto = serializers.CharField(source='get_foto', read_only=True)

    class Meta:
        model = models.EspacioPublicitario
        fields = ('propietario', 'pk', 'nombre', 'fecha_de_registro', 'fecha_de_actualizacion', 'geo_localidad', 'caracteristicas_fisicas', 'giro', 'tipo', 'foto')

    def update(self, instance, validated_data):
        espacio = instance

        geo_localidad_data = validated_data.pop('geo_localidad', False)
        nombre = validated_data.pop('nombre', False)
        caracteristicas_fisicas_data = validated_data.pop('caracteristicas_fisicas', False)
        giro_data = validated_data.pop('giro', False)
        tipo_data = validated_data.pop('tipo', False)

        if(geo_localidad_data):
            geo_localidad = GeolocalidadSerializer(instance.geo_localidad, data=geo_localidad_data, partial=True)
            geo_localidad.is_valid()
            geo_localidad.save()

        if(caracteristicas_fisicas_data):
            caracteristicas_fisicas = CaracteristicasFisicasSerializer(instance.caracteristicas_fisicas, data=caracteristicas_fisicas_data, partial=True)
            caracteristicas_fisicas.is_valid()
            caracteristicas_fisicas.save()

        if(giro_data):
            espacio.giro = giro_data

        if(tipo_data):
            espacio.tipo = tipo_data

        if(nombre):
            espacio.nombre = nombre
        espacio.save()

        return espacio

class LocalSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()
    foto = FotoSerializer()
    direccion = DireccionSerializer()
    propietario = PropietarioSerializer()

    class Meta:
        model = models.Local
        fields = ('pk', 'nombre_comercial', 'espacio_tipo', 'geo_localidad', 'foto', 'direccion', 'propietario', 'giro')

    def update(self, instance, validated_data):
        local = instance
        geo_localidad_data = validated_data.pop('geo_localidad', False)
        foto_data = validated_data.pop('foto', False)
        direccion_data = validated_data.pop('direccion', False)
        nombre_comercial = validated_data.pop('nombre_comercial', False)
        espacio_tipo = validated_data.pop('espacio_tipo', False)
        giro = validated_data.pop('giro', False)

        if(direccion_data):
            direccion = DireccionSerializer(instance.direccion, data=direccion_data, partial=True)
            direccion.is_valid()
            direccion.save()

        if(geo_localidad_data):
            geo_localidad = GeolocalidadSerializer(instance.geo_localidad, data=geo_localidad_data, partial=True)
            geo_localidad.is_valid()
            geo_localidad.save()

        if(foto_data):
            foto = FotoSerializer(instance.foto, data=foto_data, partial=True)
            foto.is_valid()
            foto.save()

        if(nombre_comercial):
            local.nombre_comercial = nombre_comercial

        if(espacio_tipo):
            local.espacio_tipo = espacio_tipo

        if(giro):
            local.giro = giro

        local.save()

        return local

class EspacioInstitucionalSerializer(serializers.ModelSerializer):
    geo_localidad = GeolocalidadSerializer()

    class Meta:
        model = models.EspacioInstitucional
        fields = ('pk', 'geo_localidad', 'data',)
