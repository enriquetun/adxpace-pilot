from django.urls import path
from . import views

urlpatterns = [
    path('', views.StaffIndex.as_view(), name='index_staff'),
    path('login/', views.StaffLoginView.as_view(), name='login_staff'),
    path('logout/', views.StaffLogoutView.as_view(), name='logout_staff'),
    path('promotores/', views.Promotores.as_view()),
    path('promotores/<int:pk>', views.PropietarioPromotor.as_view()),
    path('propietarios/', views.Propietarios.as_view()),
    path('propietarios/<int:pk>', views.Propietario.as_view()),
    path('propietarios/espacios/<int:id>', views.espacios_por_propietario), # cambiar a Classs base
    path('propietarios/locales/<int:id>', views.locales_por_propietario), # cambiar a Classs base
    path('caracteristicas_fisicas/<int:pk>', views.CaracteristicasFisicas.as_view()),
    path('espacios/', views.Espacios.as_view()),
    path('espacios/<int:pk>', views.Espacio.as_view()),
    path('espacios_institucionales/', views.EspaciosInstitucionales.as_view()),
    path('locales/', views.Locales.as_view()),
    path('locales/<int:pk>', views.Local.as_view()),
    path('locales_sin_giro', views.SinGiro.as_view()),
    path('password_reset/', views.CustomPasswordResetView.as_view(), name='custom_password_reset'),
]
