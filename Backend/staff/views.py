from django.contrib.auth.views import PasswordResetView, LoginView, LogoutView
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.template import loader
from django.views.generic.base import TemplateView
import googlemaps
from rest_framework import generics, permissions
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from . import mixins, forms, serializers, pagination

import pilot, comun.views

class StaffLoginView(comun.views.LoginView):
    next = '/staff/'
    group = 'staff'
    auth_url = '/staff'

class StaffLogoutView(comun.views.LogoutView):
    url = '/staff/login/'


class StaffIndex(mixins.StaffMixin, TemplateView):
    template_name = 'staff/index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        auth_login(self.request, form.get_user())
        if (self.request.user.groups.filter(name__in=['staff']).exists()):
            return HttpResponseRedirect(self.get_success_url())
        else:
            logout(self.request)
            return HttpResponseRedirect('/staff')

class CustomPasswordResetView(PasswordResetView):
    form_class = forms.CustomPasswordResetForm

class SinGiro(generics.ListAPIView):
    serializer_class = serializers.LocalSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    queryset = pilot.models.Local.objects.filter(giro=None)

class Locales(generics.ListAPIView):
    serializer_class = serializers.LocalSerializer
    pagination_class = pagination.CustomPageNumberPagination
    pagination_class.page_size = 100
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    pk = None
    point = None
    queryset = pilot.models.Local.objects.all()
    giro = pilot.models.Giro.objects.all()

    point = None
    radius = 1

    def get(self, request, *args, **kwargs):
        if(self.request.GET.get('pk')):
            self.pk = pilot.models.Local.objects.filter(pk=self.request.GET.get('pk'))
            return self.list(request, *args, **kwargs)

        if (self.request.GET.get('direccion') or self.request.GET.get('geo_localidad')):
            self.point = set_point(self.request)

        if(self.request.GET.get('radius')):
            self.radius = self.request.GET.get('radius')

        if(self.request.GET.get('giro')):
            search_array = convert_html_paramters_to_array(self.request.GET.get('giro'))
            self.giro = pilot.models.Giro.objects.filter(pk__in = search_array)

        return self.list(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        if(self.pk):
            locales = pilot.models.Local.objects.filter(
            Q(pk__in = self.pk)
            )
            return locales

        if(self.point):
            locales = pilot.models.Local.objects.filter(
            Q(giro__in = self.giro) &
            Q(geo_localidad__pnt__distance_lt=(self.point, Distance(km=self.radius)))
            ).order_by('-fecha_de_registro')

            return locales

        else:
            return self.queryset

class EspaciosInstitucionales(generics.ListAPIView):
    serializer_class = serializers.EspacioInstitucionalSerializer
    pagination_class = pagination.CustomPageNumberPagination
    pagination_class.page_size = 100
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.EspacioInstitucional.objects.all()
    radius = 1
    point = None
    pk = None
    tipo = [field['data__tipo'] for field in pilot.models.EspacioInstitucional.objects.values('data__tipo').distinct()]

    def get(self, request, *args, **kwargs):
        if(self.request.GET.get('pk')):
            self.pk = pilot.models.EspacioInstitucional.objects.filter(pk=self.request.GET.get('pk'))
            return self.list(request, *args, **kwargs)

        if (self.request.GET.get('direccion') or self.request.GET.get('geo_localidad')):
            self.point = set_point(self.request)

        if(self.request.GET.get('radius')):
            self.radius = self.request.GET.get('radius')

        if(self.request.GET.get('ooh_tipo')):
            self.tipo = self.request.GET.get('ooh_tipo').split(',')

        return self.list(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        if(self.pk):
            espacios = pilot.models.EspacioInstitucional.objects.filter(
            Q(pk__in = self.pk)
            )

            return espacios

        if(self.point):
            espacios_institucionales = pilot.models.EspacioInstitucional.objects.filter(
            Q(geo_localidad__pnt__distance_lt=(self.point, Distance(km=self.radius))),
            Q(data__tipo__in=self.tipo)
            ).order_by('-fecha_de_registro')

            return espacios_institucionales

        else:
            espacios_institucionales = pilot.models.EspacioInstitucional.objects.filter(
            Q(data__tipo__in=self.tipo)
            ).order_by('-fecha_de_registro')

            return espacios_institucionales


class Local(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.LocalSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.Local.objects.all()

class CaracteristicasFisicas(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.CaracteristicasFisicasSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.CaracteristicasFisicas.objects.all()

class Espacios(generics.ListAPIView):
    serializer_class = serializers.EspacioPublicitarioSerializer
    pagination_class = pagination.CustomPageNumberPagination
    pagination_class.page_size = 100
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.EspacioPublicitario.objects.all().order_by('-fecha_de_registro')
    giro = pilot.models.Giro.objects.all()
    tipo = pilot.models.Tipo.objects.all()
    pk = None
    point = None
    radius = 1

    def get(self, request, *args, **kwargs):

        if(self.request.GET.get('pk')):
            self.pk = pilot.models.EspacioPublicitario.objects.filter(pk=self.request.GET.get('pk'))
            return self.list(request, *args, **kwargs)

        if(self.request.GET.get('giro')):
            self.giro = pilot.models.Giro.objects.filter(pk = self.request.GET.get('giro'))

        if(self.request.GET.get('tipo')):
            self.tipo = pilot.models.Tipo.objects.filter(pk = self.request.GET.get('tipo'))

        if (self.request.GET.get('direccion') or self.request.GET.get('geo_localidad')):
            self.point = set_point(self.request)

        if(self.request.GET.get('radius')):
            self.radius = self.request.GET.get('radius')

        if(self.request.GET.get('page_size')):
            self.pagination_class.page_size = self.request.GET.get('page_size')

        return self.list(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):

        if(self.pk):
            espacios = pilot.models.EspacioPublicitario.objects.filter(
            Q(pk__in = self.pk)
            )

            return espacios

        elif(self.point):
            espacios = pilot.models.EspacioPublicitario.objects.filter(
            Q(giro__in = self.giro) &
            Q(tipo__in = self.tipo) &
            Q(geo_localidad__pnt__distance_lt=(self.point, Distance(km=self.radius)))
            ).order_by('-fecha_de_registro')

            return espacios

        espacios = pilot.models.EspacioPublicitario.objects.filter(
        Q(giro__in = self.giro) &
        Q(tipo__in = self.tipo)
        ).order_by('-fecha_de_registro')

        return espacios

class Espacio(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.EspacioPublicitarioSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.EspacioPublicitario.objects.all()

    def update(self, request, *args, **kwargs):

        if(request.FILES.get('foto.src')):
            instance = self.update_foto(request, *args, **kwargs)
            return Response(serializers.EspacioPublicitarioSerializer(instance).data)

        else:
            return super(generics.RetrieveUpdateAPIView, self).update(request, *args, **kwargs)

    def update_foto(self, request, *args, **kwargs):
        espacio_publicitario = pilot.models.EspacioPublicitario.objects.get(pk=self.kwargs['pk'])
        foto = pilot.models.Foto.objects.get(espacio_publicitario=espacio_publicitario)
        serializer = serializers.FotoSerializer(foto, data={'src': request.FILES.get('foto.src')}, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        instance = self.get_object()
        return instance

class Propietarios(generics.ListAPIView):
    serializer_class = serializers.PropietarioSerializer
    pagination_class = PageNumberPagination
    pagination_class.page_size = 100
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    pk=None

    def get(self, request, *args, **kwargs):
        if(self.request.GET.get('pk')):
            self.pk = pilot.models.Propietario.objects.filter(pk=self.request.GET.get('pk'))
            return self.list(request, *args, **kwargs)

        return self.list(request, *args, **kwargs)


    def get_queryset(self, *args, **kwargs):
        if(self.pk):
            propietario = pilot.models.Propietario.objects.filter(
            Q(pk__in = self.pk)
            )

            return propietario

        else:
            propietarios = pilot.models.Propietario.objects.all().order_by('-pk')

        return propietarios

class Propietario(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.PropietarioSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.Propietario.objects.all()

class Promotores(generics.ListAPIView):
    serializer_class = serializers.PromotorSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.Promotor.objects.all()

    def list(self, request, *args, **kwargs):
        promotores = pilot.models.Promotor.objects.all()
        context = [{
                'pk' : promotor.pk,
                'primer_nombre' : promotor.datos_personales.primer_nombre,
                'apellido_paterno' : promotor.datos_personales.apellido_paterno,
                'apellido_materno' : promotor.datos_personales.apellido_materno,
                'promociones' : promociones_por_promotor(promotor),
                'email' : promotor.datos_personales.email,
                'telefono' : promotor.datos_personales.telefono,
                }for promotor in promotores]

        return Response(context)

class PropietarioPromotor(generics.GenericAPIView):
    allowed_methods = ['GET',]
    serializer_class = serializers.PromotorSerializer
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = pilot.models.Promotor.objects.all()

    def get(self, request, *args, **kwargs):
        return self.get_object(request, *args, **kwargs)

    def get_object(self, request, *args, **kwargs):

        promotor = pilot.models.Promotor.objects.get(**kwargs)
        promociones = pilot.models.Promociones.objects.filter(promotor=promotor)
        propietarios_list = [promocion.propietario.pk for promocion in promociones]
        propietarios = pilot.models.Propietario.objects.filter(pk__in=propietarios_list)

        context = [{
        'pk': propietario.pk,
        'estatus' : propietario.estatus,
        'estatus_bancario': propietario.check_bancarios(),
        'primer_nombre' : propietario.datos_personales.primer_nombre,
        'apellido_paterno' : propietario.datos_personales.apellido_paterno,
        'apellido_materno' : propietario.datos_personales.apellido_materno,
        }for propietario in propietarios]

        return Response(context)

def set_point(request):
    if(request.GET.get('direccion')):
        gmaps = googlemaps.Client(key='AIzaSyDAmvouc8wiOPjIXp_CdOLlJvtYTL6RwTo')
        geocode_result = gmaps.geocode(request.GET.get('direccion') + ', Mexico')
        lat = geocode_result[0]['geometry']['location']['lat']
        lng = geocode_result[0]['geometry']['location']['lng']
        point = Point(lng, lat)

        return point

    elif (request.GET.get('geo_localidad')):
        geo_localidad = request.GET.get('geo_localidad')
        geo_localidad = geo_localidad.split(',')
        lat = float(geo_localidad[0])
        lng = float(geo_localidad[1])
        point = Point(lng, lat)

        return point

def convert_html_paramters_to_array(htmls):
    text_array = htmls.split(',')
    array = [float(data) for data in text_array]
    return array

def espacios_por_propietario(request, id):
    propietario = pilot.models.Propietario.objects.get(pk=id)
    espacios = pilot.models.EspacioPublicitario.objects.filter(propietario=propietario)
    context=[{
    'pk': espacio.pk,
    'nombre' : espacio.nombre,
    'geo_localidad' : [espacio.geo_localidad.latitud, espacio.geo_localidad.longitud],
    'giro' :  [espacio.giro.pk, espacio.giro.designacion],
    'tipo' : [espacio.tipo.pk, espacio.tipo.designacion],
    'material' : [espacio.caracteristicas_fisicas.material.pk, espacio.caracteristicas_fisicas.material.designacion],
    'digital' : espacio.caracteristicas_fisicas.digital,
    'descripcion' : espacio.caracteristicas_fisicas.descripcion,
    'dimensiones' : [espacio.caracteristicas_fisicas.alto, espacio.caracteristicas_fisicas.ancho],
    'foto' : espacio.get_foto(),
    'registro' : espacio.fecha_de_registro,
    'actualizacion' : espacio.fecha_de_actualizacion,
    }for espacio in espacios]

    return JsonResponse(context, safe=False)

def locales_por_propietario(request, id):
    propietario = pilot.models.Propietario.objects.get(pk=id)
    locales = pilot.models.Local.objects.filter(propietario=propietario)
    context=[{
    'pk': local.pk,
    'geo_localidad' : [local.geo_localidad.latitud, local.geo_localidad.longitud],
    'nombre_comercial': local.nombre_comercial,
    'espacio_tipo': local.espacio_tipo,
    'calle': local.direccion.calle,
    'numero_exterior': local.direccion.numero_exterior,
    'numero_interior': local.direccion.numero_interior,
    'colonia': local.direccion.colonia,
    'codigo_postal': local.direccion.codigo_postal,
    'municipio': local.direccion.municipio,
    'estado': local.direccion.estado,
    'foto': local.get_foto(),
    }for local in locales]

    return JsonResponse(context, safe=False)

def promociones_por_promotor(promotor):
    nr_promociones = [0,0,0]
    if bool(promotor):
        promociones = get_promociones(promotor)
        nr_promociones[0] = promociones.count()
        propietarios = [promocion.propietario for promocion in promociones]
        ids = [promocion.propietario_id for promocion in promociones]
        nr_promociones[1] = pilot.models.Propietario.objects.filter(id__in = ids, estatus = 'R').count()
        nr_promociones[2] = pilot.models.EspacioPublicitario.objects.filter(propietario__in = propietarios).count()
    return nr_promociones

def get_promociones(promotor):
    return pilot.models.Promociones.objects.filter(promotor=promotor)
