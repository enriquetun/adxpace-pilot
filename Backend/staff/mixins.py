from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

class StaffPassesTestMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.groups.filter(name__in=['staff']).exists()

class StaffMixin(LoginRequiredMixin, StaffPassesTestMixin):
    login_url = '/staff/login/'
    redirect_field_name = 'redirect_to'
