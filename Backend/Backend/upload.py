import boto3
from pilot.models import Foto

class Spaces:

    region_name = 'sfo2'
    endpoint_url = 'https://adxpace-dev.sfo2.digitaloceanspaces.com'
    key_id = '33C3NJPJ5DULGFYVOJVS'
    secret = 'aLhKtgsvXTsCJR93T6Pj4qnK+LINvx4mYluYFoIn0To'
    bucket = 'media'

    def __init__(self):
        session = boto3.session.Session()
        self.client = session.client('s3', region_name=self.region_name, endpoint_url=self.endpoint_url, aws_access_key_id=self.key_id, aws_secret_access_key=self.secret)
        print('Object initilize')

    def upload_file_from_object(self, foto):
        try:
            path = '/code' + foto.src
            file_name = path.split("/")[3]
            self.client.upload_file(path, 'media', file_name, ExtraArgs={'ACL':'public-read'})
        except:
            print('file not found.')

def upload_fotos():
    fotos = Foto.objects.all()
    spaces = Spaces()
    for foto in fotos:
        spaces.upload_file_from_object(foto)

def change_names():
    fotos = Foto.objects.all()
    for foto in fotos:
        name = foto.src.name
        new_name = name.split("/")[2]
        foto.src.name = new_name
        foto.save()
