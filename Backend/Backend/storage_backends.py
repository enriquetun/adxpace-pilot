from storages.backends.s3boto3 import S3Boto3Storage

class MediaStorage(S3Boto3Storage):
    AWS_STORAGE_BUCKET_NAME = 'media'
    # location = 'media'
    file_overwrite = False
