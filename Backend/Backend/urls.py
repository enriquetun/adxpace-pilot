from django.contrib import admin
from django.urls import include, path
from staff.views import CustomPasswordResetView
from pilot.views import logout_view
from django.contrib.auth import logout

urlpatterns = [
    path('', include('landing.urls')),
    path('landing2/', include('landing2.urls')),
    path('comun/', include('comun.urls')),
    path('pilot/', include('pilot.urls')),
    path('anunciante/', include('anunciante.urls')),
    path('propietario/', include('propietario.urls')),
    path('staff/', include('staff.urls')),
    path('vendedor/', include('vendedor.urls')),
    path('logout/', logout_view),
    path('admin/', admin.site.urls),
    path('accounts/password_reset/', CustomPasswordResetView.as_view()),
    path('accounts/', include('django.contrib.auth.urls')),
]
