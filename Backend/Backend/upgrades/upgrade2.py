from pilot import models
from datetime import datetime
import os, string
from openpyxl import load_workbook
from django.contrib.auth import get_user_model, models as auth_models
from django.utils import timezone

def bulk_upload():
    wb = load_workbook(filename = 'giros_locales.xlsx')
    sheet = wb['final']
    rows = sheet.max_row
    n = 0
    for row in range(2, rows):
        pk_propietario = sheet['A'+str(row)].value
        pk_giro = sheet['J'+str(row)].value
        giro = models.Giro.objects.get(pk=pk_giro)
        try:
            propietario = models.Propietario.objects.get(pk=pk_propietario)
            locales = models.Local.objects.filter(propietario=propietario)
            locales.update(giro=giro)
        except:
            n = n + 1
            print(pk_propietario)
    print(n)
