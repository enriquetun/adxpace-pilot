import pilot
import os, string
from openpyxl import load_workbook

espacio_delete=[185, 208,  655, 672, 688, 916, 1019, 1024, 1025, 1026, 1095, 1110, 1127, 1148, 1149, 1158, 1252, 1333, 1366, 1468, 1538, 1541, 1551, 1573, 1574, 1621, 1622, 1648, 1654, 2160, 2265, 2267, 2299, 2300, 2301, 2302, 2316, 2343, 2366, 2374, 2412, 2418, 2435, 2521, 2596, 2614, 2673, 2674, 2675, 2679, 2726, 2808, 2841, 2864, 2903, 2911, 2966, 3015, 3025, 3059, 3060, 3076, 3108, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149, 3150, 3151, 3153, 3156, 3158, 3159, 3160, 3161, 3162, 3163, 3169, 3173, 3174, 3182, 3183, 3193, 3194, 3205, 3209, 3210, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219, 3220, 3221, 3222, 3223, 3276, 3306, 3339, 3345, 3391, 3525, 3526, 3594, 3664, 3717, 3722, 3729, 3735, 3736, 3737, 3738, 3739, 3745, 3830, 3833, 3853, 3854, 3856, 3857, 3858, 3859, 3860, 3892, 3929, 3968, 3984, 3990, 4046, 4047, 4062, 4085, 4090, 4094, 4095, 4101, 4116, 4138, 4144, 4163, 4186, 4190, 4191, 4284, 4329, 4334, 4364, 4365, 4425, 4426, 4497, 4498, 4512, 4550, 4557, 4597, 4598, 4603, 4653, 4658, 4659, 4675, 4676, 4681, 4710, 4779, 4848, 4849, 4856, 4857, 4862, 4863, 4869, 4905, 4906, 4922, 4951, 4965, 4972, 4973, 4978, 5046, 5091, 5119, 5127, 5130, 5145, 5162, 5166, 5168, 5171, 5172, 5203, 5204, 5205, 5265, 5269, 5273, 5274, 5286, 5299, 5300, 5301, 5312, 5323, 5329, 5330, 5357, 5363, 5404, 5418]

local_delete=[54, 55, 57, 72, 273, 329, 331, 360, 423, 429, 430, 431, 439, 469, 471, 472, 515, 542, 551, 565, 567, 571, 667, 728, 882, 930, 933, 991, 1024, 1034, 1035, 1042, 1048, 1050, 1096, 1108, 1112, 1122, 1154, 1163, 1201, 1204, 1336, 1356, 1489, 1494, 1536, 1537, 1565, 1570, 1583, 1585, 1603, 1606, 1620, 1653, 1684, 1827, 1899, 1940, 1941, 1950, 1963, 1970, 1979, 1981, 1985, 1986, 2017, 2021, 2121, 2122, 2142, 2149, 2204]

registro_delete=[248, 312, 330, 346, 358, 364, 370, 424, 468, 502, 536, 684, 758, 934, 966, 999, 1000, 1006, 1007, 1022, 1034, 1039, 1047, 1058, 1069, 1077, 1078, 1105, 1110, 1126, 1135, 1147, 1151, 1152, 1153, 1161, 1201, 1239, 1261, 1264, 1280, 1283, 1286, 1288, 1316, 1332, 1348, 1354, 1375, 1377, 1393, 1395, 1396, 1402, 1418, 1425, 1433, 1435, 1437, 1451, 1477, 1482, 1490, 1496, 1500, 1502, 1503, 1521, 1522, 1534, 1537, 1612, 1666, 1667, 1685, 1686, 1708, 1718, 1719, 1726, 1727, 1737, 1762, 1764, 1779, 1785, 1788, 1791, 1852, 1859, 1867, 1872, 1879, 1881, 1884, 1886, 1912, 1927, 1969, 1998, 2009, 2043, 2059, 2071, 2092, 2094, 2102, 2108, 2112, 2152, 2168, 2169, 2170, 2171, 2172, 2173, 2174, 2175, 2176, 2177, 2178, 2180, 2181, 2183, 2184, 2186, 2187, 2189, 2194, 2204, 2205, 2206, 2207, 2212, 2221, 2230, 2272, 2285, 2315, 2382, 2389, 2390, 2406, 2408, 2415, 2448, 2449, 2453, 2462, 2469, 2474, 2518, 2559, 2561]

class UploadImages(object):

    def __init__(self, espacios_institucionales, path):
        self.espacios_institucionales = espacios_institucionales
        self.path = path

    def start_upgrade(self):
        path = self.path
        for espacio in self.espacios_institucionales:
            directory_list = self.search_directory(espacio.data['codigo'], path)
            image_list = [path + '/' + image for image in directory_list]
            if(image_list):
                espacio.data['foto'] = image_list
                espacio.save()

    def search_directory(self, codigo, path):
        full_path = '/code' + path
        image_list = [filename for filename in os.listdir(full_path) if filename.startswith(str(codigo))]
        return image_list

def bulk_upload(filename, path):
    last_espacio_institucional = pilot.models.EspacioInstitucional.objects.last()
    wb = load_workbook(filename = filename)
    sheet = wb['espacios']
    rows = sheet.max_row
    columns = sheet.max_column
    abc = list(string.ascii_uppercase)
    abc.append('AA')
    abc.append('AB')
    abc.append('AC')
    for row in range(2, rows):
        data = {}
        for column in range(0, columns):
            data[sheet[abc[column] + str(1)].value] = sheet[abc[column] + str(row)].value
        latitud = str(data['latitud'])
        longitud = str(data['longitud'])
        geo_localidad = pilot.models.Geolocalidad(latitud=latitud, longitud=longitud)
        geo_localidad.save()
        pilot.models.EspacioInstitucional(geo_localidad=geo_localidad, data=data).save()

    espacios_institucionales=pilot.models.EspacioInstitucional.objects.filter(pk__gte=last_espacio_institucional.pk)
    image = UploadImages(espacios_institucionales, path)
    image.start_upgrade()

def e_upgrade():
    batch4_filename = 'batch4.xlsx'
    batch4_path = '/media/espacios-institucionales-4'
    bulk_upload(batch4_filename, batch4_path)
    batch5_filename = 'batch5.xlsx'
    batch5_path = '/media/espacios-institucionales-5'
    bulk_upload(batch5_filename, batch5_path)
    locales = pilot.models.Local.objects.filter(pk__in=local_delete)
    pilot.models.EspacioPublicitario.objects.filter(local__in=locales).delete()
    locales.delete()
    pilot.models.EspacioPublicitario.objects.filter(pk__in=espacio_delete).delete()
    pilot.models.Propietario.objects.filter(pk__in=registro_delete).delete()
    print('Upgrade4... DONE')
