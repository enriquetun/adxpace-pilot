from pilot import models
from datetime import datetime
import os, string
from openpyxl import load_workbook
from django.contrib.auth import get_user_model, models as auth_models
from django.utils import timezone

UserModel = get_user_model()

giros = [
{'Aeropuerto': []},
{'Autoservicio / Supermercado': [4,]},
{'Barbería / Peluquería (hombre)': [9,]},
{'Carnicería / Pollería': [13,]},
{'Cafetería': [10,]},
{'Central camionera': []},
{'Centro comercial': [18, 19]},
{'Cocina económica / Fonda': [12,]},
{'Consultorio Médico / Óptica / Podólogo / Dentista': [16,]},
{'Escuela': [24,]},
{'Espectáculos públicos': []},
{'Estacionamiento / Lavado de autos': []},
{'Estética / Salón de belleza / Uñas (mujer)': [8,]},
{'Farmacia': [3,]},
{'Ferretería / Tlapalería': [5,]},
{'Gimnasio / Spa / Cuidado de la salud': [21,]},
{'Guardería': []},
{'Hospital': []},
{'Internet / Reparación / Venta de equipo celular y cómputo': []},
{'Jarcería / Artículos del hogar': []},
{'Joyería':[]},
{'Laboratorio clínico':[]},
{'Lavandería / Tintorería / Planchaduría':[20,]},
{'Mercería':[6,]},
{'Miscelánea / Cremería': [1,]},
{'Mueblería / Carpintería': []},
{'Panadería / Repostería / Pastelería': [7,]},
{'Papelería / Fotocopiado': [2,]},
{'Refaccionaria': []},
{'Restaurante': [11,]},
{'Salones / Artículos de Fiesta / Dulcería': []},
{'Taller mecánico': [15,]},
{'Tienda de artículos de hombre': []},
{'Tienda de artículos de mujer': []},
{'Tortillería': []},
{'Transporte público': [22,23]},
{'Universidad': []},
{'Valet Parking': []},
{'Veterinario / Estética canina': [17,]},
{'Vinatería y ultramarinos': [14,]},
{'Zapatería / Reparador de calzado': []},
{'Otro': []}
]

promotores_full_delete = [
1,
2,
3,
4,
11,
]

promotores_partial_delete = [
[10, timezone.make_aware(datetime(2018, 6, 12))],
[13, timezone.make_aware(datetime(2018, 6, 26))],
[14, timezone.make_aware(datetime(2018, 7, 5))],
[17, timezone.make_aware(datetime(2018, 8, 7))],
[16, timezone.make_aware(datetime(2018, 8, 7))],
[18, timezone.make_aware(datetime(2018, 8, 16))],
[19, timezone.make_aware(datetime(2018, 8, 17))],
[15, timezone.make_aware(datetime(2018, 9, 27))]
]

eduardo_promotor = models.Promotor.objects.get(pk=15)

# from Backend.upgrades.upgrade1 import run_upgrade

institucionales_images_path_a = '/media/espacios-institucionales-1'
institucionales_images_path_b = '/media/espacios-institucionales-2'
institucionales_images_path_c = '/media/espacios-institucionales-3'

def run_upgrade():
    bulk_upload('batch1.xlsx')
    bulk_upload('batch2.xlsx')
    bulk_upload('batch3.xlsx')
    UploadInsitucionalesImages().start_upgrade(institucionales_images_path_a)
    UploadInsitucionalesImages().start_upgrade(institucionales_images_path_b)
    UploadInsitucionalesImages().start_upgrade(institucionales_images_path_c)
    DeletePropietarios().start_upgrade()
    PromotoresUpgrade().start_upgrade()
    GiroUpgrade().start_upgrade()
    CleanDb().start_upgrade()

class DeletePropietarios(object):
    wb = load_workbook(filename = 'borrar_propietarios.xlsx')
    sheet = wb['borrar']
    rows = 332
    propietarios_list = []
    deleted_propietarios_list = []
    usuarios_list = []

    def get_propietarios_list(self):
        for row in range(1, self.rows):
            self.propietarios_list.append(self.sheet['A' + str(row)].value)
        return True

    def delete_propietarios_list(self):
        propietarios = models.Propietario.objects.filter(pk__in=self.propietarios_list)
        for propietario in propietarios:
            self.deleted_propietarios_list.append(propietario.pk)
            self.usuarios_list.append(propietario.usuario)
        propietarios.delete()
        return True

    def delete_usuarios_list(self):
        usuarios = models.Usuario.objects.filter(username=self.usuarios_list)
        for usuario in usuarios:
            UserModel.objects.filter(username=usuario.username).delete()
        usuarios.delete()
        return True

    def create_report(self):
        with open('listfile.txt', 'a') as filehandle:
            filehandle.write('De propietarios tabla Excel:\n')
            for listitem in self.deleted_propietarios_list:
                filehandle.write('%s\n' % listitem)
            filehandle.close()
        return True

    def start_upgrade(self):
        self.get_propietarios_list()
        self.delete_propietarios_list()
        self.create_report()
        self.delete_usuarios_list()

class UploadInsitucionalesImages(object):
    espacios_institucionales = models.EspacioInstitucional.objects.all()
    counter = 0

    def start_upgrade(self, path):
        for espacio in self.espacios_institucionales:
            directory_list = self.search_directory(espacio.data['codigo'], path)
            image_list = [path + '/' + image for image in directory_list]
            if(image_list):

                self.counter = self.counter + 1
                espacio.data['foto'] = image_list
                espacio.save()
                # f = open(path, 'rb')
                # foto = models.Foto(src=File(f))
                # foto.save()
                # foto.pk
                # f.close


    def search_directory(self, codigo, path):
        full_path = '/code' + path
        image_list = [filename for filename in os.listdir(full_path) if filename.startswith(str(codigo))]
        return image_list


class PromotoresUpgrade(object):
    promociones_full = models.Promociones.objects.filter(promotor__pk__in=promotores_full_delete)
    promociones_partial = promotores_partial_delete
    deleted_propietarios_list = []

    def test(self, promotor, fecha):
        promociones = models.Promociones.objects.filter(promotor__pk=promotor)

        propietarios_list = [
        promocion.propietario.pk for promocion in promociones
        ]

        propietarios = models.Propietario.objects.filter(pk__in=propietarios_list, fecha_de_registro__lt=fecha)
        for propietario in propietarios:
            print(propietario.pk, ' ', promotor)

    def delete_partial(self, promotor, fecha):
        promociones = models.Promociones.objects.filter(promotor__pk=promotor)
        propietarios_list = [
        promocion.propietario.pk for promocion in promociones
        ]
        propietarios = models.Propietario.objects.filter(pk__in=propietarios_list, fecha_de_registro__lt=fecha)
        for propietario in propietarios:
            print(propietario.pk, ' ', promotor)
            try:
                usuario = propietario.usuario
                if(usuario):
                    self.deleted_propietarios_list.append(propietario.pk)
                    usuario.delete()
                else:
                    self.deleted_propietarios_list.append(propietario.pk)
                    propietario.delete()
            except:
                print('error delete ', + propietario.pk)

    def delete_full(self, promociones):
        for promocion in promociones:
            try:
                usuario = promocion.propietario.usuario
                if(usuario):
                    usuario.delete()
                else:
                    promocion.propietario.delete()
            except:
                print(promocion.pk)

    def assign_eduardo(self, promotor):

            promociones = models.Promociones.objects.filter(promotor__pk=promotor)
            promociones.update(promotor=eduardo_promotor)
            propietarios_list = [
            promocion.propietario.pk for promocion in promociones
            ]
            propietarios = models.Propietario.objects.filter(pk__in=propietarios_list)
            propietarios.update(fecha_de_registro = timezone.make_aware(datetime(2018, 9, 28)))

    def start_test(self):
        for promocion in self.promociones_partial:
            self.test(promocion[0], promocion[1])

    def create_report(self):
        with open('listfile.txt', 'a') as filehandle:
            filehandle.write('De limpieza de promotores:\n')
            for listitem in self.deleted_propietarios_list:
                filehandle.write('%s\n' % listitem)
            filehandle.close()
        return True

    def start_upgrade(self):
        self.delete_full(self.promociones_full)
        for promocion in self.promociones_partial:
            self.delete_partial(promocion[0], promocion[1])
        self.create_report()

class GiroUpgrade(object):

    new_giros = giros

    def start_upgrade(self):
        giros = self.new_giros
        for giro in giros:
            for value in giro:
                new_giro = self.create_new_giro(value)
                self.change_giro_to_new_giro(new_giro, giro[value])
                self.delete_old_giros(giro[value])

    def delete_old_giros(self, giros):
        giros = models.Giro.objects.filter(pk__in=giros)
        giros.delete()

    def create_new_giro(self, designacion):
        giro = models.Giro(designacion=designacion)
        giro.save()
        return giro

    def change_giro_to_new_giro(self, new_giro, giros):
        espacios_publicitarios = models.EspacioPublicitario.objects.filter(giro__pk__in=giros)
        espacios_publicitarios.update(giro=new_giro)
    #
    #
    # def test(self):
    #     giros = self.new_giros
    #     for giro in giros:
    #         for value in giro:
    #             print(giro[value])
    #             print(len(giro[value]))

    # from Backend.upgrades.upgrade1 import CleanDb
    # e = models.EspacioInstitucional.objects.all().values_list('data__empresa').distinct()

class CleanDb(object):
    pm = 'PM ONSTREET'
    estructura_list = [
    'CASETA TELEFONICA PUBLICITARIA',
    'MULTIFUNCIONAL',
    'BILLBOARD DIGITAL'
    ]

    def start_upgrade(self):
        for estructura in self.estructura_list:
            self.change_pm_data(estructura)

        self.change_jc_data()
        self.change_asistente()

    def change_asistente(self):
        self.execute_auth_changes()
        self.execute_grupo_changes()
        models.Grupo.objects.get(nombre='asistente').delete()
        auth_models.Group.objects.get(name='asistente').delete()

    def execute_grupo_changes(self):
        grupo = models.Grupo(nombre='staff')
        grupo.save()
        usuario = models.Usuario.objects.filter(grupo__nombre='asistente')
        usuario.update(grupo=grupo)

    def execute_auth_changes(self):
        staff = auth_models.Group(name='staff')
        staff.save()
        asistente = auth_models.Group.objects.get(name='asistente')
        users = auth_models.User.objects.filter(groups__name='asistente')
        for user in users:
            staff.user_set.add(user)
            asistente.user_set.remove(user)

    def change_jc_data(self):
        espacios = models.EspacioInstitucional.objects.filter(data__empresa__icontains='JCDECAUX')
        for espacio in espacios:
            espacio.data['empresa'] = 'JCDECAUX'
            espacio.save()
        print('ok jc')


    def change_pm_data(self, estructura):
        espacios = models.EspacioInstitucional.objects.filter(data__empresa=self.pm, data__estructura__icontains=estructura)
        for espacio in espacios:
            espacio.data['estructura'] = estructura

            if(estructura == 'CASETA TELEFONICA PUBLICITARIA'):
                espacio.data['foto'] = ['/media/espacios-institucionales-1/' + 'Caseta Telefonica PMONSTREET.png',]

            elif(estructura == 'MULTIFUNCIONAL'):
                espacio.data['foto'] = ['/media/espacios-institucionales-1/' + 'Multifuncional PMONSTREET.png',]

            elif(estructura == 'BILLBOARD DIGITAL'):
                espacio.data['foto'] = ['/media/espacios-institucionales-1/' + 'Bilboard PMONSTREET.png',]

            espacio.save()
        print('OK change_pm_data')

def bulk_upload(filename):
    wb = load_workbook(filename = filename)
    sheet = wb['espacios']
    rows = sheet.max_row
    columns = sheet.max_column
    abc = list(string.ascii_uppercase)
    abc.append('AA')
    abc.append('AB')
    abc.append('AC')
    for row in range(2, rows):
        print(row)
        data = {}
        for column in range(0, columns):
            data[sheet[abc[column] + str(1)].value] = sheet[abc[column] + str(row)].value
        latitud = str(data['latitud'])
        longitud = str(data['longitud'])
        geo_localidad = models.Geolocalidad(latitud=latitud, longitud=longitud)
        geo_localidad.save()
        print(geo_localidad.pk)
        models.EspacioInstitucional(geo_localidad=geo_localidad, data=data).save()
