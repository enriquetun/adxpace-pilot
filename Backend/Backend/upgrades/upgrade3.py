from pilot import models
from datetime import datetime
import os, string
from openpyxl import load_workbook
from django.contrib.auth import get_user_model, models as auth_models
from django.utils import timezone

def start_upgrade():
    espacios = models.EspacioInstitucional.objects.filter(data__tipo='MIBILIARIO URBANO')
    for espacio in espacios:
        espacio.data['tipo'] = 'MOBILIARIO URBANO'
        espacio.save()
    print('upgrade ready')
