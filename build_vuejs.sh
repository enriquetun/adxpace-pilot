
#!/bin/bash

docker-compose exec vuejs npm run build
docker-compose exec vuejs node-sass src/assets/saas/app.scss src/assets/css/new.css
mv Frontend/src/assets/css/new.css Backend/landing/static/register/css/main.css
sh collectstatic.sh
